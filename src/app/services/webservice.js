'use strict';

/**
 * @ngdoc service
 * @name materialAngularApp.webService
 * @description
 * # webService
 * Service in the materialAngularApp.
 */
angular.module('BlurAdmin')
  .factory('webService', ['$http','$q','$rootScope', function($http,$q,$rootScope) {


	var WEBSERVICE_DIR = '../include/webService.php/';
	//var WEBSERVICE_DIR = 'http://52.32.43.125/computel/include/webService.php/';
  	//create cancellable requests

  	function createRequest(info){
  		var defer=$q.defer();
  		$rootScope.cargando=false;
		return {
			promise:$http({method:'POST',url:WEBSERVICE_DIR+info.url,data:{'data':info.data}, timeout: defer.promise})
			.success(function(data){
  				$rootScope.cargando=true;
			}),
			cancel:function(reason){
	            defer.resolve(reason);
	        }
		};
  	}
  	//Requests
  	var login = function(user){
		return createRequest({
			url:'login',
			data:user
		});
	};
  	var logout = function(){
		return createRequest({
			url:'logout',
			data:''
		});
	};
	var checkSession = function(){
		return createRequest({
			url:'checkSession',
			data:''
		});
	};
	var clientes = function(client){
		return createRequest({
			url:'clientes',
			data:client
		});
	};
  	var registrarCliente = function(client){
		return createRequest({
			url:'registrarCliente',
			data:client
		});
	};
	var editarCliente = function(client){
		return createRequest({
			url:'editarCliente',
			data:client
		});
	};

	var equipos = function(equipo){
		return createRequest({
			url:'equipos',
			data:equipo
		});
	};
  	var registrarEquipo = function(equipo){
		return createRequest({
			url:'registrarEquipo',
			data:equipo
		});
	};
	var editarEquipo = function(equipo){
		return createRequest({
			url:'editarEquipo',
			data:equipo
		});
	};
	var tecnicos = function(tecnico){
		return createRequest({
			url:'tecnicos',
			data:tecnico
		});
	};
  	var registrarTecnico = function(tecnico){
		return createRequest({
			url:'registrarTecnico',
			data:tecnico
		});
	};
	var editarTecnico = function(tecnico){
		return createRequest({
			url:'editarTecnico',
			data:tecnico
		});
	};
	var proveedores = function(proveedor){
		return createRequest({
			url:'proveedores',
			data:proveedor
		});
	};
  	var registrarProveedor = function(proveedor){
		return createRequest({
			url:'registrarProveedor',
			data:proveedor
		});
	};
	var editarProveedor = function(proveedor){
		return createRequest({
			url:'editarProveedor',
			data:proveedor
		});
	};

	var getMarcas = function(data){
		return createRequest({
			url:'getMarcas',
			data:data
		});
	};
  	var getModelos = function(data){
		return createRequest({
			url:'getModelos',
			data:data
		});
	};
	var getTiposEquipo = function(data){
		return createRequest({
			url:'getTiposEquipo',
			data:data
		});
	};

	var Users = function(data){
		return createRequest({
			url:'users',
			data:data
		});
	};
	var registrarUsers = function(data){
		return createRequest({
			url:'registrarUsers',
			data:data
		});
	};
	var editarUsers = function(data){
		return createRequest({
			url:'editarUsers',
			data:data
		});
	};

	var registrarUser = function(user){
		return createRequest({
			url:'registrarUser',
			data:user
		});
	};
	var nuevaOrden = function(data){
		return createRequest({
			url:'nuevaOrden',
			data:data
		});
	};
	var getOrdenes = function(data){
		return createRequest({
			url:'getOrdenes',
			data:data
		});
	};

	var editOrden = function(data){
		return createRequest({
			url:'editOrden',
			data:data
		});
	}

	var EquiposTecnico = function(data){
		return createRequest({
			url:'EquiposTecnico',
			data:data
		});
	};
	var changeEstadoOrden = function(data){
		return createRequest({
			url:'changeEstadoOrden',
			data:data
		});
	};

	var desactivarUser = function(user){
		return createRequest({
			url:'registrarUser',
			data:user
		});
	};
	var inventario = function(data){
		return createRequest({
			url:'inventario',
			data:data
		});
	};
	
	var eliminarInventario = function(data){
		return createRequest({
			url:'eliminarInventario',
			data:data
		});
	};
	var NewFacturaCompra = function(data){
		return createRequest({
			url:'NewFacturaCompra',
			data:data
		});
	}
	var ordenes = function(data){
		return createRequest({
			url:'ordenes',
			data:data
		});
	}
	var getModelCompra = function(data){
		return createRequest({
			url:'getModelCompra',
			data:data
		});
	}
	var getTecnicosOrden = function(data){
		return createRequest({
			url:'getTecnicosOrden',
			data:data
		});
	}
	var getTecnicosOrden = function(data){
		return createRequest({
			url:'getTecnicosOrden',
			data:data
		});
	}
	var asignarTecnicos = function(data){
		return createRequest({
			url:'asignarTecnicos',
			data:data
		});
	}	
	var ImprimirComprobante = function(data){
		return createRequest({
			url:'ImprimirComprobante',
			data:data
		});
	}

	var NewMarca = function(data){
		return createRequest({
			url:'NewMarca',
			data:data
		});
	}

	var getMarcasModelo = function(data){
		return createRequest({
			url:'getMarcasModelo',
			data:data
		});
	}
	
	var updateMarca = function(data){
		return createRequest({
			url:'updateMarca',
			data:data
		});
	}
	
	var NewModelo = function(data){
		return createRequest({
			url:'NewModelo',
			data:data
		});
	}

	var SaveHerramienta = function(data){
		return createRequest({
			url:'SaveHerramienta',
			data:data
		});
	}
		
	var SavePieza = function(data){
		return createRequest({
			url:'SavePieza',
			data:data
		});
	}
	var categorias = function(data){
		return createRequest({
			url:'categorias',
			data:data
		});
	}
	
	var facturar = function(data){
		return createRequest({
			url:'facturar',
			data:data
		});
	}
	var indicador1 = function(data){
		return createRequest({
			url:'indicador1',
			data:data
		});
	}
	var indicador2 = function(data){
		return createRequest({
			url:'indicador2',
			data:data
		});
	}
	var indicador3 = function(data){
		return createRequest({
			url:'indicador3',
			data:data
		});
	}
	
	var indicador4 = function(data){
		return createRequest({
			url:'indicador4',
			data:data
		});
	}
	
	var indicador5 = function(data){
		return createRequest({
			url:'indicador5',
			data:data
		});
	}

	var indicador6 = function(data){
		return createRequest({
			url:'indicador6',
			data:data
		});
	}
	var reporte1 = function(data){
		return createRequest({
			url:'reporte1',
			data:data
		});
	}
	var reporte2 = function(data){
		return createRequest({
			url:'reporte2',
			data:data
		});
	}
	var reporte3 = function(data){
		return createRequest({
			url:'reporte3',
			data:data
		});
	}
	var reporte4 = function(data){
		return createRequest({
			url:'reporte4',
			data:data
		});
	}
	var reporte5 = function(data){
		return createRequest({
			url:'reporte5',
			data:data
		});
	}
		
	var newCategoria = function(data){
		return createRequest({
			url:'newCategoria',
			data:data
		});
	}

	var comisiones = function(data){
		return createRequest({
			url:'comisiones',
			data:data
		});
	}
	
	var factura_servicio = function(data){
		return createRequest({
			url:'factura_servicio',
			data:data
		});
	}
	
	var newTipoEquipo = function(data){
		return createRequest({
			url:'newTipoEquipo',
			data:data
		});
	}
	
	var factura_final = function(data){
		return createRequest({
			url:'factura_final',
			data:data
		});
	}

	var newBanco = function(data){
		return createRequest({
			url:'newBanco',
			data:data
		});
	}

	var bancos = function(data){
		return createRequest({
			url:'bancos',
			data:data
		});
	}
		
return {
		login: function(user){return login(user);},
		logout: function(){return logout();},
		checkSession: function(){return checkSession();},
		clientes: function(client){return clientes(client);},
		registrarCliente: function(client){return registrarCliente(client);},
		editarCliente: function(client){return editarCliente(client);},
		equipos: function(equipo){return equipos(equipo);},
		registrarEquipo: function(equipo){return registrarEquipo(equipo);},
		editarEquipo: function(equipo){return editarEquipo(equipo);},
		tecnicos: function(tecnico){return tecnicos(tecnico);},
		registrarTecnico: function(tecnico){return registrarTecnico(tecnico);},
		editarTecnico: function(tecnico){return editarTecnico(tecnico);},
		proveedores: function(proveedor){return proveedores(proveedor);},
		registrarProveedor: function(proveedor){return registrarProveedor(proveedor);},
		editarProveedor: function(proveedor){return editarProveedor(proveedor);},
		getMarcas: function(data){return getMarcas(data);},
		getModelos: function(data){return getModelos(data);},
		getTiposEquipo: function(data){return getTiposEquipo(data);},
		Users: function(data){return Users(data);},
		registrarUsers: function(data){return registrarUsers(data);},
		editarUsers: function(data){return editarUsers(data);},
		nuevaOrden: function(data){return nuevaOrden(data);},
		getOrdenes: function(data){return getOrdenes(data);},
		editOrden: function(data){return editOrden(data);},
		EquiposTecnico: function(data){return EquiposTecnico(data);},
		changeEstadoOrden: function(data){return changeEstadoOrden(data);},
		desactivarUser: function(data){return desactivarUser(data);},
		inventario: function(data){return inventario(data);},
		eliminarInventario: function(data){return eliminarInventario(data);},
		NewFacturaCompra: function(data){return NewFacturaCompra(data);},
		ordenes: function(data){return ordenes(data);},
		getModelCompra: function(data){return getModelCompra(data);},
		getTecnicosOrden: function(data){return getTecnicosOrden(data);},
		asignarTecnicos: function(data){return asignarTecnicos(data);},
		NewMarca: function(data){return NewMarca(data);},
		getMarcasModelo: function(data){return getMarcasModelo(data);},
		updateMarca: function(data){return updateMarca(data);},
		NewModelo: function(data){return NewModelo(data);},
		ImprimirComprobante: function(data){return ImprimirComprobante(data);},
		SaveHerramienta: function(data){return SaveHerramienta(data);},
		SavePieza: function(data){return SavePieza(data);},
		categorias: function(data){return categorias(data);},
		facturar: function(data){return facturar(data);},
		indicador1: function(data){return indicador1(data);},
		indicador2: function(data){return indicador2(data);},
		indicador3: function(data){return indicador3(data);},
		indicador4: function(data){return indicador4(data);},
		indicador5: function(data){return indicador5(data);},
		indicador6: function(data){return indicador6(data);},
		reporte1: function(data){return reporte1(data);},
		reporte2: function(data){return reporte2(data);},
		reporte3: function(data){return reporte3(data);},
		reporte4: function(data){return reporte4(data);},
		reporte5: function(data){return reporte5(data);},
		comisiones: function(data){return comisiones(data);},
		newCategoria: function(data){return newCategoria(data);},
		factura_servicio: function(data){return factura_servicio(data);},
		newTipoEquipo: function(data){return newTipoEquipo(data);},
		factura_final: function(data){return factura_final(data);},
		newBanco: function(data){return newBanco(data);},
		bancos: function(data){return bancos(data);},
	};
    
  }]);
 