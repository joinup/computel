/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages', [
    'ui.router',

    'BlurAdmin.pages.dashboard',
    'BlurAdmin.pages.ui',
    'BlurAdmin.pages.components',
    'BlurAdmin.pages.form',
    'BlurAdmin.pages.tables',
    'BlurAdmin.pages.charts',
    'BlurAdmin.pages.maps',
    'BlurAdmin.pages.profile',
    
    'BlurAdmin.pages.cliente',
    'BlurAdmin.pages.equipos',
    'BlurAdmin.pages.orden_reparacion',
    'BlurAdmin.pages.tecnicos',
    'BlurAdmin.pages.inventario',
    'BlurAdmin.pages.proveedor',
    'BlurAdmin.pages.user',
    'BlurAdmin.pages.tecnicoEquipo',
    'BlurAdmin.pages.indicador',
    'BlurAdmin.pages.ordenes',
    'BlurAdmin.pages.marca',
    'BlurAdmin.pages.reportes',
    'login',

  ])
    .config(routeConfig)
    .run(function ($rootScope, webService,$location) {
        $rootScope.$on('$routeChangeStart', function (event, next,current) {
            console.log('algo')
        });
        $rootScope.login=false;
        console.log($location.path())
        webService.checkSession().promise.success(function(data){
            if (data.success) {
                $rootScope.currentUser=data.data;
                $rootScope.login=false;
                if($location.url()=="/login")
                    $location.path("/")
            }else{
                $rootScope.login=true;
                $location.path("/login")
            };
        });
        $rootScope.$on('logout',function(){
                $rootScope.currentUser=null; 
                $rootScope.login=true;
                $location.url("/login");
                webService.logout().promise.success(function(){});
            });
        $rootScope.logout = function(){
            $rootScope.currentUser=null; 
                $rootScope.login=true;
                $location.url("/login");
                webService.logout().promise.success(function(){});
        }
    });

  /** @ngInject */
  function routeConfig($urlRouterProvider, baSidebarServiceProvider) {
    $urlRouterProvider.otherwise('/ordenes');

    // baSidebarServiceProvider.addStaticItem({
    //   title: 'Pages',
    //   icon: 'ion-document',
    //   subMenu: [{
    //     title: 'Sign In',
    //     fixedHref: 'auth.html',
    //     blank: true
    //   }, {
    //     title: 'Sign Up',
    //     fixedHref: 'reg.html',
    //     blank: true
    //   }, {
    //     title: 'User Profile',
    //     stateRef: 'profile'
    //   }, {
    //     title: '404 Page',
    //     fixedHref: '404.html',
    //     blank: true
    //   }]
    // });
    // baSidebarServiceProvider.addStaticItem({
    //   title: 'Menu Level 1',
    //   icon: 'ion-ios-more',
    //   subMenu: [{
    //     title: 'Menu Level 1.1',
    //     disabled: true
    //   }, {
    //     title: 'Menu Level 1.2',
    //     subMenu: [{
    //       title: 'Menu Level 1.2.1',
    //       disabled: true
    //     }]
    //   }]
    // });
  }

})();

