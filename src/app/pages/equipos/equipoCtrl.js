/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.equipos')
    .controller('EquiposCtrl', EquiposCtrl);

  /** @ngInject */
  function EquiposCtrl($scope, fileReader, $filter, $uibModal, DTOptionsBuilder,webService,toastr) {
  	$scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap().withLanguageSource('espanol.json');;
    
    $scope.data= {};    
    $scope.newMarca = false;
    $scope.newModelo = false;
    $scope.newType = false;
    


    cargarEquipos();
    //Functionsmodelos

    function cargarEquipos(){
      webService.equipos().promise.success(function(data){
        if (data.success) {
          $scope.equipos=data.data;
        };
      });
    }

    $scope.nuevoEquipo = function(){
      getDataModal();
    	$scope.modal =  $uibModal.open({
    	  animation: true,
    	  templateUrl: 'app/pages/equipos/ModalNuevoEquipo.html',
    	  size: 'lg',
    	  scope:$scope,
    	});

    }

    function getDataModal(){
      webService.getTiposEquipo().promise.success(function(data){
        if (data.success) {
          $scope.tiposEquipo=data.data;
          $scope.tiposEquipo.push({codigo_tipo : '-1', nombre : 'Agregar Nuevo Tipo ', estado:'activo'});
        }else{
          $scope.tiposEquipo=[{codigo_tipo : '-1', nombre : 'Agregar Nuevo Tipo ', estado:'activo'}];
        }
      })

      webService.getMarcas().promise.success(function(data){
        if (data.success) {
          $scope.marcas = data.data;
          $scope.marcas.push({id : '-1', nombre : 'Agregar nueva Marca', estado:'activo'})
        }else{
          $scope.marcas = [{id : '-1', nombre : 'Agregar nueva Marca', estado:'activo'}];
        }
      });
        
    }

    $scope.getModelosMarca = function(){
      webService.getModelos($scope.data.id_marca).promise.success(function(data){
        if (data.success) {
          $scope.modelos = data.data;
          $scope.modelos.push({id : '-1', nombre : 'Agregar nueva Modelo', estado:'activo'});
        }else{
          $scope.modelos = [{id : '-1', nombre : 'Agregar nueva Modelo', estado:'activo'}];
        }
      });
    }

    $scope.newItem = function(){
      if ($scope.data.id_marca == -1) {
        $scope.newMarca = true;
        $scope.newModelo = true;
      }else{
        $scope.getModelosMarca();
      } 

      if ($scope.data.id_modelo == -1) {
        $scope.newModelo = true;
      }

      if ($scope.data.codigo_tipo == -1) {
        $scope.newType = true;
      }

    }

    $scope.Save = function(){
      webService.registrarEquipo($scope.data).promise.success(function(data){
        if (data.success) {
          toastr.success('registrado con exito');//mensaje de exito
          $scope.modal.dismiss('ok'); //cerrar la modal
          $scope.data = {};//reinicia los campos de la modal
          cargarEquipos();//recarga los clientes
          $scope.newMarca = false;
          $scope.newModelo = false;
          $scope.newType = false;
          
        }else{
          toastr.error('Error al registrar');//mensaje de error
        }
      });
    }

    $scope.validate = function (){
      var equipo = {};
      equipo.serial_equipo=$scope.data.serial_equipo;
      webService.equipos(equipo).promise.success(function(data){
        if (data.success) {
          toastr.error('Este equipo ya se encuentra registrado');//mensaje de exito
        };
      });
    }

    $scope.editEquipo = function(team){
      var equipo = angular.copy(team);
      delete equipo.marca;
      delete equipo.modelo;
      delete equipo.tipo;
      webService.editarEquipo(equipo).promise.success(function(data){
        if (data.success) {
          toastr.success('Editado con exito');
          cargarEquipos();//recarga los clientes
        }else{
          toastr.error('Error al editar');
        }
      });
    }


  }

})();
