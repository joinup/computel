/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.user')
    .controller('UserCtrl', UserCtrl);

  /** @ngInject */
  function UserCtrl($scope,$rootScope, fileReader, $filter, $uibModal, editableOptions,editableThemes,webService, DTOptionsBuilder, toastr) {
    
    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap().withLanguageSource('espanol.json');;
    
    $scope.nuevoUser = {};
    $scope.estados = ['activo', 'inactivo'];
    $scope.tipos = ['admin', 'tecnico'];

    $scope.usu={};
    $scope.usu.estado='activo';

    $scope.cargarUser = function(estado){
      console.log(estado);
      webService.Users(estado).promise.success(function(data){
        if (data.success) {
          $scope.users = data.data;
        }
      });
    }
    $scope.cargarUser($scope.usu);

    $scope.NewUser = function () { //funcion para abrir la modal
      console.log('hello');
      $scope.modal = $uibModal.open({
        animation: true,
        templateUrl: 'app/pages/user/ModalNewUser.html',
        size: 'lg',
        scope:$scope,
      });
    };

    $scope.saveUser = function (user) {
      user.tipo='admin';
      webService.registrarUsers(user).promise.success(function(data){
        if (data.success) {
          toastr.success('registrado con exito');//mensaje de exito
          $scope.modal.dismiss('ok'); //cerrar la modal
          $scope.nuevoUser = {};//reinicia los campos de la modal
          $scope.usu.estado='activo';
          $scope.cargarUser($scope.usu);//recarga los clientes
        }else{
          toastr.error('Error al registrar');//mensaje de error
        }
      })
    }

    $scope.editUser = function(user){
      webService.editarUsers(user).promise.success(function(data){
        if (data.success) {
          toastr.success('Editado con exito');//mensaje de exito
          $scope.usu.estado='activo';
          $scope.cargarUser($scope.usu);
        }else{
          toastr.error('Error al editar');//mensaje de error
        }
      })
    }   

    $scope.removeUser = function(user){
      user.estado='inactivo'
      webService.editarUsers(user).promise.success(function(data){
        if (data.success) {
          toastr.success('Desactivado con exito');//mensaje de exito
        }else{
          toastr.error('Error al Desactivar');//mensaje de error
        }
      })
    }

    $scope.cambiarClave = function(usuario){
      $scope.newPassword = {};
      $scope.newPassword.cedula = usuario.cedula;
      $scope.clave = usuario.clave;
      $scope.errorContraseña = false;
      $scope.modal = $uibModal.open({
        animation: true,
        templateUrl: 'app/pages/user/cambiarContrasena.html',
        size: 'md',
        scope:$scope,
      });
    }

    $scope.validar = function(){
      if ($scope.newPassword.clave != $scope.newPassword.clave2) {
        $scope.errorPassword = true;
      }else{
        $scope.errorPassword = false;
      }
    }

    $scope.changePassword = function(){
      if ($scope.newPassword.clave == $scope.newPassword.clave2) {
        delete $scope.newPassword.clave2;
        webService.editarUsers($scope.newPassword).promise.success(function(data){
          if (data.success) {
            toastr.success('La contraseña ha sido actualizada');//mensaje de exito
            $scope.modal.dismiss('ok'); //cerrar la modal
          }else{
            toastr.error('Error al editar');//mensaje de error
          }
        })
      }
    }

    //esto es para los bontonees del datatable NO TOCAR
    editableOptions.theme = 'bs3';
    editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary btn-with-icon"><i class="ion-checkmark-round"></i></button>';
    editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-default btn-with-icon"><i class="ion-close-round"></i></button>';

  }

})();

