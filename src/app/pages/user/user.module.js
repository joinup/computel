/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.user', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('user', {
          url: '/user',
          title: 'Usuario',
          templateUrl: 'app/pages/user/user.html',
          controller: 'UserCtrl',
          sidebarMeta: {
            icon: 'fa fa-user',
            order: 7,
          },
        });
  }

})();