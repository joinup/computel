/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

    angular.module('BlurAdmin.pages.orden_reparacion', [])
      .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('orden_reparacion', {
              url: '/orden_reparacion',
              title: 'Orden Reparacion',
              templateUrl: 'app/pages/orden_reparacion/orden_reparacion.html',
              controller: 'OrdenReparacionCtrl',
              sidebarMeta: {
                icon: 'ion-compose',
                order: 1,
              },
        });
    }

})();
