/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
	'use strict';

	angular.module('BlurAdmin.pages.orden_reparacion')
	.controller('OrdenReparacionCtrl', OrdenReparacionCtrl);

	/** @ngInject */
	function OrdenReparacionCtrl($scope, filterFilter, fileReader,webService, $filter,toastr, $uibModal,editableOptions,editableThemes,DTOptionsBuilder) {

		$scope.estados = [
			'revision',
		  	'devuelto',
		   	'proceso',
		   	'reparado',
		   	'entregado'
		];
		$scope.orden={};
		$scope.orden.estado='revision';
		$scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap().withLanguageSource('espanol.json');;
		
		$scope.typePayment = [{id : 'efectivo' , nombre : 'Efectivo'},
							{id : 'debito' , nombre : 'Debito'},
							{id : 'credito' , nombre : 'Credito'},
							{id : 'transferencia' , nombre : 'Transferencia'}];

		$scope.estado_factura = [{id:'pagada', nombre: 'Pagada'},
								{id:'anulada', nombre: 'Anulada'},
								{id:'espera', nombre: 'Espera'}];

		$scope.validateEstado = function(estado){
			if (estado == 'revision') {
				$scope.estados = ['revision','devuelto','proceso','reparado','entregado'];
			}else if (estado == 'devuelto') {
				$scope.estados = ['devuelto','entregado'];
			}else if (estado == 'proceso') {
				$scope.estados = ['proceso','devuelto','reparado','entregado'];
			}else if (estado == 'reparado') {
				$scope.estados = ['reparado','entregado'];
			}else{
				$scope.estados = ['entregado'];
			}

		}
		$scope.showState = function(orden) {
		  	if(orden.estado && $scope.estados.length) {
		    	var selected = $filter('filter')($scope.estados, {id: orden.estado});
		    	return selected.length ? selected[0].text : 'Not set';
		  	}else return 'Not set'
		};
		$scope.opened = {};

		$scope.open = function($event, elementOpened) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.opened[elementOpened] = !$scope.opened[elementOpened];
		}
		$scope.editOrden = function(orden){
			var ordenRep = {};
			ordenRep.num_reparacion = orden.num_reparacion;
			ordenRep.descripcion = orden.descripcion;
			ordenRep.falla = orden.falla;
			ordenRep.estado = orden.estado;
			var continuar=true;
			webService.getTecnicosOrden(orden).promise.success(function(data){
				if (data.success){
					var tecnicos = data.data;
				}

				if (orden.estado=='reparado' || orden.estado=='entregado' || orden.estado=='devuelto') {
					for (var i = 0; i < tecnicos.length; i++) {
						if (tecnicos[i].estado!='reparado' || tecnicos[i].estado!='devuelto') {
							continuar=false;
						}
					}
				}

				if (continuar==true) {
					webService.editOrden(ordenRep).promise.success(function(data){
						if (data.success) {
			       			toastr.success(data.msg);
							$scope.init();
							$scope.orden.estado=orden.estado;
							$scope.getOrdenes($scope.orden);
			        	}else{
			        		toastr.error(data.msg);
			        	}
					});
				}else{
					$scope.getOrdenes();
			        toastr.error("Los técnicos aun no han reparado este equipo");
				}
			})

		}

		$scope.newOrder = function () {
			$scope.open=!$scope.open;
		};
		$scope.getTecnicosOrden = function(item){
			webService.getTecnicosOrden(item).promise.success(function(data){
				if (data.success)
					$scope.info.piezas_usadas = data.data[0].accesorios;
					$scope.info.tecnicos = data.data;

			})
			webService.factura_final(item.num_reparacion).promise.success(function(data){
				if (data.success)
					$scope.info.descripcion_fact=data.data.descripcion;
					$scope.fechaFactura=data.data.fecha;

			})
		}
		$scope.infoOrden = function (item) {
			$scope.info = item;
			$scope.getTecnicosOrden(item);

			var day = moment().format("YYYY-MM-DD");
			if ($scope.info.estado == 'entregado') {
				console.log($scope.info.estado);
				$scope.info.tiempoSitio = restaFechas($scope.info.fecha,$scope.fechaFactura);
				
			}else{
				$scope.info.tiempoSitio = restaFechas($scope.info.fecha,day);
			}
		  $uibModal.open({
		    animation: true,
		    templateUrl: 'app/pages/orden_reparacion/infoOrden.html',
		    size: 'lg',
		    scope:$scope,
		  });
		
		};

		function restaFechas (f1,f2){
			var fecha1 = new Date(f1);
			var fecha2 = new Date(f2);				
			var diferencia = 0;
			if(fecha2.valueOf()>fecha1.valueOf()){
				diferencia = fecha2.valueOf()-fecha1.valueOf();
			}else{
				diferencia = fecha1.valueOf()-fecha2.valueOf();
			}
			return  diferencia/(1000*60*60*24);
		}

		$scope.asignarTecnico = function(data){
			$scope.modal = $uibModal.open({
			  animation: true,
			  templateUrl: 'app/pages/orden_reparacion/asignatTecnico.html',
			  size: 'md',
			  scope:$scope,
			});
		}
		$scope.facturar = function(item){
			$scope.info = item;
			$scope.getTecnicosOrden(item)
			bancos();
			$scope.modal = $uibModal.open({
			  animation: true,
			  templateUrl: 'app/pages/orden_reparacion/facturar.html',
			  size: 'md',
			  scope:$scope,
			});
		}
		$scope.GuardarFactura = function(){
			webService.facturar($scope.info).promise.success(function(data){
				if (data.success) {
					$scope.modal.dismiss('ok'); 
					toastr.success('Factura Generada');
					$scope.orden.estado='entregado';
					$scope.getOrdenes();
					window.open('../include/pdf/'+data.data,'_black');
				}else{
					toastr.error(data.msg);
				}
			});
		}
		$scope.guardarTecnicos = function(tecnico){
			var info = {num_reparacion:$scope.info.num_reparacion,cedula_tecnico:tecnico}
			webService.asignarTecnicos(info).promise.success(function(data){
				if (data.success) {
	       			toastr.success(data.msg);
	       			$scope.getTecnicosOrden($scope.info)
					$scope.modal.dismiss('ok'); 
	        	}else{
	        		toastr.error(data.msg);
	        	}
			})
		}	

		function bancos(){
		    webService.bancos().promise.success(function(data){
		        if (data.success) {
		            $scope.bancos = data.data;
		        }
		    });
		}
		
		$scope.imprimirComprobante = function(data){
			webService.ImprimirComprobante(data).promise.success(function(data){
				if (data.success) {
					window.open('../include/pdf/'+data.data,'_black')
				}else{
					toastr.error(data.msg);
				}
			})
		}

		editableOptions.theme = 'bs3';
    	editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary btn-with-icon"><i class="ion-checkmark-round"></i></button>';
    	editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-default btn-with-icon"><i class="ion-close-round"></i></button>';

		/////////////////Nueva Orden///////////////////////////
		$scope.init = function(){
			$scope.data= {
				cliente:{tipo:"V"},
				equipo:{},
				reparacion:{},
				tecnicos:[],
				accesorios:[{name:'audifonos',selected:false,value:'audifonos'},
							{name:'cargador',selected:false,value:'cargador'},
							{name:'bateria',selected:false,value:'pila'},
							{name:'SimCard',selected:false,value:'sim_car'},
							{name:'Tapa',selected:false,value:'tapa'},
							{name:'tarjeta SD',selected:false,value:'tarjeta_memoria'}],
			}
			$scope.paso=1;
			$scope.open=false;
		}
		$scope.init();
		$scope.siguiente=function(){
			$scope.paso++;
		}
		$scope.anterior=function(){
			$scope.paso--;
		}

		$scope.getOrdenes = function(){

			webService.getOrdenes($scope.orden).promise.success(function(data){
				if (data.success) {
					$scope.ordenes = data.data;
				}else{
					toastr.error(data.msg);
				}
			})
		}
		$scope.getOrdenes();

		webService.getTiposEquipo().promise.success(function(data){
			if (data.success) {
				$scope.tipo_equipos = data.data;
			}

		});
		webService.getMarcas().promise.success(function(data){
			if (data.success) {
				$scope.marcas = data.data;
			}

		});
		$scope.getModelosMarca = function(){
			webService.getModelos($scope.data.equipo.id_marca).promise.success(function(data){
				if (data.success) {
					$scope.modelos = data.data;
				}
			})
		}
		
		var item={};
		item.estado='activo';
		webService.tecnicos(item).promise.success(function(data){
			if (data.success) {
				$scope.tecnicos = data.data;
			}
		});

		$scope.guardarOrden = function(orden){

			webService.nuevaOrden(orden).promise.success(function(data){
				if (data.success) {
	       			toastr.success(data.msg);
					$scope.init();
					$scope.getOrdenes();
	        	}else{
	        		toastr.error(data.msg);
	        	}
			});
		}
		/*$scope.selectedAccesorios = function selectedAccesorios() {
		    return filterFilter($scope.accesorios, { selected: true });
		  };

		  // watch accesorios for changes
		  $scope.$watch('accesorios|filter:{selected:true}', function (nv) {
		    $scope.data.accesorios = nv.map(function (accesorio) {
		      return accesorio.name;
		    });
		  }, true);*/

		$scope.verificarCliente = function (cliente){
			webService.clientes(cliente).promise.success(function(data){
				if (data.success) {
					$scope.data.cliente = data.data[0];
				}
			})  
		}

		$scope.verificarEquipo = function (equipo){
			$scope.revision=false;
			webService.equipos(equipo).promise.success(function(data){
				if (data.success) {
					for (var i = 0; i < $scope.ordenes.length; i++) {
						if ($scope.ordenes[i].serial_equipo == data.data[0].serial_equipo ) {
							if ($scope.ordenes[i].estado !='entregado') {
								toastr.error('Este equipo ya se encuentra en revisión');
								$scope.revision=true;
								console.log('jaef');
							}
						}
					}
					if ($scope.revision==false) {
						$scope.equipoExist = data.data[0];
						$scope.data.equipo.codigo_tipo=$scope.equipoExist.codigo_tipo;
						$scope.data.equipo.id_marca=$scope.equipoExist.id_marca;
						$scope.data.equipo.id_modelo=$scope.equipoExist.id_modelo;
						$scope.data.equipo.observaciones=$scope.equipoExist.observaciones;
					}

				}else{
					$scope.equipoExist=false;
					$scope.data.equipo.codigo_tipo='';
					$scope.data.equipo.id_marca='';
					$scope.data.equipo.id_modelo='';
					$scope.data.equipo.observaciones='';
				}

			})  
		}

		$scope.comision = function(total){
			for (var i = 0; i < $scope.info.tecnicos.length; i++) {
				$scope.info.tecnicos[i].comision=total*0.05;
			}
		}
		


	}

})();
