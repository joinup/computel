/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.inventario', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('inventario', {
          url: '/inventario',
          title: 'Inventario',
          templateUrl: 'app/pages/inventario/inventario.html',
          controller: 'InventarioCtrl',
          sidebarMeta: {
            icon: 'ion-clipboard',
            order: 9,
          }
        });
  }

})();
