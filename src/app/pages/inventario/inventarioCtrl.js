/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.inventario')
    .controller('InventarioCtrl', InventarioCtrl);

  /** @ngInject */

  function InventarioCtrl($scope, fileReader, $filter, $uibModal, DTOptionsBuilder,webService,toastr) {
	$scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap().withLanguageSource('espanol.json');

    $scope.item={};
    
    $scope.data ={};
    $scope.dataEliminar={};
    $scope.type = true;
    $scope.showInfoProd = false;
    $scope.newProducto= {
    	factura:{},
    	prod:[]

    }
    $scope.dateOptions = {
        maxDate : new Date()
    };
    $scope.newProducto.factura.fecha_compra = new Date();
    $scope.paso=1;
    $scope.open=false;
    $scope.openReduccion = false;
    $scope.showItemPiecesPropio = false;

    $scope.estados = ['activo','inactivo'];


    $scope.typePayment = [{id : 'efectivo' , nombre : 'Efectivo'},
						{id : 'debito' , nombre : 'Debito'},
						{id : 'credito' , nombre : 'Credito'},
						{id : 'transferencia' , nombre : 'Transferencia'}];

    $scope.typeProd = [{id: 'herramienta', nombre: 'Herramienta'},
                        {id : 'mobiliario', nombre: 'Mobiliario'},
                        {id : 'pieza' , nombre: 'Pieza'}];

    $scope.format = 'dd-MMMM-yyyy';
    $scope.popup1 = {
        opened: false
    };
    $scope.altInputFormats = ['M!/d!/yyyy'];


	loadInventario();

	function loadInventario(){
		webService.inventario().promise.success(function(data){
		 	if (data.success) {
			    $scope.pieces = data.data.piezas;
		    	$scope.tool = data.data.herramientas;
		    	$scope.furniture = data.data.mobiliario;
                $scope.facturaCompra =data.data.factura_compra;
                $scope.loadProduct();
		  }
		});
	}

    $scope.NewPurchase  = function(){
        proveedor();
        bancos();
        $scope.open=!$scope.open;
        $scope.openReduccion = false;

    }  

    $scope.reduccionInventario  = function(){
    	$scope.openReduccion = !$scope.openReduccion;
        $scope.open=false;
    }

    function proveedor(){
        webService.proveedores().promise.success(function(data){
            if (data.success) {
                $scope.proveedor = data.data;
            }
        });
    }    

    function bancos(){
        webService.bancos().promise.success(function(data){
            if (data.success) {
                $scope.bancos = data.data;
            }
        });
    }

    

    $scope.loadProduct = function(){
    	if ($scope.data.tipo == 'herramienta') {
            $scope.producto = angular.copy($scope.tool);
    	}else if ($scope.data.tipo == 'mobiliario'){
    		$scope.producto = angular.copy($scope.furniture);
    	}
    	if ($scope.data.tipo == 'herramienta' || $scope.data.tipo == 'mobiliario') {
    		$scope.showItemPiecesPropio = false;
	    	if ($scope.producto == false) {
	    		$scope.producto = [{id : '-1', nombre : 'Agregar nuevo'}];
	    	}else{
	    		$scope.producto.push({id : '-1', nombre : 'Agregar nuevo'});
	    	}
    	}

    	if ($scope.data.tipo == 'pieza') {
    		$scope.showItemPiecesPropio = true;
    	    $scope.producto = angular.copy($scope.pieces);
    	    if ($scope.producto == false) {
    	    	$scope.producto = [{id_pieza : '-1', nombre : 'Agregar nuevo'}];
    	    }else{
    	    	$scope.producto.push({id_pieza : '-1', nombre : 'Agregar nuevo'});
    	    }
    	}
    }  

    $scope.getModelos = function(){
        webService.getModelos().promise.success(function(data){
            if (data.success) {
              $scope.modelos = data.data;
            }
        });  
    }

    /*DatePicker*/

    $scope.open1 = function() {
      $scope.popup1.opened = true;
    };

    $scope.loadItem = function(){
        console.log($scope.data);
    	if ($scope.data.tipo == 'herramienta' && $scope.data.id_inventario == -1) {	
            $scope.item.tipo = 'herramienta';
            openNewProducto();
    	}else if ($scope.data.id_inventario == -1 && $scope.data.tipo == 'mobiliario') {
           $scope.item.tipo = 'mobiliario';
            openNewProducto();
        }else if ($scope.data.id_pieza == -1 && $scope.data.tipo == 'pieza') {
            $scope.item.tipo = 'pieza';
            openNewPieza();
    	}
    }

    function openNewProducto(){
        $scope.modal = $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/inventario/newItem.html',
          size: 'md',
          scope:$scope,
        });
    }

    function openNewPieza(){
        getCategoria();
        $scope.getModelos();
        $scope.modal = $uibModal.open({
          animation: true,
          templateUrl: 'app/pages/inventario/newPieza.html',
          size: 'md',
          scope:$scope,
        });
    }

    function getCategoria(){
        webService.categorias().promise.success(function(data){
            if (data.success) {
                $scope.categorias=data.data;
                console.log($scope.categorias);
            }
        }); 
    }

    $scope.saveProd = function(producto){
        webService.SaveHerramienta(producto).promise.success(function(data){
            if (data.success) {
              toastr.success('Producto registrado');//mensaje de exito
              $scope.modal.dismiss('ok'); //cerrar la modal
              $scope.item.nombre='';
              loadInventario();//recarga los clientes
            }
        }); 
    }

    $scope.savePieza = function(producto){
        webService.SavePieza(producto).promise.success(function(data){
            if (data.success==true) {
              toastr.success('Pieza registrada');//mensaje de exito
              $scope.modal.dismiss('ok'); //cerrar la modal
              loadInventario();//recarga los clientes
            }else{
              toastr.error(data.msg);//mensaje de exito
            }
        }); 
    }


    $scope.addItem = function(){
    	$scope.newProducto.prod.push(angular.copy($scope.data));
    	$scope.data = {};
	    $scope.data.tipo = 'herramienta';
        $scope.showInfoProd = true;
        $scope.infoProd = $scope.newProducto.prod;
        LoadInfoTable();
        $scope.loadProduct();
        //$scope.showItemPiecesPropio = false;
    }

    function LoadInfoTable(){
        console.log($scope.infoProd );
        var total= 0;
		var totalTable= 0;
		for (var i = 0; i < $scope.infoProd.length; i++) {
			if (typeof( $scope.infoProd[i].nombre) == 'undefined') {
				for (var j = 0; j < $scope.producto.length; j++) {
                    if ($scope.infoProd[i].tipo=='pieza') {
    					if ($scope.producto[j].id_pieza == $scope.infoProd[i].id_pieza ) {
                            $scope.infoProd[i].nombre = $scope.producto[j].nombre;
                        }
                    }else{
                        if ($scope.producto[j].id == $scope.infoProd[i].id_inventario ) {
                            $scope.infoProd[i].nombre = $scope.producto[j].nombre;
    					}
                    }

				}
			}

            total = total + parseFloat($scope.infoProd[i].costo) * parseFloat($scope.infoProd[i].cantidad);
			totalTable = parseFloat($scope.infoProd[i].costo) * parseFloat($scope.infoProd[i].cantidad);
            $scope.newProducto.factura.monto_total = total;
            $scope.infoProd[i].monto_total = totalTable;
		}
	}

	$scope.siguiente=function(){
		$scope.paso++;
	}
	$scope.anterior=function(){
		$scope.paso--;
	}

	$scope.suprimirProducto = function (item){
		for (var i = 0; i < $scope.infoProd.length; i++) {
			if ($scope.infoProd[i].nombre == item.nombre) {
				$scope.infoProd.splice(i,1);
				$scope.newProducto.prod = $scope.infoProd;
			}
		}
		LoadInfoTable();
	}

	$scope.guardarCompra = function(){
		var date = $scope.newProducto.factura.fecha_compra;
        date = moment(date).format("YYYY-MM-DD");
    	$scope.open=false;
        $scope.newProducto.factura.fecha_compra =date;
        webService.NewFacturaCompra($scope.newProducto).promise.success(function(data){
            if (data.success) {
    	        $scope.open=false;
                toastr.success('Compra agregada con exito');
                loadInventario();
                $scope.newProducto= {
                    factura:{},
                    prod:[]
                };
                $scope.paso=1;
                $scope.showInfoProd = false;
                $scope.avanzar=false;


            }else{
                toastr.error(data.msg);
            }
        });
	}

    $scope.loadProductos = function(){
        $scope.Product = {};
        if ($scope.dataEliminar.tipo == 'herramienta') {
            $scope.type = true;
            $scope.Product = $scope.tool;
        }else if ($scope.dataEliminar.tipo == 'pieza') {
            $scope.type = false;
            $scope.Product = $scope.pieces;
        }else{
            $scope.Product = $scope.furniture;
            $scope.type = true;
        }
    }

	$scope.guardarReduccion = function (item){
        webService.eliminarInventario($scope.dataEliminar).promise.success(function(data){
            if (data.success) {
                toastr.success(data.msg);//mensaje de exito
                $scope.dataEliminar={};
                loadInventario();
                $scope.openReduccion= false;
            }else{
                toastr.error(data.msg);//mensaje de exito
            }
        });
	}
    
    $scope.avanzar=false;
    $scope.validate = function(num_factura){
        if (num_factura.rif && num_factura.num_factura) {
            webService.factura_servicio(num_factura).promise.success(function(data){
                if (data.success==false) {
                    toastr.error(data.msg);//mensaje de exito
                    $scope.avanzar=false;
                }else{
                    $scope.avanzar=true;
                }
            });   
        }
    }

    $scope.precio_vent = function(data){
        if (data.tipo == 'pieza') {
            $scope.data.precio_venta = parseInt( data.costo) +(parseInt(data.costo) * 0.30);
        }
    }


  }

})();
