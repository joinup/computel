/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.marca', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('marca', {
          url: '/marca',
          title: 'Configuración',
          templateUrl: 'app/pages/marca/marca_modelo.html',
          controller: 'Marca_modeloCtrl',
          sidebarMeta: {
            icon: 'fa fa-cog',
            order: 300,
          },
        });
  }

})();
