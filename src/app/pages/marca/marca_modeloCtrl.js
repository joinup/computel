/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.marca')
    .controller('Marca_modeloCtrl', Marca_modeloCtrl);

  /** @ngInject */
  function Marca_modeloCtrl($scope,$uibModal,webService,toastr,DTOptionsBuilder) {
  	$scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap().withLanguageSource('espanol.json');
  	cargarMarcas();
  	$scope.option='marcas';
  	function cargarMarcas(){
  		webService.getMarcasModelo().promise.success(function(data){
  		  if (data.success) {
  		  	$scope.marcas = data.data;
  		  }else{
  		    toastr.success('No existe marca registrada');
  		  }
  		});
  	}

	$scope.newMarca = function(){
		$scope.modal = $uibModal.open({
		  animation: true,
		  templateUrl: 'app/pages/marca/newMarca.html',
		  size: 'md',
		  scope:$scope,
		});
	}

	$scope.newBanco = function(){
		$scope.modal = $uibModal.open({
		  animation: true,
		  templateUrl: 'app/pages/marca/newBanco.html',
		  size: 'md',
		  scope:$scope,
		});
	}

	$scope.newModelo = function(){
		$scope.modal = $uibModal.open({
		  animation: true,
		  templateUrl: 'app/pages/marca/newModelo.html',
		  size: 'md',
		  scope:$scope,
		});
	}

	$scope.newCategoria = function(){
		$scope.modal = $uibModal.open({
		  animation: true,
		  templateUrl: 'app/pages/marca/newCategoria.html',
		  size: 'md',
		  scope:$scope,
		});
	}

	$scope.newTipo = function(){
		$scope.modal = $uibModal.open({
		  animation: true,
		  templateUrl: 'app/pages/marca/newTipoEquipo.html',
		  size: 'md',
		  scope:$scope,
		});
	}

	$scope.saveMarca = function(marca){
		if (marca.modelos) {
			var modelos = marca.modelos.split(',');
			marca.modelos = modelos;
		}
		webService.NewMarca(marca).promise.success(function(data){
		  if (data.success) {
		    toastr.success('registrado con exito');
		    $scope.modal.dismiss('ok');
		    //$scope.nuevaMarca = {};
  			$scope.option='marcas';
		    $scope.cargar('marcas');
		  }else{
		    toastr.success('La marca ya existe');
		  }
		});
	}

	$scope.saveModelo = function(marca){
		if (marca.modelos) {
			var modelos = marca.modelos.split(',');
			marca.modelos = modelos;
		}
		webService.NewModelo(marca).promise.success(function(data){
		  if (data.success) {
		    toastr.success('registrado con exito');
		    $scope.modal.dismiss('ok');
  			$scope.option='marcas';
		    $scope.cargar('marcas');
		  }else{
		    toastr.success('el Modelo ya existe');
		  }
		});
	}

	$scope.editMarca = function(marca){
      	//delete marca.modelos;
		webService.updateMarca(marca).promise.success(function(data){
			if (data.success) {
  				cargarMarcas();
			}else{
  		    	toastr.success('Error');
			}
		});
	}

	$scope.saveCategoria = function(cat){
		webService.newCategoria(cat).promise.success(function(data){
			if (data.success) {
  				$scope.categoria=data.data;
  		    	toastr.success(data.msg);
		    	$scope.modal.dismiss('ok');
  				$scope.option='categorias';
		    	$scope.cargar('categorias');
			}else{
  		    	toastr.success('Error');
			}
		});
	}

	$scope.saveTipoEquipo = function(tipo){
		webService.newTipoEquipo(tipo).promise.success(function(data){
			if (data.success) {
  				$scope.categoria=data.data;
  		    	toastr.success(data.msg);
		    	$scope.modal.dismiss('ok');
  				$scope.option='tipo';
		    	$scope.cargar('tipo');
			}else{
  		    	toastr.success('Error');
			}
		});
	}

	$scope.saveBanco = function(banco){
		webService.newBanco(banco).promise.success(function(data){
			if (data.success) {
  				//$scope.bancos=data.data;
  		    	toastr.success(data.msg);
		    	$scope.modal.dismiss('ok');
  				$scope.option='banco';
		    	$scope.cargar('banco');
			}else{
  		    	toastr.success('Error');
			}
		});
	}

	$scope.cargar = function(tipo){
		if (tipo == 'categorias') {
			categorias();
		}else if (tipo == 'tipo') {
			tipoEquipo();
		}else if (tipo == 'banco') {
			cargarBancos();
		}else{
			cargarMarcas();
		}
	}

	function categorias(){
		webService.categorias().promise.success(function(data){
			if (data.success) {
  				$scope.categorias=data.data;
			}else{
  		    	toastr.success('No hay categorías registradas');
			}
		});
	}

	function tipoEquipo(){
		webService.getTiposEquipo().promise.success(function(data){
			if (data.success) {
  				$scope.tipos=data.data;
			}else{
  		    	toastr.success('No hay tipo de equipos registradas');
			}
		});
	}

	function cargarBancos(){
		webService.bancos().promise.success(function(data){
			if (data.success==true) {
  				$scope.bancos=data.data;
			}else{
  		    	toastr.success('No hay Bancos registrados');
			}
		});
	}
  }

})();
