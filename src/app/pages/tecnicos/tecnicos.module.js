 /**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

    angular.module('BlurAdmin.pages.tecnicos', [])
      .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('tecnicos', {
              url: '/tecnicos',
              title: 'Tecnicos',
              templateUrl: 'app/pages/tecnicos/tecnicos.html',
              controller: 'TecnicosCtrl',
              sidebarMeta: {
                icon: 'ion-settings',
                order: 5,
              },
        });
    }

})();

