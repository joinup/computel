/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.ordenes')
    .controller('OrdenesCtrl', OrdenesCtrl);

  /** @ngInject */

  function OrdenesCtrl(webService,$scope) {
	
	init();

  	function init(){

  		webService.ordenes().promise.success(function(data){
        if (data.success) {
          $scope.ordenes = data.data;
          console.log($scope.ordenes);
  			}
  		});
  	}
  }

})();
