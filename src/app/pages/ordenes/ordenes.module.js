/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

    angular.module('BlurAdmin.pages.ordenes', [])
      .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('ordenes', {
              url: '/ordenes',
              title: 'Inicio',
              templateUrl: 'app/pages/ordenes/ordenes.html',
              controller: 'OrdenesCtrl',
              sidebarMeta: {
                icon: 'ion-home',
                order: 0,
              },
        });
    }

})();
