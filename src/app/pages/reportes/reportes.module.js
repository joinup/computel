/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.reportes', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('reportes', {
          url: '/reportes',
          title: 'Reportes',
          templateUrl: 'app/pages/reportes/reportes.html',
          controller: 'ReportesCtrl',
          sidebarMeta: {
            icon: 'fa fa-file-text-o',
            order: 10,
          },
        });
  }

})();
