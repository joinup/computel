/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.reportes')
    .controller('ReportesCtrl', ReportesCtrl);

  /** @ngInject */
  function ReportesCtrl($scope,$uibModal,webService,toastr) {

	$scope.dateOptions = {
	    maxDate : new Date()
	};
	$scope.format = 'dd-MMMM-yyyy';
	$scope.popup1 = {
	    opened: false
	};
	$scope.popup2 = {
	    opened: false
	};
	$scope.altInputFormats = ['M!/d!/yyyy'];
	$scope.open1 = function() {
	  $scope.popup1.opened = true;
	};

	$scope.open2 = function() {
	  $scope.popup2.opened = true;
	};
	var total = 0;

  $scope.generarReporte = function(date){
    if (date.tipoReport=='reporte1') {
      reporte1(date);
    }else if (date.tipoReport=='reporte2') {
      reporte2(date);
    }else if (date.tipoReport=='reporte3') {
      reporte3(date);
    }else if (date.tipoReport=='reporte4') {
      reporte4(date);
    }else if (date.tipoReport=='reporte5') {
      reporte5(date);
    }else if (date.tipoReport=='comisiones') {
      comisiones(date);
    }
  }

    function reporte1(date){
  		date.startdate=moment(date.startdate).format('YYYY-MM-DD');
  		date.enddate=moment(date.enddate).format('YYYY-MM-DD');
    		
  		webService.reporte1(date).promise.success(function(data){
  			if (data.success) {
  				toastr.success('Reporte Generado');
  				window.open('../include/pdf/'+data.data,'_black')
  			} 

  		});
    }

  function reporte2(date){
		date.startdate=moment(date.startdate).format('YYYY-MM-DD');
		date.enddate=moment(date.enddate).format('YYYY-MM-DD');
  		
		webService.reporte2(date).promise.success(function(data){
			if (data.success) {
				toastr.success('Reporte Generado');
				window.open('../include/pdf/'+data.data,'_black')
			} 

		});
  }

  function reporte3(date){
    webService.reporte3(date).promise.success(function(data){
      if (data.success) {
        toastr.success('Reporte Generado');
        window.open('../include/pdf/'+data.data,'_black')
      } 

    });
  }


  function reporte4(date){
    webService.reporte4(date).promise.success(function(data){
      if (data.success) {
        toastr.success('Reporte Generado');
        window.open('../include/pdf/'+data.data,'_black')
      } 

    });
  }

  function reporte5(date){
    date.startdate=moment(date.startdate).format('YYYY-MM-DD');
    date.enddate=moment(date.enddate).format('YYYY-MM-DD');
		webService.reporte5(date).promise.success(function(data){
			if (data.success) {
				toastr.success('Reporte Generado');
				window.open('../include/pdf/'+data.data,'_black')
			} 

		});
	}

	function comisiones(date){
		date.startdate=moment(date.startdate).format('YYYY-MM-DD');
		date.enddate=moment(date.enddate).format('YYYY-MM-DD');
		webService.comisiones(date).promise.success(function(data){
			if (data.success) {
				toastr.success('Reporte Generado');
				window.open('../include/pdf/'+data.data,'_black')
			} 

		});
	}




}

})();
