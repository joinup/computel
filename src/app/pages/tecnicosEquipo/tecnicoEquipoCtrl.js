/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
	'use strict';

	angular.module('BlurAdmin.pages.tecnicoEquipo')
	.controller('tecnicoEquipoCtrl', tecnicoEquipoCtrl);


	/** @ngInject */
	function tecnicoEquipoCtrl($scope, toastr ,fileReader,webService, $filter, $uibModal,editableOptions,editableThemes,DTOptionsBuilder) {
	
	$scope.dtOptions = DTOptionsBuilder
	.newOptions()
	.withBootstrap()
	//.withButtons(['print','excel'])
	.withLanguageSource('espanol.json');;

		$scope.piezasR = [];
		$scope.data={};
		$scope.ordenes = {};
		$scope.ordenes.estado='espera'

		$scope.EquiposTecnico = function(orden){
			webService.EquiposTecnico(orden).promise.success(function(data){
				if (data.success) {
					$scope.Equipos = data.data;
				}else{
					toastr.error(data.msg);
				}
			});
		}

		$scope.infoOrden = function(data){	
			$scope.info = data;
			$scope.modal = $uibModal.open({
		    animation: true,
		    templateUrl: 'app/pages/tecnicosEquipo/infoEquipo.html',
		    size: 'lg',
		    scope:$scope,
		    resolve: {
		      items: function () {
		        return $scope.items;
		      }
		    }
		  });
		}

		$scope.maxPieza = function(data){
			console.log(data);
			for (var i = 0; i < $scope.piezas.length; i++) {
				if ($scope.piezas[i].id_pieza==data){
					$scope.maximo = $scope.piezas[i].cantidad;
				}
			}
		}


		$scope.add = function(data){
			//$scope.data={};
			console.log(data)
			if (typeof(data.cantidad)!='undefined') {
				for (var i = 0; i < $scope.piezas.length; i++) {

					if ($scope.piezas[i].id_pieza==data.pieza) {
						$scope.piezasR.push({pieza:data.pieza, cantidad:data.cantidad, nombre:$scope.piezas[i].nombre, costo:$scope.piezas[i].precio_venta});
					}
				}
			}
		}

		$scope.EquiposTecnico($scope.ordenes);
		$scope.cambiarEstado = function(estado,item){
			if (estado=='reparado') {
				var get = {
					estado:estado,
					num_reparacion:$scope.data.num_reparacion,
					cedula_tecnico:$scope.data.cedula_tecnico,
					piezas:$scope.piezasR,
					observacion:item.observacion
				}
			}else{
				var get = {
					estado:estado,
					num_reparacion:item.num_reparacion,
					cedula_tecnico:item.cedula_tecnico
				}
			}
			webService.changeEstadoOrden(get).promise.success(function(data){
				if (data.success) {
					toastr.success('se ha actualizado la orden');
					$scope.ordenes.estado=estado;
					$scope.EquiposTecnico($scope.ordenes);
					$scope.modal.dismiss('ok'); //cerrar la modal
				}else{
					toastr.error(data.msg);
				}

			})
		}

		$scope.reparar = function (item) {
		  	$scope.data = item;
		  	$scope.piezasR=[];
		  	console.log($scope.data);
			$scope.modal = $uibModal.open({
		    animation: true,
		    templateUrl: 'app/pages/tecnicosEquipo/reparacion.html',
		    size: 'md',
		    scope:$scope,
		    resolve: {
		      items: function () {
		        return $scope.items;
		      }
		    }
		  });
		};
		function loadingPieces(){
			webService.inventario().promise.success(function(data){
			  if (data.success) {
			    $scope.piezas = data.data.piezas;
			  }
			});
		}
		loadingPieces();
		
		$scope.GuardarReparado = function(data){
			
		}

    	//esto es para los bontonees del datatable NO TOCAR
			// editableOptions.theme = 'bs3';
	  //   	editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary btn-with-icon"><i class="ion-checkmark-round"></i></button>';
	  //   	editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-default btn-with-icon"><i class="ion-close-round"></i></button>';

	}


})();
