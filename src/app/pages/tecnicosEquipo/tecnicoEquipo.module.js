/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.tecnicoEquipo', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('tecnicoEquipo', {
          url: '/tecnicoEquipo',
          title: 'Equipos Tecnico',
          templateUrl: 'app/pages/tecnicosEquipo/tecnicoEquipo.html',
          controller: 'tecnicoEquipoCtrl',
           sidebarMeta: {
                icon: 'ion-android-phone-portrait',
                order: 6,
            },
        });
  }

})();
