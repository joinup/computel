/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.cliente')
    .controller('ClienteCtrl', ClienteCtrl);

  /** @ngInject */
  function ClienteCtrl($scope, fileReader, $filter, $uibModal,webService, DTOptionsBuilder,editableOptions,editableThemes, toastr) {
	
	  $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap().withLanguageSource('espanol.json');;
    $scope.nuevoCliente = {};
    $scope.estados = ['activo', 'inactivo'];
    $scope.cliente={};
    $scope.cliente.estado='activo';

    $scope.cargarClientes = function(data){
      webService.clientes(data).promise.success(function(data){
        if (data.success) {
          $scope.client = data.data;
        }else{
          toastr.error('No existe clientes con esta categoría');
        }
      });
    }
    $scope.cargarClientes($scope.cliente);

    $scope.newClient = function () { //funcion para abrir la modal
      $scope.modal = $uibModal.open({
        animation: true,
        templateUrl: 'app/pages/cliente/ModalNewClient.html',
        size: 'lg',
        scope:$scope,
      });
    };

    $scope.validate = function (){
      webService.clientes($scope.nuevoCliente).promise.success(function(data){
        if (data.success) {
          toastr.error('El cliente ya existe');
          
        }
      });
    }

    $scope.saveClient = function(newClient){
      //newClient.estado = "activo";
      webService.registrarCliente(newClient).promise.success(function(data){
        if (data.success) {
          toastr.success('registrado con exito');//mensaje de exito
          $scope.modal.dismiss('ok'); //cerrar la modal
          $scope.nuevoCliente = {};//reinicia los campos de la modal
          $scope.cliente.estado='activo';
          $scope.cargarClientes($scope.cliente);//recarga los clientes
        }else{
          toastr.error('Error al registrar');//mensaje de error
        }
      })
    }
 
    function DialogController($scope, $uibModal) {
      $scope.cancel = function() {
        $$uibModal.cancel();
      };
      $scope.hide = function() {
        $$uibModal.hide();
      };
      $scope.answer = function(answer) {
        $$uibModal.hide(answer);
      };
    }

    $scope.deactivate = function(client){
      client.estado = "inactivo";
      webService.editarCliente(client).promise.success(function(data){
        if (data.success) {
          toastr.success('Desactivado con exito');//mensaje de exito
        }else{
          toastr.error('Error al desactivar');//mensaje de error
        }
      })
    }

    $scope.editClient = function(client){
      webService.editarCliente(client).promise.success(function(data){
        if (data.success) {
          toastr.success('Editado con exito');//mensaje de exito
          $scope.cliente.estado='activo';
          $scope.cargarClientes($scope.cliente);//recarga los clientes
        }else{
          toastr.error('Error al editar');//mensaje de error
        }
      })
    }

    $scope.check = function(data){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var validation = re.test(data);
        if (!validation) 
          return "Coloque un email valido";
      }
    $scope.tlf = function(data){
        var re = /^\d{11}$/;
        var valida = re.test(data);
        if (!valida) 
          return "Coloque solo 11 digitos";
      }
    

    //esto es para los bontonees del datatable NO TOCAR
    editableOptions.theme = 'bs3';
    editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary btn-with-icon"><i class="ion-checkmark-round"></i></button>';
    editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-default btn-with-icon"><i class="ion-close-round"></i></button>';


  }

})();
