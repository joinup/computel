/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.cliente', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('cliente', {
          url: '/cliente',
          title: 'Clientes',
          templateUrl: 'app/pages/cliente/cliente.html',
          controller: 'ClienteCtrl',
          sidebarMeta: {
            icon: 'ion-android-contacts',
            order: 3,
          },
        });
  }

})();
