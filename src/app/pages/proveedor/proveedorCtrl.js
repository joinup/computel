/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
	'use strict';

	angular.module('BlurAdmin.pages.proveedor')
	.controller('ProveedorCtrl', ProveedorCtrl);


	/** @ngInject */
	function ProveedorCtrl($scope, toastr ,fileReader,webService, $filter, $uibModal,editableOptions,editableThemes,DTOptionsBuilder) {
		
		$scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap().withLanguageSource('espanol.json');;

		$scope.cargarProveedores = function(){ //carga todos los proveedores de la base de datos
			webService.proveedores().promise.success(function(data){
				if (data.success) {
					$scope.proveedores = data.data;
				}
			});
		}
		$scope.cargarProveedores();
		$scope.nuevoProveedor = {}; //almacena todo lo que se va a aguardar en la modal de registrar
			
		// $scope.tipos = ['software', 'hardware', 'ambos'];//esto es para el select del editar
		// $scope.estados = ['activo', 'inactivo'];

		/// esto era para ordenar los select pero ya no es necesario porque la base de datos retorna el nombre del estado
	    /*$scope.showTipo = function(tecnico) { 
	        if(tecnico.tipo && $scope.tipos.length) { 
	          var selected = $filter('filter')($scope.tipos,tecnico.tipo);
			    return selected.length ? selected[0].text : 'Not set';
			  	}else return 'Not set'
			};

		
		$scope.showState = function(tecnico) { 
       		 if(tecnico.estado && $scope.estados.length) { 
				var selected = $filter('filter')($scope.estados,tecnico.estado);
		    	return selected.length ? selected[0].text : 'Not set';
		  	}else return 'Not set'
		};*/

		$scope.opened = {};

		$scope.open = function($event, elementOpened) { //para el editar NO TOCAR
			$event.preventDefault();
			$event.stopPropagation();
			$scope.opened[elementOpened] = !$scope.opened[elementOpened];
		}

		//editar tecnico
		$scope.editProveedor = function(proveedor){
			// console.log(tecnico);
			webService.editarProveedor(proveedor).promise.success(function(data){
				if (data.success) {
	       			toastr.success('Editado con exito');//mensaje de exito
	        	}else{
	        		toastr.error('Error al editar');//mensaje de error
	        	}
			});
		}

		$scope.newProveedor = function () { //funcion para abrir la modal
		  $scope.modal = $uibModal.open({
		    animation: true,
		    templateUrl: 'app/pages/proveedor/nuevoProveedor.html',
		    size: 'lg',
		    scope:$scope,
		    resolve: {
		      items: function () {
		        return $scope.items;
		      }
		    }
		  });

		};
		//Guardar Tecnico
		$scope.guardar= function(nuevoProveedor){
	        webService.registrarProveedor(nuevoProveedor).promise.success(function(data){
	        	if (data.success) {
	       			toastr.success('registrado con exito');//mensaje de exito
		 			$scope.modal.dismiss('ok'); //cerrar la modal
		 			$scope.nuevoProveedor = {};//reinicia los campos de la modal
		 			$scope.cargarProveedores();//recarga los tecnicos
	        	}else{
	        		toastr.error('Error al registrar');//mensaje de error
	        	}
	    	});
    	}

    	$scope.check = function(data){
    		console.log(data);
    		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    		var validation = re.test(data);
    		if (!validation) 
    			return "Coloque un email valido";
    	}

    	$scope.tlf = function(data){
    		console.log(data);
    		var re = /^\d{11}$/;

    		var valida = re.test(data);
    		if (!valida && data != null) 
    			return "Coloque solo 11 digitos";
    	}
		
    	//esto es para los bontonees del datatable NO TOCAR
		editableOptions.theme = 'bs3';
    	editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary btn-with-icon"><i class="ion-checkmark-round"></i></button>';
    	editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-default btn-with-icon"><i class="ion-close-round"></i></button>';

	}

})();
