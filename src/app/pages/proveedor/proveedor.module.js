/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.proveedor', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('proveedor', {
          url: '/proveedor',
          title: 'Proveedor',
          templateUrl: 'app/pages/proveedor/proveedor.html',
          controller: 'ProveedorCtrl',
           sidebarMeta: {
                icon: 'ion-android-contact',
                order: 4,
            },
        });
  }

})();
