/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('login')
    .controller('loginCtrl', loginCtrl);

  /** @ngInject */
function loginCtrl($scope,$rootScope, fileReader, $filter, $uibModal,webService,$location,toastr) {
    $rootScope.login = true;
    $scope.login = function(user){
    	webService.login(user).promise.success(function(info){
    		
    		if (info.success) {
           $rootScope.currentUser=info.data;
           console.log($rootScope.currentUser)
    			$rootScope.login = false;
    			toastr.success(info.msg)
				$location.path("/")
    		}else{
    			toastr.error('Intente de nuevo', 'Datos no validos');
    		}
    	});
    }
  }

})();
