(function () {
  'use strict';

  angular.module('BlurAdmin.pages.indicador')
    .controller('Indicador5Ctrl', Indicador5Ctrl);

  /** @ngInject */
  function Indicador5Ctrl($scope,webService) {
    $scope.dateOptions = {
          maxDate : new Date()
    };
    $scope.format = 'dd-MMMM-yyyy';
    
    $scope.popup1 = {
        opened: false
    };
    
    $scope.popup2 = {
        opened: false
    };
    
    $scope.altInputFormats = ['M!/d!/yyyy'];
   
    $scope.open1 = function() {
      $scope.popup1.opened = true;
    };
    
    $scope.open2 = function() {
        $scope.popup2.opened = true;
    };
    
    var total = 0;
    $scope.show=false;

    $scope.indicador5 = function(date){
      date.startdate=moment(date.startdate).format('YYYY-MM-DD');
      date.enddate=moment(date.enddate).format('YYYY-MM-DD');

      webService.indicador5(date).promise.success(function(data){
      $scope.show=true;

        var total2 = 0;
        var result = [];
        $scope.chartConfig.series = [];

        for (var i = 0; i < data.data.length; i++) {
          result.push(parseFloat(data.data[i]['a']));
          result.push(parseFloat(data.data[i]['b']));
          result.push(parseFloat(data.data[i]['c']));
          total2 +=data.data[i]['y'];
          $scope.chartConfig.series.push({
            name : data.data[i]['nombre'],
            data: result
          });
          result = [];
        };

        $scope.chartConfig.title={text: 'Satifacción Técnica del '+date.startdate+' / '+date.enddate};
        $scope.chartConfig.xAxis={
          categories: ['0-3 días','4-7 días','> 7 días']
        }
       
      });
    }

    $scope.chartConfig = {
      options: {
          chart: {
              type: 'column'
          },
          tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal} Piorcentaje: {point.percentage:.1f} %'
          },
          plotOptions: {
            column: {
              stacking: 'normal',
              dataLabels: {
                  enabled: true,
                  color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
              }
            }
          },
      },
      yAxis: {
            min: 0,
            title: {
                text: 'Total equipos reparados'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
    
    };



  }

})();
