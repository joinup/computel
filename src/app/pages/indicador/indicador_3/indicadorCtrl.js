(function () {
  'use strict';

  angular.module('BlurAdmin.pages.indicador')
    .controller('Indicador3Ctrl', Indicador3Ctrl);

  /** @ngInject */
  function Indicador3Ctrl($scope,webService) {
  	$scope.dateOptions = {
          maxDate : new Date()
    };
  	$scope.format = 'dd-MMMM-yyyy';
    
    $scope.popup1 = {
        opened: false
    };
    
    $scope.popup2 = {
        opened: false
    };
    
    $scope.altInputFormats = ['M!/d!/yyyy'];
   
    $scope.open1 = function() {
      $scope.popup1.opened = true;
    };
  	
    $scope.open2 = function() {
        $scope.popup2.opened = true;
    };
    
    var total = 0;
    $scope.show=false;

    $scope.indicador3 = function(date){
      date.startdate=moment(date.startdate).format('YYYY-MM-DD');
      date.enddate=moment(date.enddate).format('YYYY-MM-DD');


    	webService.indicador3(date).promise.success(function(data){
    		total = 0;
        $scope.show=true;

        var dat = [];
        $scope.xAxis=[];

        for (var i = 0; i < data.data.length; i++) {
          data.data[i]['y']=parseFloat(data.data[i]['y']);
          total +=parseInt(data.data[i]['y']);
        };
        $scope.chartConfig.title={text: 'Repuestos más usados del '+date.startdate+' / '+date.enddate+' total: '+total} 
        $scope.chartConfig.series = [{
            name: 'Pieza: ',
            colorByPoint: true,
            data:data.data
        }];
    	
    	});
    }

    $scope.chartConfig = {
      options: {
          chart: {
              type: 'pie'
          },
          tooltip: {
               pointFormat: '{series.name}{point.y}<br> <b>{point.percentage:.1f}%</b>'
          },
          plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    /*style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }*/
                }
            }
        },
      },
      loading: false,
      xAxis: {
      currentMin: 0,
      currentMax: 20,
      title: {text: 'values'}
      },
    
    };


  }

})();
