/**
 * @author k.danovsky
 * created on 15.01.2016
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.indicador', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('indicador', {
          url: '/indicador',
          template : '<ui-view></ui-view>',
          abstract: true,
          title: 'Indicadores',
          sidebarMeta: {
            icon: 'fa fa-bar-chart',
            order: 250,
          },
        })
        .state('indicador.indicador_1', {
          url: '/indicador_1',
          templateUrl: 'app/pages/indicador/indicador_1/indicador_1.html',
          controller: 'Indicador1Ctrl',
          title: 'Reparación por falla',
          sidebarMeta: {
            order: 1,
          },
        })
        .state('indicador.indicador_2', {
          url: '/indicador_2',
          templateUrl: 'app/pages/indicador/indicador_2/indicador.html',
          controller: 'IndicadorCtrl',
          title: 'Equipos reparados',
          sidebarMeta: {
            order: 2,
          },
        })
        .state('indicador.indicador_3', {
          url: '/indicador_3',
          templateUrl: 'app/pages/indicador/indicador_3/indicador_3.html',
          controller: 'Indicador3Ctrl',
          title: 'Repuestos más utilizados',
          sidebarMeta: {
            order: 3,
          },
        })
        .state('indicador.indicador_4', {
          url: '/indicador_4',
          templateUrl: 'app/pages/indicador/indicador_4/indicador.html',
          controller: 'Indicador4Ctrl',
          title: 'Efectividad Técnica',
          sidebarMeta: {
            order: 4,
          },
        })
        .state('indicador.indicador_5', {
          url: '/indicador_5',
          templateUrl: 'app/pages/indicador/indicador_5/indicador.html',
          controller: 'Indicador5Ctrl',
          title: 'Satisfacción Técnica',
          sidebarMeta: {
            order: 5,
          },
        })
        .state('indicador.indicador_6', {
          url: '/indicador_6',
          templateUrl: 'app/pages/indicador/indicador_6/indicador.html',
          controller: 'Indicador6Ctrl',
          title: 'Modalidad de pago',
          sidebarMeta: {
            order: 6,
          },
        });
  }

})();
