(function () {
  'use strict';

  angular.module('BlurAdmin.pages.indicador')
    .controller('Indicador6Ctrl', Indicador6Ctrl);

  /** @ngInject */
  function Indicador6Ctrl($scope,webService) {
    $scope.dateOptions = {
          maxDate : new Date()
    };
    $scope.format = 'dd-MMMM-yyyy';
    
    $scope.popup1 = {
        opened: false
    };
    
    $scope.popup2 = {
        opened: false
    };
    
    $scope.altInputFormats = ['M!/d!/yyyy'];
   
    $scope.open1 = function() {
      $scope.popup1.opened = true;
    };
    
    $scope.open2 = function() {
        $scope.popup2.opened = true;
    };
    
    var total = 0;
    $scope.show=false;


    $scope.indicador6 = function(date){
        date.startdate=moment(date.startdate).format('YYYY-MM-DD');
        date.enddate=moment(date.enddate).format('YYYY-MM-DD');
        webService.indicador6(date).promise.success(function(data){
          total = 0;
          $scope.show=true;

          for (var i = 0; i < data.data.length; i++) {
            data.data[i]['y']=parseFloat(data.data[i]['y']);
            total +=data.data[i]['y'];
          };
          $scope.chartConfig.title={text: 'Modalidad de pago del '+date.startdate+' / '+date.enddate+'  total: '+total} 
          $scope.chartConfig.series = [{
                name: 'Cantidad: ',
                colorByPoint: true,
                data:data.data
            }];
        });
    }

    $scope.chartConfig = {
      options: {
          chart: {
              type: 'pie'
          },
          tooltip: {
              /*style: {
                  padding: 10,
                  fontWeight: 'bold'
              },*/
               pointFormat: '{series.name}{point.y}<br> <b>{point.percentage:.1f}%</b>'
          },
          plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    /*style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }*/
                }
            }
        },
      },
      loading: false,
      xAxis: {
      currentMin: 0,
      currentMax: 20,
      title: {text: 'values'}
      },
    
    };



  }

})();
