(function () {
  'use strict';

  angular.module('BlurAdmin.pages.indicador')
    .controller('Indicador1Ctrl', Indicador1Ctrl);

  /** @ngInject */
  function Indicador1Ctrl($scope,webService) {
  	$scope.dateOptions = {
          maxDate : new Date()
    };
  	$scope.format = 'dd-MMMM-yyyy';
      $scope.popup1 = {
          opened: false
      };
      $scope.popup2 = {
          opened: false
      };
      $scope.altInputFormats = ['M!/d!/yyyy'];
      $scope.open1 = function() {
        $scope.popup1.opened = true;
      };
  	
    $scope.open2 = function() {
        $scope.popup2.opened = true;
    };
    var total = 0;
    $scope.show=false;

    $scope.indicador1 = function(date){
      date.startdate=moment(date.startdate).format('YYYY-MM-DD');
      date.enddate=moment(date.enddate).format('YYYY-MM-DD');
    	webService.indicador1(date).promise.success(function(data){
        $scope.show=true;
    		total = 0;
    		for (var i = 0; i < data.data.length; i++) {
    			data.data[i]['y']=parseFloat(data.data[i]['y']);
    			total +=data.data[i]['y'];
    		};
    		$scope.chartConfig.title={text: 'Reparación por falla del '+date.startdate+' / '+date.enddate+' </br> total: '+total}	
    		$scope.chartConfig.series = [{
            	name: 'Cantidad: ',
           	 	colorByPoint: true,
            	data:data.data
        	}];
    	});
    }

    $scope.chartConfig = {
      options: {
          chart: {
              type: 'pie'
          },
          tooltip: {
              /*style: {
                  padding: 10,
                  fontWeight: 'bold'
              },*/
               pointFormat: '{series.name}{point.y}<br> <b>{point.percentage:.1f}%</b>'
          },
        	plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    /*style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }*/
                }
            }
        },
      },
      loading: false,
      xAxis: {
      currentMin: 0,
      currentMax: 20,
      title: {text: 'values'}
      },
 
    };
    $scope.chartConfigBar={
        options: {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            plotOptions: {
                column: {
                    stacking: 'percent'
                }
            },
        },
        
        yAxis: {
            min: 0,
            title: {
                text: 'Total'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },

    }



  }

})();
