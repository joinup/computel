--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: estado; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN estado AS character varying(8)
	CONSTRAINT estado_check CHECK (((VALUE)::text = ANY (ARRAY[('activo'::character varying)::text, ('inactivo'::character varying)::text])));


ALTER DOMAIN public.estado OWNER TO postgres;

--
-- Name: estado_activo; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN estado_activo AS character varying(8)
	CONSTRAINT estado_activo_check CHECK (((VALUE)::text = ANY (ARRAY[('activo'::character varying)::text, ('dañado'::character varying)::text, ('inactivo'::character varying)::text])));


ALTER DOMAIN public.estado_activo OWNER TO postgres;

--
-- Name: estado_equipo; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN estado_equipo AS character varying(10)
	CONSTRAINT estado_equipo_check CHECK (((VALUE)::text = ANY (ARRAY[('espera'::character varying)::text, ('procesado'::character varying)::text, ('reparado'::character varying)::text, ('cancelado'::character varying)::text])));


ALTER DOMAIN public.estado_equipo OWNER TO postgres;

--
-- Name: estado_factura; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN estado_factura AS character varying(8)
	CONSTRAINT estado_factura_check CHECK (((VALUE)::text = ANY (ARRAY[('pagada'::character varying)::text, ('anulada'::character varying)::text])));


ALTER DOMAIN public.estado_factura OWNER TO postgres;

--
-- Name: estado_orden; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN estado_orden AS character varying(10)
	CONSTRAINT estado_orden_check CHECK (((VALUE)::text = ANY (ARRAY[('revision'::character varying)::text, ('devuelto'::character varying)::text, ('proceso'::character varying)::text, ('reparado'::character varying)::text, ('entregado'::character varying)::text])));


ALTER DOMAIN public.estado_orden OWNER TO postgres;

--
-- Name: forma_pago; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN forma_pago AS character varying(15)
	CONSTRAINT forma_pago_check CHECK (((VALUE)::text = ANY (ARRAY[('efectivo'::character varying)::text, ('debito'::character varying)::text, ('credito'::character varying)::text, ('transferencia'::character varying)::text])));


ALTER DOMAIN public.forma_pago OWNER TO postgres;

--
-- Name: tipo_activo; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN tipo_activo AS character varying(15)
	CONSTRAINT tipo_activo_check CHECK (((VALUE)::text = ANY (ARRAY[('herramienta'::character varying)::text, ('mobiliario'::character varying)::text])));


ALTER DOMAIN public.tipo_activo OWNER TO postgres;

--
-- Name: tipo_tecnico; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN tipo_tecnico AS character varying(8)
	CONSTRAINT tipo_tecnico_check CHECK (((VALUE)::text = ANY (ARRAY[('software'::character varying)::text, ('hardware'::character varying)::text, ('ambos'::character varying)::text])));


ALTER DOMAIN public.tipo_tecnico OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accesorios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE accesorios (
    num_reparacion integer NOT NULL,
    cargador boolean NOT NULL,
    pila boolean NOT NULL,
    audifonos boolean NOT NULL,
    tarjeta_memoria boolean NOT NULL,
    sim_car boolean NOT NULL,
    otro character varying(300),
    tapa boolean
);


ALTER TABLE public.accesorios OWNER TO postgres;

--
-- Name: asigna_orden_tecnico; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE asigna_orden_tecnico (
    num_reparacion integer NOT NULL,
    cedula_tecnico character varying(20) NOT NULL,
    fecha_inicio date,
    fecha_fin date,
    estado character varying(10) DEFAULT 'espera'::character varying,
    observacion character varying(100),
    comision numeric(16,2) DEFAULT 0.00,
    CONSTRAINT asigna_orden_tecnico_estado_check CHECK (((estado)::text = ANY (ARRAY[('espera'::character varying)::text, ('procesado'::character varying)::text, ('reparado'::character varying)::text, ('cancelado'::character varying)::text])))
);


ALTER TABLE public.asigna_orden_tecnico OWNER TO postgres;

--
-- Name: asigna_orden_tecnico_num_reparacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE asigna_orden_tecnico_num_reparacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.asigna_orden_tecnico_num_reparacion_seq OWNER TO postgres;

--
-- Name: asigna_orden_tecnico_num_reparacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE asigna_orden_tecnico_num_reparacion_seq OWNED BY asigna_orden_tecnico.num_reparacion;


--
-- Name: banco; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE banco (
    id integer NOT NULL,
    nombre character varying(20) NOT NULL
);


ALTER TABLE public.banco OWNER TO postgres;

--
-- Name: banco_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE banco_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.banco_id_seq OWNER TO postgres;

--
-- Name: banco_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE banco_id_seq OWNED BY banco.id;


--
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categoria (
    id integer NOT NULL,
    nombre character varying(40)
);


ALTER TABLE public.categoria OWNER TO postgres;

--
-- Name: categoria_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categoria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categoria_id_seq OWNER TO postgres;

--
-- Name: categoria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categoria_id_seq OWNED BY categoria.id;


--
-- Name: cliente; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cliente (
    tipo character varying(1) NOT NULL,
    cedula character varying(20) NOT NULL,
    nombre character varying(20) NOT NULL,
    apellido character varying(20) NOT NULL,
    direccion character varying(200) NOT NULL,
    tlf character varying(15) NOT NULL,
    email character varying(30),
    estado character varying(8) DEFAULT 'activo'::character varying,
    CONSTRAINT cliente_estado_check CHECK (((estado)::text = ANY (ARRAY[('activo'::character varying)::text, ('inactivo'::character varying)::text])))
);


ALTER TABLE public.cliente OWNER TO postgres;

--
-- Name: detalle_factura_compra; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE detalle_factura_compra (
    rif character varying(10) NOT NULL,
    num_factura integer NOT NULL,
    cantidad_comprado numeric(16,0) NOT NULL,
    costo_unitario numeric(16,2) NOT NULL,
    codigo_pieza character varying,
    codigo_herramienta integer,
    id integer NOT NULL
);


ALTER TABLE public.detalle_factura_compra OWNER TO postgres;

--
-- Name: equipo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE equipo (
    serial_equipo character varying(30) NOT NULL,
    id_modelo integer NOT NULL,
    id_marca integer NOT NULL,
    codigo_tipo integer NOT NULL,
    observaciones character varying(100)
);


ALTER TABLE public.equipo OWNER TO postgres;

--
-- Name: factura_compra; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE factura_compra (
    num_factura integer NOT NULL,
    rif character varying(10) NOT NULL,
    fecha_compra date NOT NULL,
    monto_total numeric(16,2),
    tipo_pago character varying(15),
    num_referencia character varying,
    num_tarjeta character varying,
    banco integer,
    CONSTRAINT factura_compra_tipo_pago_check CHECK (((tipo_pago)::text = ANY (ARRAY[('efectivo'::character varying)::text, ('debito'::character varying)::text, ('credito'::character varying)::text, ('transferencia'::character varying)::text])))
);


ALTER TABLE public.factura_compra OWNER TO postgres;

--
-- Name: factura_compra_num_factura_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE factura_compra_num_factura_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.factura_compra_num_factura_seq OWNER TO postgres;

--
-- Name: factura_compra_num_factura_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE factura_compra_num_factura_seq OWNED BY factura_compra.num_factura;


--
-- Name: factura_servicio; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE factura_servicio (
    num_factura integer NOT NULL,
    num_control integer NOT NULL,
    cedula_cliente character varying(20) NOT NULL,
    num_reparacion integer NOT NULL,
    fecha date NOT NULL,
    descripcion character varying(200) NOT NULL,
    monto numeric(16,2),
    estado_factura character varying(8) NOT NULL,
    tipo_pago character varying(15),
    num_tarjeta character varying,
    num_transferencia character varying,
    banco integer,
    CONSTRAINT factura_servicio_estado_factura_check CHECK (((estado_factura)::text = ANY (ARRAY[('pagada'::character varying)::text, ('anulada'::character varying)::text]))),
    CONSTRAINT factura_servicio_tipo_pago_check CHECK (((tipo_pago)::text = ANY (ARRAY[('efectivo'::character varying)::text, ('debito'::character varying)::text, ('credito'::character varying)::text, ('transferencia'::character varying)::text])))
);


ALTER TABLE public.factura_servicio OWNER TO postgres;

--
-- Name: factura_servicio_num_factura_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE factura_servicio_num_factura_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.factura_servicio_num_factura_seq OWNER TO postgres;

--
-- Name: factura_servicio_num_factura_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE factura_servicio_num_factura_seq OWNED BY factura_servicio.num_factura;


--
-- Name: herramientas_mobiliario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE herramientas_mobiliario (
    tipo character varying(15) NOT NULL,
    id integer NOT NULL,
    nombre character varying(30) NOT NULL,
    unidad character varying(30),
    cantidad numeric(10,0),
    valor_ref numeric(16,2),
    estado character varying(8) DEFAULT 'activo'::character varying,
    CONSTRAINT herramientas_mobiliario_estado_check CHECK (((estado)::text = ANY (ARRAY[('activo'::character varying)::text, ('dañado'::character varying)::text, ('inactivo'::character varying)::text]))),
    CONSTRAINT herramientas_mobiliario_tipo_check CHECK (((tipo)::text = ANY (ARRAY[('herramienta'::character varying)::text, ('mobiliario'::character varying)::text])))
);


ALTER TABLE public.herramientas_mobiliario OWNER TO postgres;

--
-- Name: herramientas_mobiliario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE herramientas_mobiliario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.herramientas_mobiliario_id_seq OWNER TO postgres;

--
-- Name: herramientas_mobiliario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE herramientas_mobiliario_id_seq OWNED BY herramientas_mobiliario.id;


--
-- Name: marca; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE marca (
    id integer NOT NULL,
    nombre character varying(20) NOT NULL,
    estado character varying(8) DEFAULT 'activo'::character varying,
    CONSTRAINT marca_estado_check CHECK (((estado)::text = ANY (ARRAY[('activo'::character varying)::text, ('inactivo'::character varying)::text])))
);


ALTER TABLE public.marca OWNER TO postgres;

--
-- Name: modelo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE modelo (
    id integer NOT NULL,
    id_marca integer NOT NULL,
    nombre character varying(20) NOT NULL,
    estado character varying(8) DEFAULT 'activo'::character varying,
    CONSTRAINT modelo_estado_check CHECK (((estado)::text = ANY (ARRAY[('activo'::character varying)::text, ('inactivo'::character varying)::text])))
);


ALTER TABLE public.modelo OWNER TO postgres;

--
-- Name: info_equipo; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW info_equipo AS
 SELECT a.serial_equipo,
    a.id_modelo,
    a.id_marca,
    a.codigo_tipo,
    a.observaciones,
    b.nombre AS marca,
    c.nombre AS modelo
   FROM ((equipo a
     JOIN marca b ON ((a.id_marca = b.id)))
     JOIN modelo c ON ((a.id_modelo = c.id)));


ALTER TABLE public.info_equipo OWNER TO postgres;

--
-- Name: marca_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE marca_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.marca_id_seq OWNER TO postgres;

--
-- Name: marca_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE marca_id_seq OWNED BY marca.id;


--
-- Name: modelo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE modelo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modelo_id_seq OWNER TO postgres;

--
-- Name: modelo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE modelo_id_seq OWNED BY modelo.id;


--
-- Name: orden_reparacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE orden_reparacion (
    num_reparacion integer NOT NULL,
    fecha date NOT NULL,
    serial_equipo character varying(30) NOT NULL,
    cedula_cliente character varying(20) NOT NULL,
    falla character varying(50),
    descripcion character varying(200),
    estado character varying(10) DEFAULT 'revision'::character varying,
    CONSTRAINT orden_reparacion_estado_check CHECK (((estado)::text = ANY (ARRAY[('revision'::character varying)::text, ('devuelto'::character varying)::text, ('proceso'::character varying)::text, ('reparado'::character varying)::text, ('entregado'::character varying)::text])))
);


ALTER TABLE public.orden_reparacion OWNER TO postgres;

--
-- Name: orden_reparacion_num_reparacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE orden_reparacion_num_reparacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orden_reparacion_num_reparacion_seq OWNER TO postgres;

--
-- Name: orden_reparacion_num_reparacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE orden_reparacion_num_reparacion_seq OWNED BY orden_reparacion.num_reparacion;


--
-- Name: pieza_modelo_marca; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pieza_modelo_marca (
    id_marca integer NOT NULL,
    id_modelo integer NOT NULL,
    id_pieza character varying NOT NULL
);


ALTER TABLE public.pieza_modelo_marca OWNER TO postgres;

--
-- Name: piezas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE piezas (
    id_pieza character varying NOT NULL,
    nombre character varying(30) NOT NULL,
    descripcion character varying(200),
    unidad character varying(30),
    medida character varying(30),
    cantidad numeric(10,0),
    costo numeric(16,2),
    precio_venta numeric(16,2),
    estado character varying(8) DEFAULT 'activo'::character varying,
    categoria integer,
    CONSTRAINT piezas_estado_check CHECK (((estado)::text = ANY ((ARRAY['activo'::character varying, 'inactivo'::character varying])::text[])))
);


ALTER TABLE public.piezas OWNER TO postgres;

--
-- Name: proveedor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE proveedor (
    letra character varying(2) NOT NULL,
    rif character varying(10) NOT NULL,
    nombre_prov character varying(20) NOT NULL,
    direccion character varying(200),
    tlf character varying(15),
    nombre_resp character varying(20),
    tlf_resp character varying(15),
    email character varying(30)
);


ALTER TABLE public.proveedor OWNER TO postgres;

--
-- Name: tecnico; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tecnico (
    cedula character varying(20) NOT NULL,
    nombre character varying(20) NOT NULL,
    apellido character varying(20) NOT NULL,
    direccion character varying(200),
    tlf character varying(15),
    email character varying(30),
    tipo character varying(8),
    estado character varying(8) DEFAULT 'activo'::character varying,
    CONSTRAINT tecnico_estado_check CHECK (((estado)::text = ANY (ARRAY[('activo'::character varying)::text, ('inactivo'::character varying)::text]))),
    CONSTRAINT tecnico_tipo_check CHECK (((tipo)::text = ANY (ARRAY[('software'::character varying)::text, ('hardware'::character varying)::text, ('ambos'::character varying)::text])))
);


ALTER TABLE public.tecnico OWNER TO postgres;

--
-- Name: tipo_equipo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipo_equipo (
    codigo_tipo integer NOT NULL,
    nombre character varying(20) NOT NULL,
    estado character varying(8) DEFAULT 'activo'::character varying,
    CONSTRAINT tipo_equipo_estado_check CHECK (((estado)::text = ANY (ARRAY[('activo'::character varying)::text, ('inactivo'::character varying)::text])))
);


ALTER TABLE public.tipo_equipo OWNER TO postgres;

--
-- Name: tipo_equipo_codigo_tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipo_equipo_codigo_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_equipo_codigo_tipo_seq OWNER TO postgres;

--
-- Name: tipo_equipo_codigo_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipo_equipo_codigo_tipo_seq OWNED BY tipo_equipo.codigo_tipo;


--
-- Name: uso_piezas_reparacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE uso_piezas_reparacion (
    num_reparacion integer NOT NULL,
    id_pieza character varying NOT NULL,
    cantidad numeric(16,0) DEFAULT 1,
    costo numeric(16,2) NOT NULL
);


ALTER TABLE public.uso_piezas_reparacion OWNER TO postgres;

--
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    cedula character varying(40) NOT NULL,
    clave character varying(50) NOT NULL,
    tipo character varying(10),
    nombre character varying(30),
    apellido character varying(30),
    direccion character varying(50),
    estado character varying DEFAULT 'activo'::character varying
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- Name: num_reparacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY asigna_orden_tecnico ALTER COLUMN num_reparacion SET DEFAULT nextval('asigna_orden_tecnico_num_reparacion_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY banco ALTER COLUMN id SET DEFAULT nextval('banco_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoria ALTER COLUMN id SET DEFAULT nextval('categoria_id_seq'::regclass);


--
-- Name: num_factura; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY factura_compra ALTER COLUMN num_factura SET DEFAULT nextval('factura_compra_num_factura_seq'::regclass);


--
-- Name: num_factura; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY factura_servicio ALTER COLUMN num_factura SET DEFAULT nextval('factura_servicio_num_factura_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY herramientas_mobiliario ALTER COLUMN id SET DEFAULT nextval('herramientas_mobiliario_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY marca ALTER COLUMN id SET DEFAULT nextval('marca_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modelo ALTER COLUMN id SET DEFAULT nextval('modelo_id_seq'::regclass);


--
-- Name: num_reparacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orden_reparacion ALTER COLUMN num_reparacion SET DEFAULT nextval('orden_reparacion_num_reparacion_seq'::regclass);


--
-- Name: codigo_tipo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipo_equipo ALTER COLUMN codigo_tipo SET DEFAULT nextval('tipo_equipo_codigo_tipo_seq'::regclass);


--
-- Name: pk_banco; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY banco
    ADD CONSTRAINT pk_banco PRIMARY KEY (id);


--
-- Name: pk_detalle_factura_compra; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY detalle_factura_compra
    ADD CONSTRAINT pk_detalle_factura_compra PRIMARY KEY (rif, num_factura, id);


--
-- Name: pk_equipo; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY equipo
    ADD CONSTRAINT pk_equipo PRIMARY KEY (serial_equipo);


--
-- Name: pk_factura_compra; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY factura_compra
    ADD CONSTRAINT pk_factura_compra PRIMARY KEY (num_factura, rif);


--
-- Name: pk_factura_servicio; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY factura_servicio
    ADD CONSTRAINT pk_factura_servicio PRIMARY KEY (num_factura, cedula_cliente, num_reparacion);


--
-- Name: pk_herramientas_mobiliario; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY herramientas_mobiliario
    ADD CONSTRAINT pk_herramientas_mobiliario PRIMARY KEY (id);


--
-- Name: pk_marca; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY marca
    ADD CONSTRAINT pk_marca PRIMARY KEY (id);


--
-- Name: pk_modelo; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY modelo
    ADD CONSTRAINT pk_modelo PRIMARY KEY (id, id_marca);


--
-- Name: pk_orden_reparacion; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY orden_reparacion
    ADD CONSTRAINT pk_orden_reparacion PRIMARY KEY (num_reparacion);


--
-- Name: pk_pieza_modelo_marca; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pieza_modelo_marca
    ADD CONSTRAINT pk_pieza_modelo_marca PRIMARY KEY (id_pieza, id_modelo, id_marca);


--
-- Name: pk_piezas; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY piezas
    ADD CONSTRAINT pk_piezas PRIMARY KEY (id_pieza);


--
-- Name: pk_proveedor; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY proveedor
    ADD CONSTRAINT pk_proveedor PRIMARY KEY (rif);


--
-- Name: pk_tecnico; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tecnico
    ADD CONSTRAINT pk_tecnico PRIMARY KEY (cedula);


--
-- Name: pk_tipo_equipo; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipo_equipo
    ADD CONSTRAINT pk_tipo_equipo PRIMARY KEY (codigo_tipo);


--
-- Name: pk_uso_piezas_reparacion; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY uso_piezas_reparacion
    ADD CONSTRAINT pk_uso_piezas_reparacion PRIMARY KEY (num_reparacion, id_pieza);


--
-- Name: pk_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT pk_usuario PRIMARY KEY (cedula);


--
-- Name: factura_compra_banco_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY factura_compra
    ADD CONSTRAINT factura_compra_banco_fkey FOREIGN KEY (banco) REFERENCES banco(id);


--
-- Name: factura_servicio_banco_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY factura_servicio
    ADD CONSTRAINT factura_servicio_banco_fkey FOREIGN KEY (banco) REFERENCES banco(id);


--
-- Name: fk_factura_compra; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detalle_factura_compra
    ADD CONSTRAINT fk_factura_compra FOREIGN KEY (num_factura, rif) REFERENCES factura_compra(num_factura, rif);


--
-- Name: fk_marca_modelo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modelo
    ADD CONSTRAINT fk_marca_modelo FOREIGN KEY (id_marca) REFERENCES marca(id);


--
-- Name: fk_modelo_equipo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY equipo
    ADD CONSTRAINT fk_modelo_equipo FOREIGN KEY (id_modelo, id_marca) REFERENCES modelo(id, id_marca);


--
-- Name: fk_modelo_marca_pieza; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pieza_modelo_marca
    ADD CONSTRAINT fk_modelo_marca_pieza FOREIGN KEY (id_marca, id_modelo) REFERENCES modelo(id_marca, id);


--
-- Name: fk_orden_reparacion_accesorios; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accesorios
    ADD CONSTRAINT fk_orden_reparacion_accesorios FOREIGN KEY (num_reparacion) REFERENCES orden_reparacion(num_reparacion);


--
-- Name: fk_orden_reparacion_servicio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY factura_servicio
    ADD CONSTRAINT fk_orden_reparacion_servicio FOREIGN KEY (num_reparacion) REFERENCES orden_reparacion(num_reparacion);


--
-- Name: fk_orden_reparacion_tecnico; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY asigna_orden_tecnico
    ADD CONSTRAINT fk_orden_reparacion_tecnico FOREIGN KEY (num_reparacion) REFERENCES orden_reparacion(num_reparacion);


--
-- Name: fk_orden_reparacion_uso_pieza; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uso_piezas_reparacion
    ADD CONSTRAINT fk_orden_reparacion_uso_pieza FOREIGN KEY (num_reparacion) REFERENCES orden_reparacion(num_reparacion);


--
-- Name: fk_piezas_uso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uso_piezas_reparacion
    ADD CONSTRAINT fk_piezas_uso FOREIGN KEY (id_pieza) REFERENCES piezas(id_pieza);


--
-- Name: fk_proveedor_factura; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY factura_compra
    ADD CONSTRAINT fk_proveedor_factura FOREIGN KEY (rif) REFERENCES proveedor(rif);


--
-- Name: fk_tecnico_asigna; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY asigna_orden_tecnico
    ADD CONSTRAINT fk_tecnico_asigna FOREIGN KEY (cedula_tecnico) REFERENCES tecnico(cedula);


--
-- Name: fk_tipo_equipo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY equipo
    ADD CONSTRAINT fk_tipo_equipo FOREIGN KEY (codigo_tipo) REFERENCES tipo_equipo(codigo_tipo);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

