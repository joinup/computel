'use strict';
require('es6-promise').polyfill();

var gulp = require('gulp');
var inject = require('gulp-inject');
var wrench = require('wrench');

/**
 *  This will load all js or coffee files in the gulp directory
 *  in order to load all gulp tasks
 */
wrench.readdirSyncRecursive('./gulp').filter(function(file) {
  return (/\.(js|coffee)$/i).test(file);
}).map(function(file) {
  require('./gulp/' + file);
});
 var paths = {
     sass: ['./scss/**/*.scss'],
     javascript: [
         './www/**/*.js',
         '!./www/js/app.js',
         '!./www/lib/**'
     ],
     css: [
         './www/**/*.css',
         '!./www/css/ionic.app*.css',
         '!./www/lib/**'
     ]
 };
 gulp.task('index', function(){
     return gulp.src('./www/index.html')
         .pipe(inject(
             gulp.src(paths.javascript,
                 {read: false}), {relative: true}))
         .pipe(gulp.dest('./www'))
         .pipe(inject(
             gulp.src(paths.css,
             {read: false}), {relative: true}))
         .pipe(gulp.dest('./www'));
 });
  gulp.task('default', ['sass', 'index']);
 gulp.task('watch', function() {
     gulp.watch(paths.sass, ['sass']);
     gulp.watch([
     paths.javascript,
     paths.css
     ], ['index']);
 });
/**
 *  Default task clean temporaries directories and launch the
 *  main optimization build task
 */
gulp.task('default', ['clean'], function () {
  gulp.start('build');
});