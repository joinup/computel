--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: estado; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN estado AS character varying(8)
	CONSTRAINT estado_check CHECK (((VALUE)::text = ANY (ARRAY[('activo'::character varying)::text, ('inactivo'::character varying)::text])));


ALTER DOMAIN estado OWNER TO postgres;

--
-- Name: estado_activo; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN estado_activo AS character varying(8)
	CONSTRAINT estado_activo_check CHECK (((VALUE)::text = ANY (ARRAY[('activo'::character varying)::text, ('dañado'::character varying)::text, ('inactivo'::character varying)::text])));


ALTER DOMAIN estado_activo OWNER TO postgres;

--
-- Name: estado_equipo; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN estado_equipo AS character varying(10)
	CONSTRAINT estado_equipo_check CHECK (((VALUE)::text = ANY (ARRAY[('espera'::character varying)::text, ('procesado'::character varying)::text, ('reparado'::character varying)::text, ('cancelado'::character varying)::text])));


ALTER DOMAIN estado_equipo OWNER TO postgres;

--
-- Name: estado_factura; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN estado_factura AS character varying(8)
	CONSTRAINT estado_factura_check CHECK (((VALUE)::text = ANY (ARRAY[('pagada'::character varying)::text, ('anulada'::character varying)::text])));


ALTER DOMAIN estado_factura OWNER TO postgres;

--
-- Name: estado_orden; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN estado_orden AS character varying(10)
	CONSTRAINT estado_orden_check CHECK (((VALUE)::text = ANY (ARRAY[('revision'::character varying)::text, ('devuelto'::character varying)::text, ('proceso'::character varying)::text, ('reparado'::character varying)::text, ('entregado'::character varying)::text])));


ALTER DOMAIN estado_orden OWNER TO postgres;

--
-- Name: forma_pago; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN forma_pago AS character varying(15)
	CONSTRAINT forma_pago_check CHECK (((VALUE)::text = ANY (ARRAY[('efectivo'::character varying)::text, ('debito'::character varying)::text, ('credito'::character varying)::text, ('transferencia'::character varying)::text])));


ALTER DOMAIN forma_pago OWNER TO postgres;

--
-- Name: tipo_activo; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN tipo_activo AS character varying(15)
	CONSTRAINT tipo_activo_check CHECK (((VALUE)::text = ANY (ARRAY[('herramienta'::character varying)::text, ('mobiliario'::character varying)::text])));


ALTER DOMAIN tipo_activo OWNER TO postgres;

--
-- Name: tipo_tecnico; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN tipo_tecnico AS character varying(8)
	CONSTRAINT tipo_tecnico_check CHECK (((VALUE)::text = ANY (ARRAY[('software'::character varying)::text, ('hardware'::character varying)::text, ('ambos'::character varying)::text])));


ALTER DOMAIN tipo_tecnico OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accesorios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE accesorios (
    num_reparacion integer NOT NULL,
    cargador boolean NOT NULL,
    pila boolean NOT NULL,
    audifonos boolean NOT NULL,
    tarjeta_memoria boolean NOT NULL,
    sim_car boolean NOT NULL,
    otro character varying(300),
    tapa boolean
);


ALTER TABLE accesorios OWNER TO postgres;

--
-- Name: asigna_orden_tecnico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE asigna_orden_tecnico (
    num_reparacion integer NOT NULL,
    cedula_tecnico character varying(20) NOT NULL,
    fecha_inicio date,
    fecha_fin date,
    estado character varying(10) DEFAULT 'espera'::character varying,
    observacion character varying(100),
    comision numeric(16,2) DEFAULT 0.00,
    CONSTRAINT asigna_orden_tecnico_estado_check CHECK (((estado)::text = ANY (ARRAY[('espera'::character varying)::text, ('procesado'::character varying)::text, ('reparado'::character varying)::text, ('cancelado'::character varying)::text])))
);


ALTER TABLE asigna_orden_tecnico OWNER TO postgres;

--
-- Name: asigna_orden_tecnico_num_reparacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE asigna_orden_tecnico_num_reparacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE asigna_orden_tecnico_num_reparacion_seq OWNER TO postgres;

--
-- Name: asigna_orden_tecnico_num_reparacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE asigna_orden_tecnico_num_reparacion_seq OWNED BY asigna_orden_tecnico.num_reparacion;


--
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE categoria (
    id integer NOT NULL,
    nombre character varying(40)
);


ALTER TABLE categoria OWNER TO postgres;

--
-- Name: categoria_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categoria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categoria_id_seq OWNER TO postgres;

--
-- Name: categoria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categoria_id_seq OWNED BY categoria.id;


--
-- Name: cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cliente (
    tipo character varying(1) NOT NULL,
    cedula character varying(20) NOT NULL,
    nombre character varying(20) NOT NULL,
    apellido character varying(20) NOT NULL,
    direccion character varying(200) NOT NULL,
    tlf character varying(15) NOT NULL,
    email character varying(30),
    estado character varying(8) DEFAULT 'activo'::character varying,
    CONSTRAINT cliente_estado_check CHECK (((estado)::text = ANY (ARRAY[('activo'::character varying)::text, ('inactivo'::character varying)::text])))
);


ALTER TABLE cliente OWNER TO postgres;

--
-- Name: detalle_factura_compra; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE detalle_factura_compra (
    rif character varying(10) NOT NULL,
    num_factura integer NOT NULL,
    cantidad_comprado numeric(16,0) NOT NULL,
    costo_unitario numeric(16,2) NOT NULL,
    codigo_pieza integer,
    codigo_herramienta integer,
    id integer NOT NULL
);


ALTER TABLE detalle_factura_compra OWNER TO postgres;

--
-- Name: equipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE equipo (
    serial_equipo character varying(30) NOT NULL,
    id_modelo integer NOT NULL,
    id_marca integer NOT NULL,
    codigo_tipo integer NOT NULL,
    observaciones character varying(100)
);


ALTER TABLE equipo OWNER TO postgres;

--
-- Name: factura_compra; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE factura_compra (
    num_factura integer NOT NULL,
    rif character varying(10) NOT NULL,
    fecha_compra date NOT NULL,
    monto_total numeric(16,2),
    tipo_pago character varying(15),
    num_referencia character varying,
    banco character varying,
    num_tarjeta character varying,
    CONSTRAINT factura_compra_tipo_pago_check CHECK (((tipo_pago)::text = ANY (ARRAY[('efectivo'::character varying)::text, ('debito'::character varying)::text, ('credito'::character varying)::text, ('transferencia'::character varying)::text])))
);


ALTER TABLE factura_compra OWNER TO postgres;

--
-- Name: factura_compra_num_factura_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE factura_compra_num_factura_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE factura_compra_num_factura_seq OWNER TO postgres;

--
-- Name: factura_compra_num_factura_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE factura_compra_num_factura_seq OWNED BY factura_compra.num_factura;


--
-- Name: factura_servicio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE factura_servicio (
    num_factura integer NOT NULL,
    num_control integer NOT NULL,
    cedula_cliente character varying(20) NOT NULL,
    num_reparacion integer NOT NULL,
    fecha date NOT NULL,
    descripcion character varying(200) NOT NULL,
    monto numeric(16,2),
    estado_factura character varying(8) NOT NULL,
    tipo_pago character varying(15),
    banco character varying,
    num_tarjeta character varying,
    num_transferencia character varying,
    CONSTRAINT factura_servicio_estado_factura_check CHECK (((estado_factura)::text = ANY (ARRAY[('pagada'::character varying)::text, ('anulada'::character varying)::text]))),
    CONSTRAINT factura_servicio_tipo_pago_check CHECK (((tipo_pago)::text = ANY (ARRAY[('efectivo'::character varying)::text, ('debito'::character varying)::text, ('credito'::character varying)::text, ('transferencia'::character varying)::text])))
);


ALTER TABLE factura_servicio OWNER TO postgres;

--
-- Name: factura_servicio_num_factura_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE factura_servicio_num_factura_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE factura_servicio_num_factura_seq OWNER TO postgres;

--
-- Name: factura_servicio_num_factura_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE factura_servicio_num_factura_seq OWNED BY factura_servicio.num_factura;


--
-- Name: herramientas_mobiliario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE herramientas_mobiliario (
    tipo character varying(15) NOT NULL,
    id integer NOT NULL,
    nombre character varying(30) NOT NULL,
    unidad character varying(30),
    cantidad numeric(10,0),
    valor_ref numeric(16,2),
    estado character varying(8) DEFAULT 'activo'::character varying,
    CONSTRAINT herramientas_mobiliario_estado_check CHECK (((estado)::text = ANY (ARRAY[('activo'::character varying)::text, ('dañado'::character varying)::text, ('inactivo'::character varying)::text]))),
    CONSTRAINT herramientas_mobiliario_tipo_check CHECK (((tipo)::text = ANY (ARRAY[('herramienta'::character varying)::text, ('mobiliario'::character varying)::text])))
);


ALTER TABLE herramientas_mobiliario OWNER TO postgres;

--
-- Name: herramientas_mobiliario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE herramientas_mobiliario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE herramientas_mobiliario_id_seq OWNER TO postgres;

--
-- Name: herramientas_mobiliario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE herramientas_mobiliario_id_seq OWNED BY herramientas_mobiliario.id;


--
-- Name: marca; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE marca (
    id integer NOT NULL,
    nombre character varying(20) NOT NULL,
    estado character varying(8) DEFAULT 'activo'::character varying,
    CONSTRAINT marca_estado_check CHECK (((estado)::text = ANY (ARRAY[('activo'::character varying)::text, ('inactivo'::character varying)::text])))
);


ALTER TABLE marca OWNER TO postgres;

--
-- Name: modelo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE modelo (
    id integer NOT NULL,
    id_marca integer NOT NULL,
    nombre character varying(20) NOT NULL,
    estado character varying(8) DEFAULT 'activo'::character varying,
    CONSTRAINT modelo_estado_check CHECK (((estado)::text = ANY (ARRAY[('activo'::character varying)::text, ('inactivo'::character varying)::text])))
);


ALTER TABLE modelo OWNER TO postgres;

--
-- Name: info_equipo; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW info_equipo AS
 SELECT a.serial_equipo,
    a.id_modelo,
    a.id_marca,
    a.codigo_tipo,
    a.observaciones,
    b.nombre AS marca,
    c.nombre AS modelo
   FROM ((equipo a
     JOIN marca b ON ((a.id_marca = b.id)))
     JOIN modelo c ON ((a.id_modelo = c.id)));


ALTER TABLE info_equipo OWNER TO postgres;

--
-- Name: marca_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE marca_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE marca_id_seq OWNER TO postgres;

--
-- Name: marca_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE marca_id_seq OWNED BY marca.id;


--
-- Name: modelo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE modelo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE modelo_id_seq OWNER TO postgres;

--
-- Name: modelo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE modelo_id_seq OWNED BY modelo.id;


--
-- Name: orden_reparacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE orden_reparacion (
    num_reparacion integer NOT NULL,
    fecha date NOT NULL,
    serial_equipo character varying(30) NOT NULL,
    cedula_cliente character varying(20) NOT NULL,
    falla character varying(50),
    descripcion character varying(200),
    estado character varying(10) DEFAULT 'revision'::character varying,
    CONSTRAINT orden_reparacion_estado_check CHECK (((estado)::text = ANY (ARRAY[('revision'::character varying)::text, ('devuelto'::character varying)::text, ('proceso'::character varying)::text, ('reparado'::character varying)::text, ('entregado'::character varying)::text])))
);


ALTER TABLE orden_reparacion OWNER TO postgres;

--
-- Name: orden_reparacion_num_reparacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE orden_reparacion_num_reparacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE orden_reparacion_num_reparacion_seq OWNER TO postgres;

--
-- Name: orden_reparacion_num_reparacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE orden_reparacion_num_reparacion_seq OWNED BY orden_reparacion.num_reparacion;


--
-- Name: pieza_modelo_marca; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE pieza_modelo_marca (
    id_marca integer NOT NULL,
    id_modelo integer NOT NULL,
    id_pieza integer NOT NULL
);


ALTER TABLE pieza_modelo_marca OWNER TO postgres;

--
-- Name: piezas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE piezas (
    id_pieza integer NOT NULL,
    nombre character varying(30) NOT NULL,
    descripcion character varying(200) NOT NULL,
    unidad character varying(30),
    medida character varying(30),
    cantidad numeric(10,0),
    costo numeric(16,2),
    precio_venta numeric(16,2),
    estado character varying(8) DEFAULT 'activo'::character varying,
    categoria integer,
    CONSTRAINT piezas_estado_check CHECK (((estado)::text = ANY (ARRAY[('activo'::character varying)::text, ('inactivo'::character varying)::text])))
);


ALTER TABLE piezas OWNER TO postgres;

--
-- Name: piezas_id_pieza_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE piezas_id_pieza_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piezas_id_pieza_seq OWNER TO postgres;

--
-- Name: piezas_id_pieza_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE piezas_id_pieza_seq OWNED BY piezas.id_pieza;


--
-- Name: proveedor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE proveedor (
    letra character varying(2) NOT NULL,
    rif character varying(10) NOT NULL,
    nombre_prov character varying(20) NOT NULL,
    direccion character varying(200),
    tlf character varying(15),
    nombre_resp character varying(20),
    tlf_resp character varying(15),
    email character varying(30)
);


ALTER TABLE proveedor OWNER TO postgres;

--
-- Name: tecnico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tecnico (
    cedula character varying(20) NOT NULL,
    nombre character varying(20) NOT NULL,
    apellido character varying(20) NOT NULL,
    direccion character varying(200),
    tlf character varying(15),
    email character varying(30),
    tipo character varying(8),
    estado character varying(8) DEFAULT 'activo'::character varying,
    CONSTRAINT tecnico_estado_check CHECK (((estado)::text = ANY (ARRAY[('activo'::character varying)::text, ('inactivo'::character varying)::text]))),
    CONSTRAINT tecnico_tipo_check CHECK (((tipo)::text = ANY (ARRAY[('software'::character varying)::text, ('hardware'::character varying)::text, ('ambos'::character varying)::text])))
);


ALTER TABLE tecnico OWNER TO postgres;

--
-- Name: tipo_equipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tipo_equipo (
    codigo_tipo integer NOT NULL,
    nombre character varying(20) NOT NULL,
    estado character varying(8) DEFAULT 'activo'::character varying,
    CONSTRAINT tipo_equipo_estado_check CHECK (((estado)::text = ANY (ARRAY[('activo'::character varying)::text, ('inactivo'::character varying)::text])))
);


ALTER TABLE tipo_equipo OWNER TO postgres;

--
-- Name: tipo_equipo_codigo_tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipo_equipo_codigo_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipo_equipo_codigo_tipo_seq OWNER TO postgres;

--
-- Name: tipo_equipo_codigo_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipo_equipo_codigo_tipo_seq OWNED BY tipo_equipo.codigo_tipo;


--
-- Name: uso_piezas_reparacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE uso_piezas_reparacion (
    num_reparacion integer NOT NULL,
    id_pieza integer NOT NULL,
    cantidad numeric(16,0) NOT NULL,
    costo numeric(16,2)
);


ALTER TABLE uso_piezas_reparacion OWNER TO postgres;

--
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE usuario (
    cedula character varying(40) NOT NULL,
    clave character varying(50) NOT NULL,
    tipo character varying(10),
    nombre character varying(30),
    apellido character varying(30),
    direccion character varying(50),
    estado character varying DEFAULT 'activo'::character varying
);


ALTER TABLE usuario OWNER TO postgres;

--
-- Name: num_reparacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY asigna_orden_tecnico ALTER COLUMN num_reparacion SET DEFAULT nextval('asigna_orden_tecnico_num_reparacion_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoria ALTER COLUMN id SET DEFAULT nextval('categoria_id_seq'::regclass);


--
-- Name: num_factura; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY factura_compra ALTER COLUMN num_factura SET DEFAULT nextval('factura_compra_num_factura_seq'::regclass);


--
-- Name: num_factura; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY factura_servicio ALTER COLUMN num_factura SET DEFAULT nextval('factura_servicio_num_factura_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY herramientas_mobiliario ALTER COLUMN id SET DEFAULT nextval('herramientas_mobiliario_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY marca ALTER COLUMN id SET DEFAULT nextval('marca_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modelo ALTER COLUMN id SET DEFAULT nextval('modelo_id_seq'::regclass);


--
-- Name: num_reparacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orden_reparacion ALTER COLUMN num_reparacion SET DEFAULT nextval('orden_reparacion_num_reparacion_seq'::regclass);


--
-- Name: id_pieza; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY piezas ALTER COLUMN id_pieza SET DEFAULT nextval('piezas_id_pieza_seq'::regclass);


--
-- Name: codigo_tipo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipo_equipo ALTER COLUMN codigo_tipo SET DEFAULT nextval('tipo_equipo_codigo_tipo_seq'::regclass);


--
-- Data for Name: accesorios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY accesorios (num_reparacion, cargador, pila, audifonos, tarjeta_memoria, sim_car, otro, tapa) FROM stdin;
2	f	t	f	f	t	\N	f
3	t	t	t	f	t	\N	f
4	f	t	f	t	f	\N	f
5	t	t	f	f	t	\N	f
6	t	f	f	f	f	\N	t
7	t	f	t	f	f	\N	t
1	f	t	f	t	t	linterna	f
8	f	f	f	t	f	\N	t
9	f	f	f	f	f	\N	t
10	f	f	f	f	f	foco	f
11	f	f	t	f	f	otro	t
12	f	f	t	f	f	\N	t
13	t	f	f	t	f	\N	t
14	f	f	t	f	f	nuevo	t
15	f	f	f	f	f	Cable de corriente	f
16	f	t	f	f	f	Cable Jig	t
17	t	t	f	f	f	\N	f
\.


--
-- Data for Name: asigna_orden_tecnico; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY asigna_orden_tecnico (num_reparacion, cedula_tecnico, fecha_inicio, fecha_fin, estado, observacion, comision) FROM stdin;
2	22650768	2016-11-09	2016-11-09	reparado	\N	0.00
3	22650768	2016-11-09	2016-11-09	reparado	\N	0.00
4	22650768	2016-11-09	2016-11-09	reparado	\N	0.00
5	22650768	2016-11-01	2016-11-14	reparado	\N	0.00
5	19765456	2016-11-01	2016-11-15	reparado	\N	0.00
5	21765456	2016-11-01	2016-11-15	reparado	\N	0.00
6	21765456	\N	\N	espera	\N	0.00
7	21765456	\N	\N	espera	\N	0.00
8	14663635	\N	\N	espera	\N	2300.00
9	43562457	\N	\N	reparado	\N	2300.00
10	12343567	\N	\N	reparado	\N	1200.00
11	19765456	\N	\N	reparado	\N	250.00
1	22650768	2016-11-08	2016-11-15	reparado	\N	2200.00
12	14663635	\N	\N	reparado	\N	200.00
12	19765456	\N	2016-11-19	reparado	\N	200.00
13	19765456	\N	\N	espera	\N	0.00
16	19115898	\N	\N	reparado	\N	0.00
16	21765456	2016-11-23	2016-11-23	reparado	\N	500.00
15	14663635	\N	\N	reparado	\N	500.00
14	22650768	\N	\N	reparado	\N	230.00
17	12343567	\N	\N	espera	\N	0.00
\.


--
-- Name: asigna_orden_tecnico_num_reparacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('asigna_orden_tecnico_num_reparacion_seq', 1, false);


--
-- Data for Name: categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY categoria (id, nombre) FROM stdin;
1	cargador
2	camara
3	Pantalla
4	Pin de carga
5	tv
6	telefono
7	Categoria Nueva
\.


--
-- Name: categoria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categoria_id_seq', 7, true);


--
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cliente (tipo, cedula, nombre, apellido, direccion, tlf, email, estado) FROM stdin;
J	22650768	Alejandro	Guerra	Villa san antonio	04165467577	ale@gmail.com	activo
V	14545656	Mabel	Bustamante	Jorge coll	04142343567	mabel@gmail.com	activo
J	12132445	Marquez	Calsadilla	Jorge coll	04143425678	Marquez@gmail.com	activo
V	25467653	Mathias	Guerra	Villa san antonio	04123456545	Mathias@gmail.com	activo
V	43543548	Mereen	Suarez	LAs colinas	04143244343	suarez@gmail.com	activo
V	21232333	ana	Guerra	Villa san antonio	04123435678	Ana@gmail.com	activo
V	36546535	Mario	casas	Los robles	02954565678	casas@gmail.com	activo
V	22650767	Maria	Guerra	Villa san antonio	04123595776	fernandalongart@gmail.com	activo
V	8730054	eyamir	ugueto	porlmar	04160000000	uguetor@gmail.com	activo
V	19115898	Jethro	Chacon	San Antonio	04240000000	correo@gmail.com	activo
V	12121212	fulnito	detal	su casa	99999999999	coreo@gmail.com	activo
V	24109936	Nestor	Gomez	Villa san antonio	04142456355	Neta@gmail.com	activo
E	12345654	nico	Madariagas	m@gmail.com	00000000000	Madariagas@gmail.com	activo
E	215654543	Bernardo	Valencia	costazul	02954655656	valencia@gmail.com	activo
E	253434546	Sergio	Fernandez	Costa azul	04123456635	sergio@gmail.com	activo
V	5987767	Marcelo	Matero	los robles	04124234567	matero@gmail.com	activo
\.


--
-- Data for Name: detalle_factura_compra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY detalle_factura_compra (rif, num_factura, cantidad_comprado, costo_unitario, codigo_pieza, codigo_herramienta, id) FROM stdin;
2233434	1	3	1200.00	\N	25	1
2233434	2	3	15000.00	\N	26	1
2233434	3	2	2300.00	21	\N	1
2233434	212	5	1100.00	21	\N	1
2233434	42352345	5	2300.00	\N	25	1
2233434	42352345	1	1200.00	\N	26	2
2233434	42352345	5	2300.00	21	\N	3
2233434	3333	6	1290.00	22	\N	1
2233434	45	4	12000.00	\N	25	1
2233434	45	4	12000.00	\N	26	2
2233434	45	5	2300.00	21	\N	3
2233434	9090	10	1200.00	23	\N	1
2233434	43434	5	1500.00	23	\N	1
2233434	333434	2	1200.00	\N	25	1
2233434	333434	2	2500.00	23	\N	2
2233434	765432	10	4000.00	23	\N	1
2233434	2134567	10	5000.00	21	\N	1
405908793	12	1	12.00	\N	27	1
405908793	12	1	12.00	\N	28	2
405908793	12	100	1000.00	24	\N	3
405908793	12454	1	1300.00	\N	27	1
405908793	14	10	1000.00	25	\N	1
405908793	33333	2	1300.00	\N	28	1
405908793	1	1	20000.00	\N	29	1
405908793	1	1	12000.00	\N	30	2
405908793	1	10	12000.00	26	\N	3
2233434	1234	2	1200.00	\N	27	1
405908793	4444	9	2300.00	\N	25	1
405908793	33434	3	4300.00	\N	27	1
\.


--
-- Data for Name: equipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY equipo (serial_equipo, id_modelo, id_marca, codigo_tipo, observaciones) FROM stdin;
35345	11	1	1	Reparando x terceros
090999	11	1	1	Raspado,rayado
0003	10	1	1	Es nuevo
0089	12	1	1	Modelo viejo
08876363	14	1	3	Forro de Iron man
9872747	10	1	3	en amarilo
00099	10	1	2	en crisis
0006	11	1	2	fduhgusehfuaehf
0999	11	1	2	nfdgansf
0888	13	1	3	regwertg
0007	10	1	2	weiojqwirj
09897	13	1	1	Es color amarillo
00989	11	1	5	blanco con azul
12345678	11	1	5	nuevo
1234567890	15	1	4	Con mwmorias
111112222244444	11	1	6	Pantalla Con Rayas
0002	13	1	1	ooooooooooooooooooooooooooo
99999999	17	2	2	Verde con dorado
\.


--
-- Data for Name: factura_compra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY factura_compra (num_factura, rif, fecha_compra, monto_total, tipo_pago, num_referencia, banco, num_tarjeta) FROM stdin;
1	2233434	2016-09-09	3600.00	efectivo	\N	\N	\N
2	2233434	2016-09-09	45000.00	efectivo	\N	\N	\N
3	2233434	2016-09-09	4600.00	efectivo	\N	\N	\N
212	2233434	2016-09-09	5500.00	efectivo	\N	\N	\N
42352345	2233434	2016-09-09	24200.00	debito	\N	\N	\N
3333	2233434	2016-09-09	7740.00	efectivo	\N	\N	\N
45	2233434	2016-09-09	107500.00	efectivo	\N	\N	\N
9090	2233434	2016-09-09	12000.00	efectivo	\N	\N	\N
43434	2233434	2016-09-09	7500.00	efectivo	\N	\N	\N
333434	2233434	2016-09-09	7400.00	debito	\N	\N	\N
765432	2233434	2016-09-09	40000.00	credito	\N	\N	\N
2134567	2233434	2016-09-09	50000.00	transferencia	123456789	\N	\N
12	405908793	2016-09-09	100024.00	credito	\N	\N	\N
12454	405908793	2016-09-09	1300.00	debito	\N	\N	\N
14	405908793	2016-09-09	10000.00	transferencia	40000	\N	\N
33333	405908793	2016-11-23	2600.00	efectivo	\N	\N	\N
1	405908793	2016-11-23	152000.00	transferencia	121212	\N	\N
1234	2233434	2016-11-24	2400.00	debito	\N	Venezuela	09765554443344
4444	405908793	2016-11-09	20700.00	efectivo	\N	\N	\N
33434	405908793	2016-11-24	12900.00	transferencia	234354	venezuela	\N
\.


--
-- Name: factura_compra_num_factura_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('factura_compra_num_factura_seq', 1, false);


--
-- Data for Name: factura_servicio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY factura_servicio (num_factura, num_control, cedula_cliente, num_reparacion, fecha, descripcion, monto, estado_factura, tipo_pago, banco, num_tarjeta, num_transferencia) FROM stdin;
42	1	25467653	5	2016-11-19	Un trabajon esa vainaa	15000.00	pagada	efectivo	\N	\N	\N
43	1	22650768	6	2016-11-19	Solo era el teck	23000.00	pagada	debito	\N	\N	\N
45	1	43543548	7	2016-11-19	EL pin de carga	13000.00	pagada	efectivo	\N	\N	\N
46	1	14545656	8	2016-11-19	Desastroso pero facil	14000.00	pagada	efectivo	\N	\N	\N
47	1	22650768	9	2016-11-19	otra falla nueva	10000.00	pagada	efectivo	\N	\N	\N
48	1	21232333	10	2016-11-19	No cargaba	2300.00	pagada	efectivo	\N	\N	\N
49	1	36546535	11	2016-11-19	complicado	1000.00	pagada	efectivo	\N	\N	\N
50	1	22650768	1	2016-11-19	Complicado	16500.00	pagada	debito	\N	\N	\N
51	1	24109936	12	2016-11-22	Muy complicada	13400.00	pagada	efectivo	\N	\N	\N
52	1	12121212	16	2016-11-23		2000.00	pagada	debito	\N	\N	\N
53	1	8730054	15	2016-11-24	pendejadas	2000.00	pagada	efectivo	\N	\N	\N
54	1	8730054	15	2016-11-24	pendejadas	2000.00	pagada	credito	\N	\N	\N
55	1	22650767	14	2016-11-24	otra mas	3400.00	pagada	debito	Caroni	7437723747234	
\.


--
-- Name: factura_servicio_num_factura_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('factura_servicio_num_factura_seq', 55, true);


--
-- Data for Name: herramientas_mobiliario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY herramientas_mobiliario (tipo, id, nombre, unidad, cantidad, valor_ref, estado) FROM stdin;
mobiliario	26	Mesa	unidad	3	12000.00	activo
mobiliario	28	Silla	unidad	2	1300.00	activo
herramienta	29	Estación	unidad	1	20000.00	activo
mobiliario	30	Mueble Gavetero	Unidad	1	12000.00	activo
herramienta	25	Destornillador	unidad	18	2300.00	activo
herramienta	27	cautin	unidad	6	4300.00	activo
\.


--
-- Name: herramientas_mobiliario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('herramientas_mobiliario_id_seq', 30, true);


--
-- Data for Name: marca; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY marca (id, nombre, estado) FROM stdin;
2	HP	activo
1	Samsung	activo
\.


--
-- Name: marca_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('marca_id_seq', 2, true);


--
-- Data for Name: modelo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY modelo (id, id_marca, nombre, estado) FROM stdin;
16	2	Pavilion 1	activo
17	2	Pavilion 2	activo
18	2	Pavilion e2	activo
10	1	S4	activo
11	1	S5	activo
12	1	S3	activo
13	1	S7	activo
14	1	S88	activo
15	1	s9	activo
\.


--
-- Name: modelo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('modelo_id_seq', 18, true);


--
-- Data for Name: orden_reparacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY orden_reparacion (num_reparacion, fecha, serial_equipo, cedula_cliente, falla, descripcion, estado) FROM stdin;
3	2016-11-09	0003	14545656	Se queda en el logo	Se queda en el logo	entregado
4	2016-11-09	0089	12132445	software	Modelo viejo	entregado
2	2016-11-09	0002	12345654	no se escucha	no se escucha	entregado
5	2016-11-14	08876363	25467653	hardware	se salio un boton	entregado
6	2016-11-19	9872747	22650768	ambos	no sirve para nada	entregado
7	2016-11-19	00099	43543548	hardware	repuestos faltantes	entregado
8	2016-11-19	0006	14545656	software	ertwey	entregado
9	2016-11-19	0999	22650768	software	wrfaksdkfnsd	entregado
11	2016-11-19	0007	36546535	software	aehiwr	entregado
1	2016-11-08	090999	22650768	Se recalienta	Rojo	entregado
10	2016-11-19	0888	21232333	software	listo	entregado
12	2016-11-19	09897	24109936	ambos	Se ve arrecha	entregado
13	2016-11-23	00989	22650767	hardware	Se rompio la mica	proceso
16	2016-11-23	111112222244444	12121212	hardware	Pantalla Dañada	entregado
15	2016-11-23	1234567890	8730054	software	Tarda en prender	entregado
14	2016-11-23	12345678	22650767	software	actualizacion de software	entregado
17	2016-11-24	99999999	5987767	hardware	Se salio la pantalla	revision
\.


--
-- Name: orden_reparacion_num_reparacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('orden_reparacion_num_reparacion_seq', 17, true);


--
-- Data for Name: pieza_modelo_marca; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pieza_modelo_marca (id_marca, id_modelo, id_pieza) FROM stdin;
1	12	21
1	10	22
1	12	22
1	13	23
1	10	24
1	12	24
1	14	24
1	11	25
1	12	26
\.


--
-- Data for Name: piezas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY piezas (id_pieza, nombre, descripcion, unidad, medida, cantidad, costo, precio_venta, estado, categoria) FROM stdin;
22	Pin de carga	Marca Japonesa	unidad	3pulg	3	1290.00	1500.00	activo	\N
23	Sensor	para telefonos grandes	unidad	1pulg	24	4000.00	5000.00	activo	3
21	Mica	Modelo viejo	unidad	3pulg	22	5000.00	6000.00	activo	\N
24	Pantalla	Pantalla Samsung S4/S3/S8	Unidad	Pieza	99	1000.00	1300.00	activo	3
25	Pantalla S5	Blanca	unidad	Pieza	9	1000.00	1300.00	activo	3
26	Pantalla S3 Mini	Blanca	Unidad	Pieza	10	12000.00	16500.00	activo	3
\.


--
-- Name: piezas_id_pieza_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('piezas_id_pieza_seq', 26, true);


--
-- Data for Name: proveedor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY proveedor (letra, rif, nombre_prov, direccion, tlf, nombre_resp, tlf_resp, email) FROM stdin;
V	2233434	Sigo electronic	av juan bautista	02952689569	\N	\N	feliz@gmail.com
J	405908793	Computel Margarita	4 de Mayo	02952633390	Jethro Chacon	04240000000	computel@gmail.com
\.


--
-- Data for Name: tecnico; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tecnico (cedula, nombre, apellido, direccion, tlf, email, tipo, estado) FROM stdin;
22650768	Alejandro	Guerra	Villa san antonio	04126646646	jose@hotmail.com	ambos	activo
19765456	Jose	Perez	san juan	04123425675	prez@gmail.com	software	activo
21765456	marlon	luna	los robles	04163425675	marlon@gmail.com	hardware	activo
14663635	Jetro	Nuñez	las villas	04145355353	jetro@gmail.com	ambos	activo
19115898	Jethro	Chacon	San Lorenzo	04240000000	correo@gmail.com	ambos	inactivo
43562457	deomar	guerra	caracas	44444444444	deo@gmail.com	software	inactivo
12343567	Martin	lopez	villa juana	04144434434	martin@gmail.com	hardware	inactivo
\.


--
-- Data for Name: tipo_equipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tipo_equipo (codigo_tipo, nombre, estado) FROM stdin;
1	44telefono	activo
2	lapto	activo
3	tablet	activo
4	compu de escritorio	activo
5	telefono	activo
6	celular	activo
7	TABLET	activo
\.


--
-- Name: tipo_equipo_codigo_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipo_equipo_codigo_tipo_seq', 7, true);


--
-- Data for Name: uso_piezas_reparacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY uso_piezas_reparacion (num_reparacion, id_pieza, cantidad, costo) FROM stdin;
1	21	2	\N
3	21	2	\N
4	21	2	\N
4	22	1	\N
5	22	5	\N
5	21	1	\N
12	22	2	\N
16	25	-11	\N
\.


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY usuario (cedula, clave, tipo, nombre, apellido, direccion, estado) FROM stdin;
22650767	123456	admin	Maria Fernanda	Guerra	Villa san Antonio	activo
22650768	1234	tecnico	Alejandro	Guerra	Villa san antonio	activo
21765456	1234	tecnico	marlon	luna	los robles	activo
24109936	1234567	\N	Nestor	Gomez	\N	inactivo
19115898	121212	tecnico	Jethro	Chacon	San Lorenzo	inactivo
12343567	1234	tecnico	Martin	lopez	villa juana	inactivo
14663635	1234	tecnico	Jetro	Nuñez	las villas	inactivo
19765456	1234	tecnico	jose	Perez	san juan	inactivo
43562457	1234	tecnico	deomar	guerra	caracas	inactivo
\.


--
-- Name: pk_detalle_factura_compra; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detalle_factura_compra
    ADD CONSTRAINT pk_detalle_factura_compra PRIMARY KEY (rif, num_factura, id);


--
-- Name: pk_equipo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY equipo
    ADD CONSTRAINT pk_equipo PRIMARY KEY (serial_equipo);


--
-- Name: pk_factura_compra; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY factura_compra
    ADD CONSTRAINT pk_factura_compra PRIMARY KEY (num_factura, rif);


--
-- Name: pk_factura_servicio; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY factura_servicio
    ADD CONSTRAINT pk_factura_servicio PRIMARY KEY (num_factura, cedula_cliente, num_reparacion);


--
-- Name: pk_herramientas_mobiliario; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY herramientas_mobiliario
    ADD CONSTRAINT pk_herramientas_mobiliario PRIMARY KEY (id);


--
-- Name: pk_marca; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY marca
    ADD CONSTRAINT pk_marca PRIMARY KEY (id);


--
-- Name: pk_modelo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modelo
    ADD CONSTRAINT pk_modelo PRIMARY KEY (id, id_marca);


--
-- Name: pk_orden_reparacion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY orden_reparacion
    ADD CONSTRAINT pk_orden_reparacion PRIMARY KEY (num_reparacion);


--
-- Name: pk_pieza_modelo_marca; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pieza_modelo_marca
    ADD CONSTRAINT pk_pieza_modelo_marca PRIMARY KEY (id_pieza, id_modelo, id_marca);


--
-- Name: pk_piezas; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY piezas
    ADD CONSTRAINT pk_piezas PRIMARY KEY (id_pieza);


--
-- Name: pk_proveedor; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY proveedor
    ADD CONSTRAINT pk_proveedor PRIMARY KEY (rif);


--
-- Name: pk_tecnico; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tecnico
    ADD CONSTRAINT pk_tecnico PRIMARY KEY (cedula);


--
-- Name: pk_tipo_equipo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipo_equipo
    ADD CONSTRAINT pk_tipo_equipo PRIMARY KEY (codigo_tipo);


--
-- Name: pk_uso_piezas_reparacion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uso_piezas_reparacion
    ADD CONSTRAINT pk_uso_piezas_reparacion PRIMARY KEY (num_reparacion, id_pieza);


--
-- Name: pk_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT pk_usuario PRIMARY KEY (cedula);


--
-- Name: fk_factura_compra; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY detalle_factura_compra
    ADD CONSTRAINT fk_factura_compra FOREIGN KEY (num_factura, rif) REFERENCES factura_compra(num_factura, rif);


--
-- Name: fk_marca_modelo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modelo
    ADD CONSTRAINT fk_marca_modelo FOREIGN KEY (id_marca) REFERENCES marca(id);


--
-- Name: fk_modelo_equipo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY equipo
    ADD CONSTRAINT fk_modelo_equipo FOREIGN KEY (id_modelo, id_marca) REFERENCES modelo(id, id_marca);


--
-- Name: fk_modelo_marca_pieza; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pieza_modelo_marca
    ADD CONSTRAINT fk_modelo_marca_pieza FOREIGN KEY (id_marca, id_modelo) REFERENCES modelo(id_marca, id);


--
-- Name: fk_orden_reparacion_accesorios; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accesorios
    ADD CONSTRAINT fk_orden_reparacion_accesorios FOREIGN KEY (num_reparacion) REFERENCES orden_reparacion(num_reparacion);


--
-- Name: fk_orden_reparacion_servicio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY factura_servicio
    ADD CONSTRAINT fk_orden_reparacion_servicio FOREIGN KEY (num_reparacion) REFERENCES orden_reparacion(num_reparacion);


--
-- Name: fk_orden_reparacion_tecnico; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY asigna_orden_tecnico
    ADD CONSTRAINT fk_orden_reparacion_tecnico FOREIGN KEY (num_reparacion) REFERENCES orden_reparacion(num_reparacion);


--
-- Name: fk_orden_reparacion_uso_pieza; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uso_piezas_reparacion
    ADD CONSTRAINT fk_orden_reparacion_uso_pieza FOREIGN KEY (num_reparacion) REFERENCES orden_reparacion(num_reparacion);


--
-- Name: fk_piezas_uso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY uso_piezas_reparacion
    ADD CONSTRAINT fk_piezas_uso FOREIGN KEY (id_pieza) REFERENCES piezas(id_pieza);


--
-- Name: fk_proveedor_factura; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY factura_compra
    ADD CONSTRAINT fk_proveedor_factura FOREIGN KEY (rif) REFERENCES proveedor(rif);


--
-- Name: fk_tecnico_asigna; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY asigna_orden_tecnico
    ADD CONSTRAINT fk_tecnico_asigna FOREIGN KEY (cedula_tecnico) REFERENCES tecnico(cedula);


--
-- Name: fk_tipo_equipo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY equipo
    ADD CONSTRAINT fk_tipo_equipo FOREIGN KEY (codigo_tipo) REFERENCES tipo_equipo(codigo_tipo);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

