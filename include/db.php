<?php 

	require_once ("constant.php");
	class DB{
		private $con=null;
		private $result=null;
		private $lastTable=null;
		private $sqlSaver=null;
		function __construct(){
			$this->con = pg_connect("host=".HOST_DB_PG." port=".PORT_DB_PG." dbname=".NAME_DB_PG."  user=".USER_DB_PG." password=".PASSWORD_DB_PG."");
			if(!$this->con)
				exit('connection failed');
		}
		function lastError(){
			return pg_last_error($this->con);
		}
		
		/*
			Query and get the first row
		*/
		function queryRow($query){
			$this->result=pg_query($this->con,$query);
			if(!$this->result)
				return FALSE;
			return pg_fetch_assoc($this->result);
		}

		function queryAll($query){
			$this->result=pg_query($this->con,$query);
			if(!$this->result)
				return FALSE;
			return pg_fetch_all($this->result);
		}
		/*
			begin transaction
		*/
		function begin(){
			return $this->query("BEGIN");
		}
		/*
			finish transaction
		*/
		function finish(){
			return $this->query("COMMIT");
		}
		/*
			rollback transaction
		*/
		function rollback(){
			return $this->query("ROLLBACK");
		}
		private function query($query){
			$this->result=pg_query($this->con,$query);
			if(!$this->result)
				return $this->getError();
			return TRUE;
		}
		function insertRow($table,$values){
			//$this->lastTable=$table;
			$result =pg_insert($this->con,$table,$values);
			return $result;
			//return pg_affected_rows($result);
		}
		/*
			Update table
		*/
		function updateRows($table,$values,$conditions){
			$result =pg_update($this->con,$table,$values,$conditions);
			return $result;
		}
		function login($user){
			$username=$this->queryRow("SELECT * FROM usuario where estado='activo' and cedula='".$user['cedula']."'");
			if ($username) {
				if ($username['clave']==$user['clave']) {
					return array('success'=>true,'msg'=>'Bienvenido '.$username['nombre']." ".$username['apellido'],'data'=>$username);
				} else {
					return array('success'=>false,'msg'=>'Clave invalida','data'=>null);
				}
			} else {
				return array('success'=>false,'msg'=>'El usuario no esta registrado o se encuentra inactivo','data'=>$user);
			}
			
		}

		function cargarProfile($email){
			return $this->queryRow("SELECT * FROM users where email='$email'");
		}
		
		function unir($tabla1,$tabla2,$id1,$id2,$union){
			if (gettype($tabla1)=='string')
				$tabla1 = $this->queryAll("SELECT * from ".$tabla1);
			if (gettype($tabla2)=='string')
				$tabla2 = $this->queryAll("SELECT * from ".$tabla2);
			$aux = array();
			foreach ($tabla1 as $key => $item1) {
				foreach ($tabla2 as $key => $item2) {
					if ($item1[$id1]==$item2[$id2]) {
						$item1[$union]=$item2;
					}
				}
				$aux[]=$item1;
			}
			return $aux;
		}
		function registrarUsers($users){
			$resp = $this->queryRow("SELECT cedula from usuario where cedula ='".$users['cedula']."'");
			if (!$resp) {
				$query = $this->insertRow('usuario',$users);
				if ($query) {
					return array('success'=>true,'msg'=>'');
				}else{
					return array('success'=>false,'msg'=>'Error al insertar el usuario',
						'error'=>$this->lastError());
				}
			}else{
				return array('success'=>false,'msg'=>'El usuario ya existe');
			}
		}
		function equipos($equipo){
			$where = (!empty($equipo))?" where a.serial_equipo ='".$equipo['serial_equipo']."'":"";
			return $resp = $this->queryAll("SELECT a.*,b.nombre as marca ,c.nombre as modelo,d.nombre as tipo 
				from equipo a 
				inner join marca b on a.id_marca=b.id 
				inner join modelo c on a.id_modelo=c.id 
				inner join tipo_equipo d on a.codigo_tipo=d.codigo_tipo ".$where);
		}
		function registrarMarca($nombre){
			//$m = $this->insertRow('marca',array('nombre' =>$nombre));
			$m=$this->queryRow("INSERT INTO marca (nombre) values ('$nombre') RETURNING id");
			if ($m) {
				//$query = $this->queryRow("SELECT id from marca order by id DESC");
				return $m['id'];
			}else{
				return false;
			}
		}
		function registrarModelo($modelo){
			$newModelo = array('nombre' => $modelo['nombre_modelo'],'id_marca'=>$modelo['id_marca']);
			//$m = $this->insertRow('modelo',$newModelo);
			$m=$this->queryRow("INSERT INTO modelo (id_marca,nombre) 
				values (".$modelo['id_marca'].",'".$modelo['nombre_modelo']."') RETURNING id");
			if ($m) {
				//$query = $this->queryRow("SELECT id from modelo order by id DESC");
				return $m['id'];
			}else{
				return false;
			}
		}

		function registrarTipo($tipo_nombre){
			$tipo=$this->queryRow("INSERT INTO tipo_equipo (nombre) values ('$tipo_nombre') RETURNING codigo_tipo");

			if ($tipo) {
				return $tipo['codigo_tipo'];
			}else{
				return false;
			}
		}

		function registrarEquipo($equipo){
			$this->begin();
			$resp = $this->queryRow("SELECT serial_equipo from equipo where serial_equipo ='".$equipo['serial_equipo']."'");
			if (!$resp) {

				if (empty($equipo['id_marca']) || $equipo['id_marca']=="-1"){
					$equipo['id_marca'] = $this->registrarMarca($equipo['nombre_marca']);
				}
				if (empty($equipo['id_modelo']) || $equipo['id_modelo']=="-1"){
					$equipo['id_modelo'] = $this->registrarModelo($equipo);
				}
				if (empty($equipo['codigo_tipo']) || $equipo['codigo_tipo']=="-1"){
					$equipo['codigo_tipo'] = $this->registrarTipo($equipo['nombre_tipo']);
				}
				unset($equipo['nombre_marca']);
				unset($equipo['nombre_modelo']);
				unset($equipo['nombre_tipo']);
				
				$query = $this->insertRow('equipo',$equipo);
				if ($query) {
					$this->finish();
					return array('success'=>true,'msg'=>'');
				}else{
					$this->rollback();
					return array('success'=>false,'msg'=>'Error al insertar el equipo',
						'error'=>$this->lastError());
					$this->finish();
				}
			}else{
				$this->finish();
				return array('success'=>false,'msg'=>'El equipo ya existe');
			}
		}

		function piezas($pieza=null){
			$where=(!empty($pieza))?" and id_pieza='".$pieza."'":"";
			return $resp = $this->queryAll("SELECT * from piezas where estado='activo'".$where);
		}
		function inventario($tipo=null,$id=null){
			$where = (empty($tipo))?'':" and tipo='$tipo'";
			$where .= (empty($id))?'':" and id=$id";
			return $resp = $this->queryAll("SELECT * from herramientas_mobiliario where estado='activo'".$where);
		}

/*		function ordenes($tipo){
			$where = ($orden)? " where estado='$tipo' and a.num_reparacion=".$orden['num_reparacion']:'';

			return $query = $this->queryAll("SELECT a.*,c.nombre,c.apellido,c.cedula from orden_reparacion a left join equipo b on a.serial_equipo = b.serial_equipo left join cliente c on a.cedula_cliente=c.cedula where estado='$tipo'");
		}*/
		function newPieza($pieza){
			extract($pieza);     
			return $this->queryRow("INSERT INTO piezas 
				(nombre, descripcion, unidad, medida, cantidad, costo)
				values ('$nombre', '$descripcion', '$unidad', '$medida', $cantidad, $costo) 
				RETURNING id_pieza");
		}
		function newInventario($inventario){
			extract($inventario);     
			$query = $this->queryRow("INSERT INTO herramientas_mobiliario
				(tipo, nombre, unidad, cantidad, valor_ref)
				values ('$tipo', '$nombre', '$unidad', $cantidad, $costo) 
				RETURNING id");
			return $query['id'];
		}
		function addPieza($id,$cantidad,$costo, $precio_venta){
			return $this->queryRow("UPDATE piezas set cantidad = cantidad+".$cantidad.", costo=$costo, precio_venta=$precio_venta where id_pieza='".$id."'");
		}
		function addInventario($id,$cantidad,$costo){
			return $this->queryRow("UPDATE herramientas_mobiliario set cantidad = cantidad+$cantidad, valor_ref=$costo where id=$id");
		}
		function minusPieza($id,$cantidad){
			return $this->queryRow("UPDATE piezas set cantidad = cantidad-$cantidad where id_pieza='".$id."'");
		}
		function minusInventario($id,$cantidad){
			return $this->queryRow("UPDATE herramientas_mobiliario set cantidad = cantidad-$cantidad where id=$id");
		}
	}
?>