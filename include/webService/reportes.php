<?php 
//include 'PDFClass.php';
$server->register("reporte1");
$server->register("reporte2");
$server->register("reporte3");
$server->register("reporte4");
$server->register("reporte5");
$server->register("comisiones");

function reporte1($date){
	$db = new DB();
	$where = ($date)?" c.fecha between '".$date['startdate']."' and  '".$date['enddate']."'":'';
	$resp = $db -> queryAll("SELECT a.num_reparacion, c.fecha, serial_equipo,c.descripcion, b.nombre,b.apellido, b.cedula, c.monto, c.tipo_pago, c.num_factura from orden_reparacion a inner join cliente b on a.cedula_cliente=b.cedula inner join factura_servicio c on a.num_reparacion=c.num_reparacion where  a.estado='entregado' and".$where);
	$total = $db -> queryRow("SELECT sum(c.monto) as total from orden_reparacion a inner join cliente b on a.cedula_cliente=b.cedula inner join factura_servicio c on a.num_reparacion=c.num_reparacion where  a.estado='entregado' and".$where);
	
	if ($resp){
		$data='';
		foreach ($resp as $key => $value) {
			$data .= '<tr>
						<td>'.$value['num_factura'].'</td>
						<td>'.$value['nombre'].' '.$value['apellido'].'</td>
						<td>'.$value['fecha'].'</td>
						<td>'.$value['serial_equipo'].'</td>
						<td>'.$value['descripcion'].'</td>
						<td>'.$value['monto'].'</td>
						<td>'.$value['tipo_pago'].'</td>
					</tr>';
		}

		$data.='<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>Total</td>
					<td>'.$total['total'].'</td>
					<td></td>
				</tr>';

		$content='<table class="table row-border table-bordered">
			<thead>
	            <tr>
	                <th>Num factura</th>
	                <th>Cliente</th>
	                <th>Fecha</th>
	                <th>Serial Equipo</th>
	                <th>Descripcion</th>
	                <th>Monto (Bs)</th>
	                <th>Tipo pago</th>
	            </tr>
	        </thead>
	        <tbody>
				'.$data.'
	        </tbody>

		</table>';
		$name='proveedores'.date("Ymdgis").'.pdf';

		newPdf($content,'Listado de Reparaciones '.$date['startdate'].' / '.$date['enddate'],'pdf/'.$name);
		return array('data' => $name,'success'=>true);
	}else{
		return array('data' => '','success'=>false,'msg'=>'No hay reparaciones');
	}

}

function reporte2($date){
	$db= new DB();
	$resp = $db->queryAll("SELECT * from cliente ");
	$total = $db->queryRow("SELECT count(*) as total from cliente ");

	if ($resp){
		$data='';
		foreach ($resp as $key => $value) {
			$data .= '<tr>
						<td>'.$value['cedula'].'</td>
						<td>'.$value['nombre'].'</td>
						<td>'.$value['apellido'].'</td>
						<td>'.$value['direccion'].'</td>
						<td>'.$value['tlf'].'</td>
						<td>'.$value['email'].'</td>
					</tr>';
		}
		$data .= '<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>Total de clientes</td>
					<td>'.$total['total'].'</td>
				</tr>';

		$content='<table class="table row-border table-bordered">
			<thead>
	            <tr>
	                <th>Cedula</th>
	                <th>Nombre</th>
	                <th>Apellido</th>
	                <th>Dirección</th>
	                <th>Teléfono</th>
	                <th>Email</th>
	            </tr>
	        </thead>
	        <tbody>
				'.$data.'
	        </tbody>

		</table>';
		$name='clientes'.date("Ymdgis").'.pdf';

		newPdf($content,'Listado de Clientes','pdf/'.$name);
		return array('data' => $name,'success'=>true);
	}else{
		return array('data' => '','success'=>false,'msg'=>'No hay clientes');
	}

		
}

function reporte3($date){
	$db= new DB();
	$resp = $db->queryAll("SELECT * from proveedor ");
	$total = $db->queryRow("SELECT count(*) as total from proveedor ");

	if ($resp){
		$data='';
		foreach ($resp as $key => $value) {
			$data .= '<tr>
						<td>'.$value['letra'].'-'.$value['rif'].'</td>
						<td>'.$value['nombre_prov'].'</td>
						<td>'.$value['direccion'].'</td>
						<td>'.$value['tlf'].'</td>
						<td>'.$value['nombre_resp'].'</td>
						<td>'.$value['tlf_resp'].'</td>
						<td>'.$value['email'].'</td>
					</tr>';
		}

		$data .= '<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>Total proveedores</td>
					<td>'.$total['total'].'</td>
				</tr>';

		$content='<table class="table row-border table-bordered">
			<thead>
	            <tr>
	                <th>Rif</th>
	                <th>Nombre</th>
	                <th>Dirección</th>
	                <th>Teléfono</th>
	                <th>Responsable</th>
	                <th>telefono del Resp.</th>
	                <th>Email del Resp.</th>
	            </tr>
	        </thead>
	        <tbody>
				'.$data.'
	        </tbody>

		</table>';

		$name='proveedores'.date("Ymdgis").'.pdf';


		newPdf($content,'Listado de Proveedores','pdf/'.$name);
		return array('data' => $name,'success'=>true);
	}else{
		return array('data' => '','success'=>false,'msg'=>'No hay proveedores');
	}
}

function reporte4($date){
	$db= new DB();

	$piezas = $db -> queryAll("SELECT * from piezas where estado='activo'");
	$herramientas = $db -> queryAll("SELECT * from herramientas_mobiliario where estado='activo'");
	$total1 = $db -> queryRow("SELECT sum(precio_venta) as total from piezas where estado='activo'");
	$total2 = $db -> queryRow("SELECT sum(cantidad) as total from herramientas_mobiliario where estado='activo'");

	$content='';
	$content2='';

	if ($herramientas){
		$data2='';
		foreach ($herramientas as $key => $value) {
			$data2 .= '<tr>
						<td>'.$value['tipo'].'</td>
						<td>'.$value['nombre'].'</td>
						<td>'.$value['cantidad'].'</td>
					</tr>';
		}

		$data2 .= '<tr>
					<td></td>
					<td>Total</td>
					<td>'.$total2['total'].'</td>
				</tr>';

		$content2='<h4>Herramientas y Mobiliarios:</h4>
		<table class="table row-border table-bordered">
			<thead>
	            <tr>
	                <th>Tipo</th>
	                <th>Nombre</th>
	                <th>Cantidad</th>
	            </tr>
	        </thead>
	        <tbody>
				'.$data2.'
	        </tbody>

		</table>';
	}


	if ($piezas){
		$data='';
		foreach ($piezas as $key => $value) {
			$data .= '<tr>
						<td>'.$value['nombre'].'</td>
						<td>'.$value['descripcion'].'</td>
						<td>'.$value['cantidad'].'</td>
						<td>'.$value['precio_venta'].'</td>
					</tr>';
		}

		$data .= '<tr>
					<td></td>
					<td></td>
					<td>Total</td>
					<td>'.$total1['total'].'</td>
				</tr>';


		$content='<h4>Piezas:</h4>
		<table class="table row-border table-bordered">
			<thead>
	            <tr>
	                <th>Nombre</th>
	                <th>Descripcion</th>
	                <th>Cantidad</th>
	                <th>Costo (Bs)</th>
	            </tr>
	        </thead>
	        <tbody>
				'.$data.'
	        </tbody>

		</table>';

	}

	if ($piezas || $herramientas) {
		$content.=$content2;
		$name='compras'.date("Ymdgis").'.pdf';
		newPdf($content,'Inventario activo','pdf/'.$name);
		return array('data' => $name,'success'=>true);
	}else{
		return array('msg'=>'No se pudo generar el reporte','success'=>false);
	}

}

function reporte5($date){
	$db= new DB();
	$where = ($date)?" where fecha_compra between '".$date['startdate']."' and  '".$date['enddate']."'":'';
	$resp = $db->queryAll("SELECT a.num_factura, fecha_compra, monto_total, nombre, cantidad_comprado, costo_unitario from factura_compra a inner join detalle_factura_compra b on a.num_factura=b.num_factura inner join piezas c on b.codigo_pieza=c.id_pieza".$where);
	$herramientas = $db->queryAll("SELECT a.num_factura, fecha_compra, nombre, cantidad_comprado, costo_unitario from factura_compra a inner join detalle_factura_compra b on a.num_factura=b.num_factura inner join herramientas_mobiliario c on b.codigo_herramienta=c.id".$where);
	$total1 = $db->queryRow("SELECT sum(costo_unitario) as total from factura_compra a inner join detalle_factura_compra b on a.num_factura=b.num_factura inner join piezas c on b.codigo_pieza=c.id_pieza".$where);
	$total2 = $db->queryRow("SELECT sum(costo_unitario) as total from factura_compra a inner join detalle_factura_compra b on a.num_factura=b.num_factura inner join herramientas_mobiliario c on b.codigo_herramienta=c.id".$where);

	$content='';
	$content2='';


	if ($herramientas){
		$data2='';
		$totalcosto1=0;
		foreach ($herramientas as $key => $value) {
			$totalCostos1=$value['costo_unitario'] * $value['cantidad_comprado'];
			$data2 .= '<tr>
						<td>'.$value['num_factura'].'</td>
						<td>'.$value['fecha_compra'].'</td>
						<td>'.$value['nombre'].'</td>
						<td>'.$value['cantidad_comprado'].'</td>
						<td>'.$value['costo_unitario'].'</td>
						<td>'.$totalCostos1.'</td>
					</tr>';
			$totalcosto1=$totalcosto1+($value['costo_unitario'] * $value['cantidad_comprado']);
		}

		$data2 .= '<tr>
					<td></td>
					<td></td>
					<td></td>
					<td>Total</td>
					<td>'.$total2['total'].'</td>
					<td>'.$totalcosto1.'</td>
				</tr>';

		$content2='<h4>Herramientas y Mobiliarios:</h4>
		<table class="table row-border table-bordered">
			<thead>
	            <tr>
	                <th>Número de factura</th>
	                <th>Fecha compra</th>
	                <th>Nombre</th>
	                <th>Cantidad</th>
	                <th>Costo (Bs)</th>
	                <th>Total (Bs)</th>
	            </tr>
	        </thead>
	        <tbody>
				'.$data2.'
	        </tbody>

		</table>';
	}



	if ($resp){
		$data='';
		$totalcosto=0;
		foreach ($resp as $key => $value) {
			$totalCosto2=$value['costo_unitario'] * $value['cantidad_comprado'];
			$data .= '<tr>
						<td>'.$value['num_factura'].'</td>
						<td>'.$value['fecha_compra'].'</td>
						<td>'.$value['nombre'].'</td>
						<td>'.$value['cantidad_comprado'].'</td>
						<td>'.$value['costo_unitario'].'</td>
						<td>'.$totalCosto2.'</td>
					</tr>';

			$totalcosto=$totalcosto+($value['costo_unitario'] * $value['cantidad_comprado']);
		}

		$data .= '<tr>
					<td></td>
					<td></td>
					<td></td>
					<td>Total</td>
					<td>'.$total1['total'].'</td>
					<td>'.$totalcosto.'</td>
				</tr>';

		$content='<h4>Piezas:</h4>
		<table class="table row-border table-bordered">
			<thead>
	            <tr>
	                <th>Número de factura</th>
	                <th>Fecha compra</th>
	                <th>Nombre</th>
	                <th>Cantidad</th>
	                <th>Costo (Bs)</th>
	                <th>Total (Bs)</th>
	            </tr>
	        </thead>
	        <tbody>
				'.$data.'
	        </tbody>

		</table>';

	}

	if ($resp || $herramientas) {
		$content.=$content2;
		$name='compras'.date("Ymdgis").'.pdf';

		newPdf($content,'Listado de Compras '.$date['startdate'].' / '.$date['enddate'],'pdf/'.$name);
		return array('data' => $name,'success'=>true);
	}else{
		return array('msg'=>'No se pudo generar el reporte','success'=>false);
	}
	
}



function comisiones($date){
	$db = new DB();
	$meses  = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	/*$where = ($date)?" where c.fecha between '".$date['startdate']."' and  '".$date['enddate']."'":'';*/
	$where = ($date)?" where b.fecha_fin between '".$date['startdate']."' and  '".$date['enddate']."'":'';
	/*$resp = $db -> queryAll("SELECT a.nombre, a.apellido, sum(b.comision) as comision, c.fecha from tecnico a inner join asigna_orden_tecnico b on a.cedula=b.cedula_tecnico inner join factura_servicio c on b.num_reparacion=c.num_reparacion ".$where." group by a.nombre, a.apellido, c.fecha");
	*/
	$resp = $db -> queryAll("SELECT a.nombre, a.apellido, sum(b.comision) as comision, date_part('year',b.fecha_fin) as anio, date_part('month',b.fecha_fin) as mes from tecnico a inner join asigna_orden_tecnico b on a.cedula=b.cedula_tecnico  ".$where." group by a.nombre, a.apellido, anio,mes order by anio, mes;
");	
	$total = $db -> queryRow("SELECT  sum(b.comision) as comision from tecnico a inner join asigna_orden_tecnico b on a.cedula=b.cedula_tecnico  ".$where.";
");
	
	if ($resp){
		$data='';
		foreach ($resp as $key => $value) {
			$mes=intval($value['mes'])-1;
			$data .= '<tr>
						<td>'.$value['nombre'].' '.$value['apellido'].'</td>
						<td style=" width:200px;"><p style=" margin-left:130px">'.$value['comision'].'</p></td>
						<td>'.$meses[$mes].'-'.$value['anio'].'</td>
					</tr>';
		}

		$content='<table class="table row-border table-bordered">
			<thead>
	            <tr>
	                <th>Técnico</th>
	                <th >Comisión (Bs)</th>
	                <th>Fecha</th>
	            </tr>
	        </thead>
	        <tbody>
				'.$data.'
	        </tbody>

		</table>';

		$name='comisiones'.date("Ymdgis").'.pdf';


		newPdf($content,'Listado de Comisiones '.$date['startdate'].' / '.$date['enddate'],'pdf/'.$name);
		return array('data' => $name,'success'=>true);
	}else{
		return array('data' => '','success'=>false,'msg'=>'No hay comisiones');
	}

}




?>