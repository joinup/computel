<?php
$server->register("indicador1");
$server->register("indicador2");
$server->register("indicador3");
$server->register("indicador4");
$server->register("indicador5");
$server->register("indicador6");


function indicador1($date){
	$db = new DB();
	$where = ($date)?"where fecha < '".$date['startdate']."' and fecha > '".$date['startdate']."'":'';
	$indicador = $db->queryAll("SELECT falla as name,count(*) as y from orden_reparacion group by falla ");
	return array('data' => $indicador,'success'=>true);
}

function indicador2($date){
	$db = new DB();
	$where = ($date)?"where fecha between '".$date['startdate']."' and  '".$date['enddate']."'":'';
	$indicador = $db->queryAll("SELECT count(b.codigo_tipo) as y, c.nombre as name from orden_reparacion a inner join equipo b on a.serial_equipo=b.serial_equipo inner join tipo_equipo c on b.codigo_tipo=c.codigo_tipo ".$where." group by c.nombre ");
	
	return array('data' => $indicador,'success'=>true);
}

function indicador3($date){
	$db = new DB();
	$where = ($date)?"where fecha_inicio between '".$date['startdate']."' and  '".$date['enddate']."' OR fecha_fin between '".$date['startdate']."' and  '".$date['enddate']."'":'';
	$indicador = $db->queryAll("SELECT c.nombre as name, sum(b.cantidad) as y from asigna_orden_tecnico a inner join uso_piezas_reparacion b  on a.num_reparacion=b.num_reparacion inner join piezas c on b.id_pieza=c.id_pieza ".$where." group by c.nombre ");
	
	return array('data' => $indicador,'success'=>true);
}

function indicador4($date){
	$db = new DB();
	$where = ($date)?" fecha_inicio between '".$date['startdate']."' and  '".$date['enddate']."' OR fecha_fin between '".$date['startdate']."' and  '".$date['enddate']."'":'';
	$indicador = $db->queryAll("SELECT b.nombre as name, count(*) as y from asigna_orden_tecnico a inner join tecnico b on a.cedula_tecnico=b.cedula where a.estado='reparado' AND ".$where." group by b.nombre");
	
	return array('data' => $indicador,'success'=>true);
}

function indicador5($date){
	$db = new DB();
	$where = ($date)?" fecha_inicio between '".$date['startdate']."' and  '".$date['enddate']."' OR fecha_fin between '".$date['startdate']."' and  '".$date['enddate']."'":'';
	
	$tecnicos = $db->queryAll("SELECT b.cedula,nombre, sum(case when  a.fecha_fin <= a.fecha_inicio + '3 day'::INTERVAL  then 1 else 0 end) as a, sum(case when  a.fecha_fin <= a.fecha_inicio + '7 day'::INTERVAL and a.fecha_fin > a.fecha_inicio + '3 day'::INTERVAL then 1 else 0 end) as b, sum(case when   a.fecha_fin > a.fecha_inicio + '7 day'::INTERVAL then 1 else 0 end) as c From asigna_orden_tecnico a inner join tecnico b on a.cedula_tecnico=b.cedula where a.estado='reparado' and ".$where." group by b.cedula");

	return array('data' => $tecnicos,'success'=>true);
}

function indicador6($date){
	$db = new DB();
	$where = ($date)?" where fecha between '".$date['startdate']."' and  '".$date['enddate']."'":'';
	$indicador = $db->queryAll("SELECT tipo_pago as name, count(*) as y from factura_servicio ".$where." group by tipo_pago");
	
	return array('data' => $indicador,'success'=>true);
}

?>
