<?php 
$server->register("clientes");
$server->register("registrarCliente");
$server->register("editarCliente");

function registrarCliente($client){
	$db= new DB();
	$resp = $db->queryRow("SELECT cedula from cliente where cedula ='".$client['cedula']."'");
	if (!$resp) {
		$query = $db->insertRow('cliente',$client);
		if ($query) {
			return array('success'=>true,'msg'=>'');
		}else{
			return array('success'=>false,'msg'=>'Error al insertar el cliente',
				'error'=>$db->lastError());
		}
	}else{
		return array('success'=>false,'msg'=>'El cliente ya existe');
	}
}

function clientes($client){
	$db = new DB();
	if (isset($client['cedula'])) {
		$where = (!empty($client))?" and cedula ='".$client['cedula']."'":"";
	}else{
		$where='';
	}
	if (isset($client['estado'])) {
		$resp = $db->queryAll("SELECT * from cliente where estado ='".$client['estado']."' ".$where);
	}else{
		$resp = $db->queryAll("SELECT * from cliente where cedula ='".$client['cedula']."' ");
	}
	if ($resp) {
		return array('success'=>true,'msg'=>'','data'=>$resp);
	}else{
		return array('success'=>false,'msg'=>'Error', 'error'=>$db->lastError());
	}
}

function editarCliente($client){
	$db = new DB();
	$resp = $db->queryAll("SELECT cedula from cliente where cedula ='".$client['cedula']."'");
	if ($resp) {
		$query = $db->updateRows('cliente',$client,array('cedula' => $client['cedula']));
		if ($query) {
			return array('success'=>true,'msg'=>'');
		}else{
			return array('success'=>false,'msg'=>'Error al editarCliente',
				'error'=>$db->lastError());
		}
	}else{
		return array('success'=>false,'msg'=>'El cliente ya existe');
	}
}



?>