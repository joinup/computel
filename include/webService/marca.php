<?php 
$server->register("NewMarca");
$server->register("getMarcasModelo");
$server->register("updateMarca");
$server->register("NewModelo");
$server->register("newCategoria");
$server->register("newTipoEquipo");
$server->register("newBanco");
$server->register("bancos");

function NewMarca($marca){
	$db = new DB();
	$resp = $db->queryAll("SELECT * from marca where nombre ='".$marca['nombre']."'");
	if (!$resp) {
		$nuevaMarca = array();
		$nuevaMarca['nombre']=$marca['nombre'];
		$query = $db->queryRow("INSERT INTO marca (nombre) values('".$nuevaMarca['nombre']."') RETURNING id");

		if ($query) {
			if (count($marca['modelos'])) {
				for ($i=0; $i < count($marca['modelos']); $i++) { 
					$modelo = $db->queryRow("INSERT INTO modelo (id_marca,nombre) values(".$query['id'].",'".$marca['modelos'][$i]."')");
				}
			}
			return array('success'=>true,'msg'=>'');
		}else{
			return array('success'=>false,'msg'=>'Error');
		}
	}else{
		return array('success'=>false,'msg'=>'La marca ya existe');
	}
}

function getMarcasModelo(){
	$db = new DB();
	$resp = $db->queryAll("SELECT * from marca");
	if ($resp) {
		$marca_modelo = array();
		for ($i=0; $i < count($resp) ; $i++) { 
			$marca_modelo[$i]['nombre']=$resp[$i]['nombre'];
			$marca_modelo[$i]['id']=$resp[$i]['id'];
			$modelos = $db->queryAll("SELECT * FROM modelo where id_marca=".$resp[$i]['id']);
			$marca_modelo[$i]['modelos']=$modelos;
		}
		return array('success'=>true,'msg'=>'', 'data'=>$marca_modelo);
	}else{
		return array('success'=>false,'msg'=>'No hay Marca registrada', 'data'=>$resp);
	}
}

function updateMarca($marca_modelo){
	$db = new DB();
	$marca['nombre']=$marca_modelo['nombre'];
	$marca['id']=$marca_modelo['id'];
	$modelo=$marca_modelo['modelos'];
	$resp = $db->queryAll("SELECT * FROM marca where id=".$marca['id']);
	if ($resp) {
		$query = $db->updateRows('marca',$marca,array('id' => $marca['id']));
	}

	for ($i=0; $i < count($modelo) ; $i++) { 
		$resp2 = $db->queryAll("SELECT * FROM modelo where id=".$modelo[$i]['id']);
		if ($resp) {
			$query = $db->updateRows('modelo',$modelo[$i],array('id' => $modelo[$i]['id']));
		}else{
			return array('success'=>false,'msg'=>'El modelo no existe');
		}
	}

	return array('success'=>true,'msg'=>'Editado con exito');

}

function NewModelo($modelo_marca){
	$db = new DB();
	$modelo = $modelo_marca['modelos'];
	for ($i=0; $i < count($modelo) ; $i++) { 
		$query = $db->queryAll("INSERT INTO modelo (id_marca, nombre) values (".$modelo_marca['marca'].",'".$modelo[$i]."')");
	}
	return array('success'=>true,'msg'=>'modelos agregados');

}

function newCategoria($categoria){
	$db = new DB();

	$resp = $db->queryRow("SELECT * from categoria where nombre ='".$categoria['nombre']."'");
	if (!$resp) {
		$query = $db->insertRow('categoria',$categoria);
		if ($query) {
			return array('success'=>true,'msg'=>'Categoría registrada con exito');
		}else{
			return array('success'=>false,'msg'=>'Error al insertar la categoria',
				'error'=>$db->lastError());
		}
	}else{
		return array('success'=>false,'msg'=>'La categoría ya existe');
	}

}

function newTipoEquipo($tipo){
	$db = new DB();

	$resp = $db->queryRow("SELECT * from tipo_equipo where nombre ='".$tipo['nombre']."'");
	if (!$resp) {
		$query = $db->insertRow('tipo_equipo',$tipo);
		if ($query) {
			return array('success'=>true,'msg'=>'Tipo de equipo registrado con exito');
		}else{
			return array('success'=>false,'msg'=>'Error al insertar el tipo de equipo',
				'error'=>$db->lastError());
		}
	}else{
		return array('success'=>false,'msg'=>'El tipo de equipo ya existe');
	}

}

function newBanco($banco){
	$db = new DB();

	$resp = $db->queryRow("SELECT * from banco where nombre ='".$banco['nombre']."'");
	if (!$resp) {
		$query = $db->insertRow('banco',$banco);
		if ($query) {
			return array('success'=>true,'msg'=>'Banco registrado con exito');
		}else{
			return array('success'=>false,'msg'=>'Error al insertar Banco',
				'error'=>$db->lastError());
		}
	}else{
		return array('success'=>false,'msg'=>'El Banco ya existe');
	}
}

function bancos($banco=""){
	$db = new DB();

	$resp = $db->queryAll("SELECT * from banco");
	if ($resp) {
		return array('success'=>true,'data'=>$resp);
	}else{
		return array('success'=>false,'msg'=>'No hay bancos registrados');
	}

}

?>