<?php
include 'PDFClass.php';
$server->register("nuevaOrden");
$server->register("getOrdenes");
$server->register("EquiposTecnico");
$server->register("changeEstadoOrden");
$server->register("ordenes");
$server->register("editOrden");
$server->register("getTecnicosOrden");
$server->register("asignarTecnicos");
$server->register("ImprimirComprobante");
$server->register("facturar");
$server->register("factura_final");


function nuevaOrden($data) {
	$db         = new DB();
	$cliente    = $data['cliente'];
	$equipo     = $data['equipo'];
	$reparacion = $data['reparacion'];
	$tecnicos   = $data['tecnicos'];
	$accesorios = $data['accesorios'];
	$db->begin();
	$resp = $db->queryRow("SELECT cedula from cliente where cedula ='".$cliente['cedula']."'");
	if (!$resp) {
		$query = $db->insertRow('cliente', $cliente);
		if (!$query) {
			$db->rollback();
			return array('success' => false, 'msg' => 'Error al insertar el cliente',
				'error'=> $db->lastError());
		}
	}
	if (!$db->equipos($equipo)) {
		$eq = $db->registrarEquipo($equipo);
		if (!$eq['success']) {
			$db->rollback();
			return array('success' => false, 'msg' => 'Error al insertar el equipo',
				'error'=> $db->lastError());
		}
	}

	$reparacion['fecha']          = date("Y-m-d");
	$reparacion['serial_equipo']  = $equipo['serial_equipo'];
	$reparacion['cedula_cliente'] = $cliente['cedula'];

	//$resp = $db->insertRow('orden_reparacion', $reparacion);
	$resp = $db->queryRow("INSERT INTO orden_reparacion (fecha,serial_equipo, cedula_cliente,falla,descripcion) values(current_date,'".$reparacion['serial_equipo'] ."','".$reparacion['cedula_cliente'] ."','".$reparacion['falla'] ."','".$reparacion['descripcion'] ."') ");
	if ($resp) {
		$db->rollback();
		return array('success' => false, 'msg' => 'Error al insertar la orden',
			'error'=> $this->lastError());
	} else {
		$num_reparacion = $db->queryRow('SELECT max(num_reparacion) from orden_reparacion')['max'];
		//accesorios
		$insertAccesorios = array('num_reparacion' => $num_reparacion);
		foreach ($accesorios as $key               => $value) {
			$insertAccesorios[$value['value']] = $value['selected'];
		}
		
		if (!$db->insertRow('accesorios', $insertAccesorios)) {
			$db->rollback();
			return array('success' => false, 'msg' => 'No se ha podido los accesorios');
		}
		if (isset($data['otro'])) {
			$insertOtro=$db->queryRow("UPDATE accesorios set otro='".$data['otro']."' where num_reparacion=".$num_reparacion);
		}
		$check = true;
		//tecnicos
		/*foreach ($tecnicos as $key => $value) {
		if (!$db->insertRow('asigna_orden_tecnico',
		array('num_reparacion' => $num_reparacion,'cedula_tecnico' => $value))) {
		$check=false;
		}
		}*/
		if (!$db->insertRow('asigna_orden_tecnico',
				array('num_reparacion' => $num_reparacion, 'cedula_tecnico' => $tecnicos))) {
			$check = false;
		}
		if (!$check) {
			$db->rollback();
			return array('success' => false, 'msg' => 'No se ha podido Guardar tecnicos');
		} else {
			$db->finish();
			return array('success' => true, 'msg' => 'Se ha insertado con exito la orden');
		}

	}

}

function getOrdenes($orden) {
	$db    = new DB();
	//$where = ($orden)?" where a.num_reparacion=".$orden['num_reparacion']:'';
	$where = ($orden)?" where a.estado='".$orden['estado']."'":'';

	$query = "SELECT g.*,a.*,c.nombre,c.apellido,c.cedula,e.nombre as modelo,f.nombre as marca from orden_reparacion a left join equipo b on a.serial_equipo = b.serial_equipo
		left join cliente c on a.cedula_cliente=c.cedula
		left join equipo d on a.serial_equipo=d.serial_equipo
		left join modelo e on d.id_modelo=e.id
		left join marca f on d.id_marca=f.id
		left join accesorios g on a.num_reparacion=g.num_reparacion
		".$where;
	$query = $db->queryAll($query);
	if ($query) {
		return array('success' => true, 'data' => $query);
	} else {

		return array('success' => false, 'msg' => 'No hay orden en esta categoría');
	}
}

/*select a.*,b.nombre as marca ,c.nombre as modelo from equipo a inner join marca b on a.id_marca=b.id inner join modelo c on a.id_modelo=c.id;
 */
function EquiposTecnico($orden) {
	$db    = new DB();
	$query = "SELECT c.*,d.*,b.cedula_tecnico,b.comision,e.nombre,e.apellido,ma.nombre as marca ,mo.nombre as modelo,b.estado as estado,b.fecha_inicio,b.fecha_fin
		from tecnico a
		inner join asigna_orden_tecnico b on a.cedula=b.cedula_tecnico
		inner join orden_reparacion c on b.num_reparacion=c.num_reparacion
		inner join equipo d on c.serial_equipo=d.serial_equipo
		inner join marca ma on d.id_marca=ma.id
		inner join modelo mo on d.id_modelo=mo.id
		inner join cliente e on c.cedula_cliente=e.cedula
		where a.cedula='".$_SESSION['cedula']."' and b.estado='".$orden['estado']."'";
	$query = $db->queryAll($query);
	if ($query) {
		return array('success' => true, 'data' => $query);
	} else {

		return array('success' => false, 'msg' => 'No existe equipos en esta categoria');
	}
}

function changeEstadoOrden($estado) {
	$db        = new DB();
	$value     = array('estado' => $estado['estado']);
	$condition = array(
		'num_reparacion' => $estado['num_reparacion'],
		'cedula_tecnico' => $estado['cedula_tecnico'],
	);
	if ($estado['estado'] == 'procesado') {
		$value['fecha_inicio'] = date('Y-m-d');
	} else {

		$value['fecha_fin'] = date('Y-m-d');
	}
	if ($estado['estado']=='reparado') {
		$value['observacion']=$estado['observacion'];
		$piezas=$estado['piezas'];
		for ($i=0; $i < count($piezas); $i++) { 
			$insertPiezas = $db->queryRow("INSERT INTO uso_piezas_reparacion values (".$condition['num_reparacion'].",'".$piezas[$i]['pieza']."',".$piezas[$i]['cantidad'].",".$piezas[$i]['costo'].")");
			//if ($insertPiezas) {
				$db->minusPieza($piezas[$i]['pieza'],$piezas[$i]['cantidad']);
			//}
		}
	}
	$update = $db->updateRows('asigna_orden_tecnico', $value, $condition);
	if ($update) {

		return array('success' => true);
	} else {

		return array('success' => false, 'msg' => 'No se pudo actualizar el estado');
	}
}

function ordenes() {
	$db = new DB();
	$where = ($_SESSION['tipo']=="tecnico")?" and c.cedula_tecnico ='".$_SESSION['cedula']."'":"";
	$query = "SELECT a.fecha, a.falla, a.cedula_cliente, a.estado, b.nombre, b.apellido, b.tlf from orden_reparacion a inner join cliente b on a.cedula_cliente = b.cedula left join asigna_orden_tecnico c on a.num_reparacion=c.num_reparacion where a.estado='proceso' or a.estado='revision'".$where;
	$query = $db->queryAll($query);

	if ($query) {
		return array('success' => true, 'data' => $query);
	} else {
		return array('success' => false);
	}

}

function editOrden($orden) {
	$db   = new DB();
	$resp = $db->queryAll("SELECT num_reparacion from orden_reparacion where num_reparacion ='".$orden['num_reparacion']."'");
	if ($resp) {
		$query = $db->updateRows('orden_reparacion', $orden, array('num_reparacion' => $orden['num_reparacion']));
		if ($query) {
			return array('success' => true, 'msg' => 'Orden de raparación editada con exito');
		} else {
			return array('success' => false, 'msg' => 'Error al editar Orden',
				'error'=> $db->lastError());
		}
	} else {
		return array('success' => false, 'msg' => 'La orden no existe');
	}
}

function getTecnicosOrden($orden) {
	$db   = new DB();
	$tecn = $db->queryAll("SELECT a.*,b.nombre,b.apellido FROM asigna_orden_tecnico a
		inner join tecnico b on a.cedula_tecnico=b.cedula
		where num_reparacion=".$orden['num_reparacion']);
	$accesorios = $db-> queryAll("SELECT a.id_pieza,a.cantidad,b.nombre, b.precio_venta from uso_piezas_reparacion a inner join piezas b on a.id_pieza=b.id_pieza where a.num_reparacion=".$orden['num_reparacion']);

	for ($i=0; $i < count($tecn) ; $i++) { 
		$tecn[$i]['accesorios']=$accesorios;
	}

	if ($tecn) {

		return array('success' => true, 'data' => $tecn);
	} else {
		return array('success' => false, 'msg' => 'Error', 'error' => $db->lastError());
	}
}
function asignarTecnicos($data) {
	$db  = new DB();
	$verif = $db->queryAll("SELECT cedula_tecnico FROM asigna_orden_tecnico where num_reparacion=".$data['num_reparacion']." and cedula_tecnico='".$data['cedula_tecnico']."'");
	if ($verif) {
		return array('success' => false, 'msg' => 'El tecnico ya esta asignado a este equipo');
	} else {
		$tecn = $db->insertRow('asigna_orden_tecnico', $data);
		if ($tecn) {
			return array('success' => true, 'msg' => 'Tecnico Guardado con exito');
		} else {
			return array('success' => false, 'msg' => 'Error', 'error' => $db->lastError());
		}
	}
}
function ImprimirComprobante($data){
	extract($data);
	$audifonos = ($audifonos=='t')?"checked":"";
	$cargador = ($cargador=='t')?"checked":"";
	$pila = ($pila=='t')?"checked":"";
	$tarjeta_memoria = ($tarjeta_memoria=='t')?"checked":"";
	$sim_car = ($sim_car=='t')?"checked":"";
	$tapa = ($tapa=='t')?"checked":"";
	$otro = ($otro!='')?"Otro: ".$otro:"";
	$content = '<div>
					<div style="font-size: 12px; line-height: 110%;">
		    		<div>Rif: J40590879-3</div></br>
		    		<div>Teléfono: 0295-2633390</div></br>
		    		<div>Correo: computelmargarita@gmail.com</div></br>
		    		<div>Dirección: Avenida 4 de Mayo CC real piso 2 oficina 12 </div></br>
		    	</div>
    	<table class="table table-striped">
			<tbody>
				<tr>
					<td>N° reparación:</td>
					<td>'.$num_reparacion.'</td>
				</tr>
				<tr>
					<td>Cedula:</td>
					<td>'.$cedula_cliente.'</td>
				</tr>
				<tr>
					<td>Nombre cliente</td>
					<td>'.$nombre.' '.$apellido.'</td>
				</tr>
				<tr>
					<td>Serial de equipo</td>
					<td>'.$serial_equipo.'</td>
				</tr>
				<tr>
					<td>Modelo de equipo</td>
					<td>'.$marca.' - '.$modelo.'</td>
				</tr>
				<tr>
					<td>Falla</td>
					<td>'.$falla.'</td>
				</tr>
				<tr>
					<td>Estado de reparación</td>
					<td>'.$estado.'</td>
				</tr>
				<tr>
				<td>Accesorios dejados:</td>
				<td>
					Audifonos
					<input type="checkbox" '.$audifonos.' disabled="true"> 
					Cargador
					<input type="checkbox" '.$cargador.' disabled="true" style="margin-left: 5px;"> 
					Pila
					<input type="checkbox" '.$pila.' disabled="true" style="margin-left: 5px;"> 
					Tarjeta memoria
					<input type="checkbox" '.$tarjeta_memoria.' disabled="true" style="margin-left: 5px;"> 
					Sim card
					<input type="checkbox" '.$sim_car.' disabled="true" style="margin-left: 5px;"> 
					Tapa
					<input type="checkbox" '.$tapa.' disabled="true" style="margin-left: 5px;"> 
					'.$otro.'
				</td>
			</tr>
			</tbody>
		</table>
		<div class="text-center" style="padding-top:150px;">__________________<br>
		'.$nombre.' '.$apellido.'<br>C.I.:'.$cedula_cliente.'</div>';

		$name='comprobante_'.date("Ymdgis").'.pdf';
			newPdf($content,'Comprobante de orden de reparación','pdf/'.$name);
			return array('data' => $name,'success'=>true);
		/*}else{
			return array('data' => '','success'=>false,'msg'=>'No se pudo generar el comprobante');
		}*/
}
function facturar($data){
	$db = new DB();
	extract($data);
	if (!isset($reparacion)) {
		$reparacion='';
	}
	
	if (!isset($num_tarjeta)) {
		$num_tarjeta='';
	}
	if (!isset($num_transferencia)) {
		$num_transferencia='';
	}

	$tecnicos=$data['tecnicos'];
	if (!isset($banco)) {
		$num_factura =  $db->queryRow("INSERT INTO factura_servicio (num_control, cedula_cliente, num_reparacion, fecha, descripcion, monto, estado_factura,tipo_pago,num_tarjeta,num_transferencia)values(1,'$cedula_cliente',$num_reparacion,current_date,'$reparacion',$total,'pagada','$tipo_pago','$num_tarjeta','$num_transferencia') RETURNING num_factura");
	}else{
		$num_factura =  $db->queryRow("INSERT INTO factura_servicio (num_control, cedula_cliente, num_reparacion, fecha, descripcion, monto, estado_factura,tipo_pago,num_tarjeta,num_transferencia,banco)values(1,'$cedula_cliente',$num_reparacion,current_date,'$reparacion',$total,'pagada','$tipo_pago','$num_tarjeta','$num_transferencia',$banco) RETURNING num_factura");
	}


	$num_factura= $db->queryRow("select max(num_factura) as num_factura from factura_servicio;
");


	$orden = $db->queryRow("UPDATE orden_reparacion set estado='entregado' where num_reparacion=".$num_reparacion);

	if ($tecnicos) {
		for ($i=0; $i < count($tecnicos) ; $i++) { 
			$db->queryRow("UPDATE asigna_orden_tecnico set comision=".$tecnicos[$i]['comision'].", estado='reparado' where cedula_tecnico='".$tecnicos[$i]['cedula_tecnico']."' and num_reparacion=".$num_reparacion);
		}
	}

	$direccion=$db->queryRow("SELECT * FROM CLIENTE WHERE cedula='".$cedula."'");

	$content = '<div>
			<div style="font-size: 12px; line-height: 110%;">
    		<div>Rif: J40590879-3</div></br>
    		<div>Teléfono: 0295-2633390</div></br>
    		<div>Correo: computelmargarita@gmail.com</div></br>
    		<div>Dirección: Avenida 4 de Mayo CC real piso 2 oficina 12 </div></br>
    	</div>
    	<h3>N° Factura: '.$num_factura['num_factura'].'</h3>
		<table class="table table-striped">
			<tbody>
				<tr>
					<td>N° reparación:</td>
					<td>'.$num_reparacion.'</td>
				</tr>
				<tr>
					<td>Cedula:</td>
					<td>'.$cedula_cliente.'</td>
				</tr>
				<tr>
					<td>Nombre cliente</td>
					<td>'.$nombre.' '.$apellido.'</td>
				</tr>
				<tr>
					<td>Dirección</td>
					<td>'.$direccion['direccion'].'</td>
				</tr>
				<tr>
					<td>Serial de equipo</td>
					<td>'.$serial_equipo.'</td>
				</tr>
				<tr>
					<td>Modelo de equipo</td>
					<td>'.$marca.' - '.$modelo.'</td>
				</tr>
			</tbody>
		</table>
		';

	$content .= '<table class="table table-bordered">
			<tbody>
				<tr>
					<th>Serial</th>
					<th>Falla</th>
					<th>Descripción de la reparación</th>
					<th>Valor</th>
				</tr>
				<tr>
					<td>'.$serial_equipo.'</td>
					<td>'.$falla.'</td>
					<td>'.$reparacion.'</td>
					<td>'.$total.' Bs</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td>Total</td>
					<td>'.$total.' Bs</td>
				</tr>
				
			</tbody>
		</table>';

		$name='factura_'.date("Ymdgis").'.pdf';
			newPdf($content,'Factura de Servicio','pdf/'.$name,$num_factura['num_factura']);
			return array('data' => $name,'success'=>true,'num_factura'=>$num_factura['num_factura']);
		/*}else{
			return array('data' => '','success'=>false,'msg'=>'No se pudo generar el comprobante');
		}*/
}
/*function indicador1($date){
	$db = new DB();
	$where = ($date)?"where fecha between '".$date['startdate']."' and  '".$date['enddate']."'":'';
	$indicador = $db->queryAll("SELECT falla as name,count(*) as y from orden_reparacion  ".$where." group by falla ");
	return array('data' => $indicador,'success'=>true);
}*/

function factura_final($num_reparacion){
	$db = new DB();
	$resp = $db->queryRow("SELECT * FROM factura_servicio where num_reparacion=".$num_reparacion['data']);

	if ($resp) {
		return array('success'=>true, 'data'=>$resp);
	}else{
		return array('success'=>false, 'data'=>$resp);
	}
}


?>