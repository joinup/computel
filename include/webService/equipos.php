<?php 
$server->register("equipos");
$server->register("registrarEquipo");
$server->register("editarEquipo");
$server->register("getMarcas");
$server->register("getModelos");
$server->register("getModelCompra");
$server->register("getTiposEquipo");


function registrarEquipo($equipo){
	$db= new DB();
	return $db->registrarEquipo($equipo);
}

function equipos($equipo){
	$db = new DB();
	$resp = $db->equipos($equipo);
	if ($resp) {
		return array('success'=>true,'msg'=>'','data'=>$resp);
	}else{
		return array('success'=>false,'msg'=>'Error', 'error'=>$db->lastError());
	}
}

function editarEquipo($equipo){
	$db = new DB();
	$resp = $db->queryAll("SELECT serial_equipo from equipo where serial_equipo ='".$equipo['serial_equipo']."'");
	if ($resp) {
		$query = $db->updateRows('equipo',$equipo,array('serial_equipo' => $equipo['serial_equipo']));
		if ($query) {
			return array('success'=>true,'msg'=>'');
		}else{
			return array('success'=>false,'msg'=>'Error al editar el equipo',
				'error'=>$db->lastError());
		}
	}else{
		return array('success'=>false,'msg'=>'El equipo ya existe');
	}
}

function getMarcas($marca=null){
	$db= new DB();
	$where = (!empty($marca))?" where id =".$marca['id']:"";
	$query = $db->queryAll("SELECT * from marca ".$where);
	return array('success'=>!!$query,'msg'=>'','data'=>$query);
}

function getModelos($marca=null){
	$db= new DB();
	$where = (!empty($marca))?" where id_marca =".$marca:"";
	$query = $db->queryAll("SELECT * from modelo ".$where);
	return array('success'=>!!$query,'msg'=>'','data'=>$query);
}

function getModelCompra($marcas){
	$db= new DB();
	$or=count($marcas) - 1;
	$where = " where ";

	for ($i=0; $i < count($marcas); $i++) { 
		if ($or != 0) {
			$where = $where."id_marca=".$marcas[$i]." or ";
		}else{
			$where = $where."id_marca=".$marcas[$i]." ";
		}
		$or=$or-1;
	}
	$query = $db->queryAll("SELECT * from modelo ".$where);
	return array('success'=>!!$query,'msg'=>'','data'=>$query);

}

function getTiposEquipo(){
	$db= new DB();
	$query = $db->queryAll("SELECT * from tipo_equipo");
	return array('success'=>!!$query,'msg'=>'','data'=>$query);
}
?>