<?php 
$server->register("inventario");
$server->register("eliminarInventario");
$server->register("NewFacturaCompra");
$server->register("SaveHerramienta");
$server->register("SavePieza");
$server->register("categorias");
$server->register("factura_servicio");

function factura_servicio($num){
	$db = new DB();
	$resp = $db->queryAll("SELECT * from factura_compra where num_factura=".$num['num_factura']." and rif='".$num['rif']."'");
	if ($resp) {
		return array('msg' =>'N° factura ya existe' ,'success'=>false);
	}else{
		return array('msg' =>'Continue' ,'success'=>true);
	}
}

function inventario() {
	$db = new DB();
	$data = array();
	$data['piezas'] = $db->piezas();
	$data['mobiliario'] = $db->inventario('mobiliario');
	$data['herramientas'] = $db->inventario('herramienta');
	$data['factura_compra'] = $db->queryAll("SELECT a.*,b.nombre_prov from factura_compra a inner join proveedor b on a.rif=b.rif"); 
	return array('data' =>$data ,'success'=>true);
}
// alter table detalle_factura_compra drop column codigo_articulo ;
function NewFacturaCompra($factura){
	$info = $factura['factura'];
	$productos = $factura['prod'];
	$db = new DB();
	$db->begin();
	
	//$info['fecha_compra']="2016-09-09";
	
	$new = $db->insertRow('factura_compra',$info);
	if (!$new) {
		$db->rollback();
		return array('success' => false,'msg'=>'error al guardar factura');
	}else{
		foreach ($productos as $key => $value) {
			$producto = array();	
			$producto['rif']=$info['rif'];
			$producto['num_factura']=$info['num_factura'];
			$producto['cantidad_comprado']=(int)$value['cantidad'];
			$producto['costo_unitario']=$value['costo'];
			$producto['id']=$key+1;

			if ($value['tipo']=='pieza') {
				$add = $db->addPieza($value['id_pieza'],$value['cantidad'],$value['costo'], $value['precio_venta']);
				$producto['codigo_pieza']=$value['id_pieza'];
			}else{
				$add = $db->addInventario($value['id_inventario'],$value['cantidad'],$value['costo']);
				$producto['codigo_herramienta']=$value['id_inventario'];
			}
			$newProducto = $db->insertRow('detalle_factura_compra',$producto);
			if (!$newProducto) {
				$db->rollback();
				return array('success' => false,'msg'=>'error al guardar detalles de factura');
			}
		}	
		$db->finish();
		return array('success' => true,'msg'=>'');
	}
}
function eliminarInventario($data){
	$db = new DB();
	if ($data['tipo'] == 'herramienta' || $data['tipo']=='mobiliario') {
		$resp = $db->queryRow("SELECT * from herramientas_mobiliario where id ='".$data['producto']."'");
		if ($resp) {
			if ($resp['cantidad'] < $data['cantidad']) {
				return array('success'=>false,'msg'=>'La cantidad excede a la existente');
			}else{
				$query = $db->queryRow("UPDATE herramientas_mobiliario set cantidad=cantidad-'".$data['cantidad']."' where id ='".$data['producto']."'");
				return array('success'=>true,'msg'=>'Productos eliminados');
			}
		}else{
			return array('success'=>true,'msg'=>'Productos no existe');
		}
	}else{
		$resp = $db->queryRow("SELECT * from piezas where id_pieza ='".$data['producto']."'");
		if ($resp) {
			if ($resp['cantidad'] < $data['cantidad']) {
				return array('success'=>false,'msg'=>'La cantidad excede a la existente');
			}else{
				$query = $db->queryRow("UPDATE piezas set cantidad=cantidad-'".$data['cantidad']."' where id_pieza ='".$data['producto']."'");
				return array('success'=>true,'msg'=>'Productos eliminados');
			}
		}else{
			return array('success'=>true,'msg'=>'Productos no existe');
		}
	}

}

function SaveHerramienta($producto){
	$db = new DB();
	$resp = $db->queryRow("SELECT * from herramientas_mobiliario where nombre ='".$producto['nombre']."'");
	$producto['cantidad']=0; 
	if (!$resp) {
		$new = $db->insertRow('herramientas_mobiliario',$producto);
		return array('success'=>true,'msg'=>'Productos agregado');
	}

}

function SavePieza($producto){
	$db = new DB();
	$producto['cantidad']=0;
	if ($producto['modelo']) {
		$modelos=$producto['modelo'];
		unset($producto['modelo']);
	}
	$resp = $db->queryRow("SELECT * from piezas where id_pieza ='".$producto['id_pieza']."'");
	$producto['cantidad']=0; 
	if (!$resp) {
		$new = $db->insertRow('piezas',$producto);
		for ($i=0; $i < count($modelos); $i++) { 
			$marca=$db->queryRow("SELECT * FROM modelo where id=".$modelos[$i]);
			$db->queryRow("INSERT INTO pieza_modelo_marca values (".$marca['id_marca'].",".$modelos[$i].",'".$producto['id_pieza']."')");
		}
		return array('success'=>true,'msg'=>'Pieza agregada');
	}else{
		return array('success'=>false,'msg'=>'Esta pieza ya existe');
	}

}
function categorias(){
	$db = new DB();
	$resp = $db->queryAll("SELECT * from categoria");
	if ($resp) {
		return array('success'=>true,'msg'=>'Pieza agregada','data'=>$resp);
	}
}

?>