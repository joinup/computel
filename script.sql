CREATE DOMAIN forma_pago AS VARCHAR (15) CHECK (value IN ('efectivo','debito','credito','transferencia'));
CREATE DOMAIN estado_factura AS VARCHAR (8) CHECK (value IN ('pagada','anulada'));
CREATE DOMAIN estado AS VARCHAR (8) CHECK (value IN ('activo','inactivo'));
CREATE DOMAIN tipo_tecnico AS VARCHAR (8) CHECK (value IN ('software','hardware','ambos'));
CREATE DOMAIN estado_orden AS VARCHAR (10) CHECK (value IN ('revision','devuelto','proceso','reparado','entregado'));
CREATE DOMAIN tipo_activo AS VARCHAR (15) CHECK (value IN ('herramienta','mobiliario'));
CREATE DOMAIN estado_activo AS VARCHAR (8) CHECK (value IN ('activo','dañado','inactivo'));
CREATE DOMAIN estado_equipo AS VARCHAR (10) CHECK (value IN ('espera','procesado','reparado','cancelado'));




CREATE TABLE cliente(
tipo varchar(1) NOT NULL, 
cedula varchar(20) NOT NULL,
nombre varchar(20) NOT NULL,
apellido VARCHAR(20) NOT NULL,
direccion VARCHAR(200),
tlf VARCHAR (15) NOT NULL,
email varchar(30),
estado VARCHAR (8) CHECK (estado IN ('activo','inactivo')) default 'activo',
CONSTRAINT pk_cliente PRIMARY KEY (cedula)
);

CREATE TABLE tecnico(
cedula varchar(20) NOT NULL,
nombre varchar(20) NOT NULL,
apellido VARCHAR(20) NOT NULL,
direccion VARCHAR(200),
tlf VARCHAR (15),
email varchar(30),
tipo VARCHAR (8) CHECK (tipo IN ('software','hardware','ambos')),
estado VARCHAR (8) CHECK (estado IN ('activo','inactivo')) default 'activo',
CONSTRAINT pk_tecnico PRIMARY KEY (cedula)
);

CREATE TABLE marca(
id SERIAL NOT NULL,
nombre varchar(20) NOT NULL, 
estado VARCHAR (8) CHECK (estado IN ('activo','inactivo')) default 'activo',
CONSTRAINT pk_marca PRIMARY KEY (id)
);

CREATE TABLE modelo(
id SERIAL NOT NULL,
id_marca INT NOT NULL,
nombre varchar(20) NOT NULL,
estado VARCHAR (8) CHECK (estado IN ('activo','inactivo')) default 'activo',
CONSTRAINT pk_modelo PRIMARY KEY (id,id_marca),
CONSTRAINT fk_marca_modelo FOREIGN KEY (id_marca) REFERENCES marca (id)
);


CREATE TABLE tipo_equipo(
codigo_tipo SERIAL NOT NULL,
nombre varchar(20) NOT NULL,
estado VARCHAR (8) CHECK (estado IN ('activo','inactivo')) default 'activo',
CONSTRAINT pk_tipo_equipo PRIMARY KEY (codigo_tipo)
);

CREATE TABLE banco(
id SERIAL NOT NULL,
nombre varchar(20) NOT NULL,
CONSTRAINT pk_banco PRIMARY KEY (id)
);


CREATE TABLE equipo(
serial_equipo varchar(30) NOT NULL, 
id_modelo INT NOT NULL,
id_marca INT NOT NULL,
codigo_tipo INT NOT NULL,
observaciones varchar(100),
CONSTRAINT pk_equipo PRIMARY KEY (serial_equipo),
CONSTRAINT fk_modelo_equipo FOREIGN KEY (id_modelo,id_marca) REFERENCES modelo (id,id_marca),
CONSTRAINT fk_tipo_equipo FOREIGN KEY (codigo_tipo) REFERENCES tipo_equipo (codigo_tipo)
);



CREATE TABLE orden_reparacion(
num_reparacion SERIAL NOT NULL,
fecha DATE NOT NULL,
serial_equipo varchar(30) NOT NULL,
cedula_cliente varchar(20) NOT NULL,
falla varchar(50),
descripcion varchar(200),
estado VARCHAR (10) CHECK (estado IN ('revision','devuelto','proceso','reparado','entregado')) default 'revision',
CONSTRAINT pk_orden_reparacion PRIMARY KEY (num_reparacion),
CONSTRAINT fk_orden_cliente FOREIGN KEY (cedula_cliente) REFERENCES cliente (cedula),
CONSTRAINT fk_equipo_reparacion FOREIGN KEY (serial_equipo) REFERENCES equipo (serial_equipo)
);

CREATE TABLE factura_servicio(
num_factura SERIAL NOT NULL,
num_control int NOT NULL,
cedula_cliente varchar(20) NOT NULL,
num_reparacion int NOT NULL,
fecha DATE default CURRENT_DATE,
descripcion VARCHAR (200) NOT NULL,
monto NUMERIC(16,2),
estado_factura VARCHAR (8) CHECK (estado_factura IN ('pagada','anulada')) NOT NULL,
tipo_pago VARCHAR (15) CHECK (tipo_pago IN ('efectivo','debito','credito','transferencia')),
CONSTRAINT pk_factura_servicio PRIMARY KEY (num_factura, cedula_cliente, num_reparacion),
CONSTRAINT fk_fatura_cliente FOREIGN KEY (cedula_cliente) REFERENCES cliente (cedula),
CONSTRAINT fk_orden_reparacion_servicio FOREIGN KEY (num_reparacion) REFERENCES orden_reparacion (num_reparacion)
);


CREATE TABLE proveedor(
letra varchar(2) NOT NULL,
rif varchar(10) NOT NULL,
nombre_prov varchar(20) NOT NULL,
direccion VARCHAR(200),
tlf VARCHAR (15),
nombre_resp varchar(20),
tlf_resp VARCHAR (15),
email varchar(30),
CONSTRAINT pk_proveedor PRIMARY KEY (rif)
);

CREATE TABLE factura_compra(
num_factura varchar(40) NOT NULL,
rif varchar(10) NOT NULL,
fecha_compra DATE default CURRENT_DATE,
monto_total NUMERIC(16,2),
tipo_pago VARCHAR (15) CHECK (tipo_pago IN ('efectivo','debito','credito','transferencia')),
num_referencia VARCHAR (25),
CONSTRAINT pk_factura_compra PRIMARY KEY (num_factura,rif),
CONSTRAINT fk_proveedor_factura FOREIGN KEY (rif) REFERENCES proveedor (rif)
);


CREATE TABLE herramientas_mobiliario(
tipo VARCHAR (15) CHECK (tipo IN ('herramienta','mobiliario')) NOT NULL,
id SERIAL NOT NULL,
nombre varchar(30) NOT NULL,
UNIDAD VARCHAR(30),
cantidad NUMERIC(10,0),
valor_ref NUMERIC(16,2),
estado VARCHAR (8) CHECK (estado IN ('activo','dañado','inactivo')) NOT NULL default 'activo' ,
CONSTRAINT pk_herramientas_mobiliario PRIMARY KEY (id)
);

CREATE TABLE piezas(
id_pieza VARCHAR NOT NULL,
nombre varchar(30) NOT NULL,
descripcion varchar(200),
UNIDAD VARCHAR(30),
MEDIDA VARCHAR(30),
cantidad NUMERIC(10,0),
costo NUMERIC(16,2),
precio_venta NUMERIC(16,2),
estado VARCHAR (8) CHECK (estado IN ('activo','inactivo')) default 'activo',
categotia integer,
CONSTRAINT pk_piezas PRIMARY KEY (id_pieza)
);

CREATE TABLE pieza_modelo_marca(
id_marca int NOT NULL,
id_modelo int NOT NULL,
id_pieza varchar NOT NULL,
CONSTRAINT pk_pieza_modelo_marca PRIMARY KEY (id_pieza, id_modelo,id_marca),
CONSTRAINT fk_modelo_marca_pieza FOREIGN KEY (id_marca,id_modelo) REFERENCES modelo (id_marca,id)
);

CREATE TABLE accesorios(
num_reparacion int NOT NULL,
cargador BOOLEAN default false,
pila BOOLEAN default false,
audifonos BOOLEAN default false,
tarjeta_memoria BOOLEAN default false,
sim_car BOOLEAN default false,
otro varchar(100),
CONSTRAINT pk_accesorios PRIMARY KEY (num_reparacion),
CONSTRAINT fk_orden_reparacion_accesorios FOREIGN KEY (num_reparacion) REFERENCES orden_reparacion (num_reparacion)
);


CREATE TABLE asigna_orden_tecnico(
num_reparacion int NOT NULL,
cedula_tecnico varchar(20) NOT NULL,
fecha_inicio DATE,
fecha_fin DATE,
estado VARCHAR (10) CHECK (estado IN ('espera','procesado','reparado','cancelado')) default 'espera',
observacion varchar(100),
CONSTRAINT pk_asigna_orden_tecnico PRIMARY KEY (num_reparacion, cedula_tecnico),
CONSTRAINT fk_orden_reparacion_tecnico FOREIGN KEY (num_reparacion) REFERENCES orden_reparacion (num_reparacion),
CONSTRAINT fk_tecnico_asigna FOREIGN KEY (cedula_tecnico) REFERENCES tecnico (cedula)
);


CREATE TABLE detalle_factura_compra(
rif varchar(10) NOT NULL,
num_factura varchar(40) NOT NULL,
item int Not NULL,
id_pieza varchar,
id_inventario int,
cantidad_comprado int NOT NULL,
costo_unitario NUMERIC(16,2) NOT NULL,
CONSTRAINT pk_detalle_factura_compra PRIMARY KEY (rif,num_factura,item),
CONSTRAINT fk_factura_compra FOREIGN KEY (num_factura, rif) REFERENCES factura_compra (num_factura,rif),
CONSTRAINT fk_factura_compra_pieza FOREIGN KEY (id_pieza) REFERENCES piezas (id_pieza),
CONSTRAINT fk_factura_inventario FOREIGN KEY (id_inventario) REFERENCES herramientas_mobiliario (id)
);


CREATE TABLE uso_piezas_reparacion(
num_reparacion int NOT NULL,
id_pieza varchar NOT NULL,
cantidad NUMERIC(16,0) default 1,
costo NUMERIC(16,2) NOT NULL,
CONSTRAINT pk_uso_piezas_reparacion PRIMARY KEY (num_reparacion,id_pieza),
CONSTRAINT fk_orden_reparacion_uso_pieza FOREIGN KEY (num_reparacion) REFERENCES orden_reparacion (num_reparacion),
CONSTRAINT fk_piezas_uso FOREIGN KEY (id_pieza) REFERENCES piezas (id_pieza)
);


CREATE TABLE usuario(
cedula varchar(40) not null,
clave varchar(50) not null,
tipo varchar(10),
nombre varchar(30),
apellido varchar(30),
direccion varchar(50),
CONSTRAINT pk_usuario primary key (cedula)
);

insert into usuario values('21322117','1234','admin','Jorge','Caraballo'),('20535755','1234','admin','Solianyelis','Caraballo'),('22650767','1234','admin','Maria','Guerra');
insert into cliente values ('V','21322117','Juan','Garcia','cruz del pastel','04248010326','juan@gmail.com'),('V','20536756','Maria','Perez','Pampatar','04248015326','maria@gmail.com'),('V','1934356','Luisa','Vasquez','La asunción','04248430326','luisa@gmail.com');

